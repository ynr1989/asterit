-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2021 at 04:28 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smydatademo_asterit`
--

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(25) NOT NULL,
  `userTypeCode` int(11) NOT NULL,
  `userType` varchar(50) DEFAULT NULL,
  `businessName` varchar(255) NOT NULL,
  `ownerFirstName` varchar(55) DEFAULT NULL,
  `ownerLastName` varchar(55) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `businessUrl` varchar(255) DEFAULT NULL,
  `businessStartDate` date DEFAULT NULL,
  `businessEndDate` date DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `test_password` varchar(155) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `ssnNumber` varchar(50) DEFAULT NULL,
  `aadharNumber` varchar(25) DEFAULT NULL,
  `passportNumber` varchar(155) DEFAULT NULL,
  `businessLogo` text DEFAULT NULL,
  `bankName` varchar(155) DEFAULT NULL,
  `bankAccountNumber` varchar(55) DEFAULT NULL,
  `routingNumber` varchar(100) DEFAULT NULL,
  `businessStatus` int(11) NOT NULL DEFAULT 0,
  `passwordFlag` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`businessUniqueId`, `businessId`, `userTypeCode`, `userType`, `businessName`, `ownerFirstName`, `ownerLastName`, `email`, `mobile`, `dob`, `businessUrl`, `businessStartDate`, `businessEndDate`, `password`, `test_password`, `address`, `gender`, `ssnNumber`, `aadharNumber`, `passportNumber`, `businessLogo`, `bankName`, `bankAccountNumber`, `routingNumber`, `businessStatus`, `passwordFlag`, `created_at`) VALUES
(1, 'BIN001', 2, 'Business', 'Smydata', 'Raj', 'R', 'smydata@gmail.com', '9515741627', '1996-06-26', 'smydata.com', '2020-06-01', '2023-06-08', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Hyderadad', 'Male', '454545454', NULL, NULL, 'smydata.png', 'SBI', '544874854564', '4165465412123', 1, 1, '2021-06-02 04:50:57');

-- --------------------------------------------------------

--
-- Table structure for table `business_documents`
--

CREATE TABLE `business_documents` (
  `businessDocumentId` int(11) NOT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `documentTypeId` int(11) NOT NULL,
  `documentType` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `documentStatus` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `clientUniqueId` int(11) NOT NULL,
  `userTypeCode` varchar(40) COLLATE utf8_bin NOT NULL,
  `userType` varchar(55) COLLATE utf8_bin DEFAULT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `clientId` varchar(50) COLLATE utf8_bin NOT NULL,
  `clientFirm` varchar(40) COLLATE utf8_bin NOT NULL,
  `contactName` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_bin NOT NULL,
  `email` varchar(155) COLLATE utf8_bin NOT NULL,
  `address` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `panNumber` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `aadharNumber` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `passportNumber` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `gender` enum('Male','Female','Other') COLLATE utf8_bin DEFAULT NULL,
  `profilePic` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `clientStatus` int(2) NOT NULL DEFAULT 1,
  `createdBy` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`clientUniqueId`, `userTypeCode`, `userType`, `businessUniqueId`, `businessId`, `clientId`, `clientFirm`, `contactName`, `mobile`, `email`, `address`, `panNumber`, `aadharNumber`, `passportNumber`, `gender`, `profilePic`, `description`, `clientStatus`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, '9', 'Client', 1, 'BIN001', 'CLI001', 'Client', 'Client', '5787878787', 'client@gmail.com', 'Hyderadad', NULL, NULL, NULL, 'Female', NULL, '', 1, NULL, '2021-06-02 05:11:33', '2021-06-02 05:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `education_documents`
--

CREATE TABLE `education_documents` (
  `educationDocumentId` int(11) NOT NULL,
  `educationId` int(11) NOT NULL,
  `documentTitle` varchar(155) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `educationDocumentStatus` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employee_education_details`
--

CREATE TABLE `employee_education_details` (
  `educationId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `qualificationTitle` varchar(255) NOT NULL,
  `specialization` varchar(155) NOT NULL,
  `institution` varchar(255) NOT NULL,
  `startYear` date NOT NULL,
  `endYear` date NOT NULL,
  `attachment1` varchar(255) NOT NULL,
  `educationStatus` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_education_details`
--

INSERT INTO `employee_education_details` (`educationId`, `userUniqueId`, `qualificationTitle`, `specialization`, `institution`, `startYear`, `endYear`, `attachment1`, `educationStatus`, `created_at`, `updated_at`) VALUES
(1, 194, '10th', '', 'APS', '2010-06-02', '2011-06-01', '1622612419_favicon.png', 1, '2021-06-02 05:40:19', '2021-06-02 05:40:19'),
(2, 194, 'Inter', 'MPC', 'BOD', '2011-06-01', '2013-06-11', '1622612419_favicon.png', 1, '2021-06-02 05:40:19', '2021-06-02 05:40:19'),
(3, 194, 'BTECH', 'CSE', 'JNTU', '2013-06-04', '2017-06-26', '1622612419_favicon.png', 1, '2021-06-02 05:40:19', '2021-06-02 05:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `employee_family_details`
--

CREATE TABLE `employee_family_details` (
  `employeeFamilyId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `employeeId` varchar(15) NOT NULL,
  `businessUniqueId` int(4) NOT NULL,
  `businessId` varchar(15) NOT NULL,
  `familyType` int(11) NOT NULL COMMENT '1=Spouse, 2=Kid',
  `firstName` varchar(155) NOT NULL,
  `lastName` varchar(155) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(155) DEFAULT NULL,
  `mobile` varchar(12) NOT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_family_details`
--

INSERT INTO `employee_family_details` (`employeeFamilyId`, `userUniqueId`, `employeeId`, `businessUniqueId`, `businessId`, `familyType`, `firstName`, `lastName`, `gender`, `email`, `mobile`, `dob`, `address`) VALUES
(1, 194, 'EMP005', 1, 'BIN001', 3, 'Vihar', 'A', 'Male', 'vihari@gmail.com', '7878787878', '2018-06-01', 'Hyderabad'),
(2, 194, 'EMP005', 1, 'BIN001', 2, 'Sarath', 'A', 'Male', 'sarath@gmail.com', '8578789787', '1992-06-01', 'Hyderabad');

-- --------------------------------------------------------

--
-- Table structure for table `employee_passport_details`
--

CREATE TABLE `employee_passport_details` (
  `passportUniqueId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `employeeId` varchar(25) NOT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(25) NOT NULL,
  `familyType` varchar(25) NOT NULL,
  `employeeFamilyId` int(11) NOT NULL,
  `firstName` varchar(155) NOT NULL,
  `lastName` varchar(155) NOT NULL,
  `passportNo` varchar(25) NOT NULL,
  `issueDate` date NOT NULL,
  `placeOfIssue` varchar(155) NOT NULL,
  `expireDate` date NOT NULL,
  `country` varchar(11) NOT NULL,
  `attachment1` varchar(255) DEFAULT NULL,
  `attachment2` varchar(155) DEFAULT NULL,
  `attachment3` varchar(155) DEFAULT NULL,
  `passportStatus` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_passport_details`
--

INSERT INTO `employee_passport_details` (`passportUniqueId`, `userUniqueId`, `employeeId`, `businessUniqueId`, `businessId`, `familyType`, `employeeFamilyId`, `firstName`, `lastName`, `passportNo`, `issueDate`, `placeOfIssue`, `expireDate`, `country`, `attachment1`, `attachment2`, `attachment3`, `passportStatus`, `created_at`, `updated_at`) VALUES
(1, 194, 'EMP005', 1, 'BIN001', 'Kids', 1, 'Vihari', 'A', '657487878', '2021-06-03', 'USA', '2024-06-06', '0', '1622612877_1.jpeg', NULL, NULL, 1, '2021-06-02 05:47:57', '2021-06-02 05:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `employee_visa_details`
--

CREATE TABLE `employee_visa_details` (
  `visaUniqueId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `employeeId` varchar(25) NOT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(25) NOT NULL,
  `familyType` varchar(25) NOT NULL,
  `employeeFamilyId` int(11) NOT NULL,
  `visaType` varchar(155) NOT NULL,
  `visaNumber` varchar(155) NOT NULL,
  `issueDate` date NOT NULL,
  `placeOfIssue` varchar(155) NOT NULL,
  `expireDate` date NOT NULL,
  `country` varchar(11) NOT NULL,
  `attachment1` varchar(255) DEFAULT NULL,
  `attachment2` varchar(155) DEFAULT NULL,
  `attachment3` varchar(155) DEFAULT NULL,
  `passportStatus` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_visa_details`
--

INSERT INTO `employee_visa_details` (`visaUniqueId`, `userUniqueId`, `employeeId`, `businessUniqueId`, `businessId`, `familyType`, `employeeFamilyId`, `visaType`, `visaNumber`, `issueDate`, `placeOfIssue`, `expireDate`, `country`, `attachment1`, `attachment2`, `attachment3`, `passportStatus`, `created_at`, `updated_at`) VALUES
(1, 194, 'EMP005', 1, 'BIN001', 'Kids', 1, 'Visiting', '8787878', '2021-06-01', '23', '2024-06-11', '0', '1622612942_orcale.jpg', NULL, NULL, 1, '2021-06-02 05:49:02', '2021-06-02 05:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `employment_details`
--

CREATE TABLE `employment_details` (
  `employmentId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `attachment1` varchar(255) DEFAULT NULL,
  `attachment2` varchar(255) DEFAULT NULL,
  `attachment3` varchar(255) DEFAULT NULL,
  `technology` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employment_details`
--

INSERT INTO `employment_details` (`employmentId`, `userUniqueId`, `companyName`, `address`, `fromDate`, `toDate`, `role`, `attachment1`, `attachment2`, `attachment3`, `technology`, `created_at`, `updated_at`) VALUES
(1, 194, 'Smydata', 'Hyderabad', '2018-06-14', '2022-06-14', 'Tester', '1622612547_mongo.png', NULL, NULL, 'Test', '2021-06-02 05:42:27', '2021-06-02 05:42:27'),
(2, 194, 'Smydata1', 'Hyderabad', '2021-06-02', '2023-06-26', 'Tester', '1622612547_orcale.jpg', NULL, NULL, 'UTest', '2021-06-02 05:42:27', '2021-06-02 05:42:27');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `projectUniqueId` int(11) NOT NULL,
  `projectId` varchar(15) NOT NULL,
  `projectName` varchar(155) NOT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(20) NOT NULL,
  `vendorUniqueId` int(11) NOT NULL,
  `vendorId` int(11) DEFAULT NULL,
  `clientUniqueId` int(11) NOT NULL,
  `clientId` varchar(15) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `billRate` varchar(15) DEFAULT NULL,
  `netDays` varchar(15) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `projectStatus` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`projectUniqueId`, `projectId`, `projectName`, `businessUniqueId`, `businessId`, `vendorUniqueId`, `vendorId`, `clientUniqueId`, `clientId`, `userUniqueId`, `employeeId`, `startDate`, `endDate`, `billRate`, `netDays`, `description`, `projectStatus`, `created_at`, `updated_at`) VALUES
(1, 'PRJ001', 'Smydata', 1, 'BIN001', 1, 0, 1, 'CLI001', 194, 0, '2021-06-01', '2023-06-02', '50000', '120', '', 1, '2021-06-02 05:16:38', '2021-06-02 05:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `project_documents`
--

CREATE TABLE `project_documents` (
  `projectDocumentId` int(11) NOT NULL,
  `documentKey` varchar(20) NOT NULL,
  `projectUniqueId` int(11) NOT NULL,
  `documentName` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `documentStatus` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sms_history`
--

CREATE TABLE `sms_history` (
  `id` int(11) NOT NULL,
  `mobile` varchar(155) NOT NULL,
  `sms` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_codes`
--

CREATE TABLE `tbl_codes` (
  `tbl_codes_id` int(4) UNSIGNED NOT NULL,
  `type` varchar(40) COLLATE utf8_bin NOT NULL,
  `code` int(6) NOT NULL,
  `value` varchar(40) COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_codes`
--

INSERT INTO `tbl_codes` (`tbl_codes_id`, `type`, `code`, `value`, `status`) VALUES
(1, 'USER_TYPE', 1, 'Superadmin', 1),
(2, 'USER_TYPE', 2, 'Business', 1),
(3, 'USER_TYPE', 3, 'HR', 1),
(4, 'USER_TYPE', 4, 'Employee', 1),
(5, 'USER_TYPE', 5, 'Accountant', 1),
(6, 'USER_TYPE', 6, 'Visa', 1),
(7, 'USER_TYPE', 7, 'Bench', 1),
(8, 'USER_TYPE', 8, 'Vendor', 1),
(9, 'USER_TYPE', 9, 'Client', 1),
(10, 'DOCUMENT_TYPE', 1, 'DL', 1),
(11, 'DOCUMENT_TYPE', 2, 'Passport', 1),
(12, 'DOCUMENT_TYPE', 3, 'State ID', 1),
(13, 'DOCUMENT_TYPE', 4, 'Others', 1),
(14, 'FAMILY_TYPE', 1, 'Self', 1),
(15, 'FAMILY_TYPE', 2, 'Spouse', 1),
(16, 'FAMILY_TYPE', 3, 'Kids', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_communications`
--

CREATE TABLE `tbl_communications` (
  `commid` int(11) NOT NULL,
  `category_name` varchar(30) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_communications`
--

INSERT INTO `tbl_communications` (`commid`, `category_name`, `subject`, `message`, `created_date_time`) VALUES
(1, 'Aster IT', 'Account Logins', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n<style>\r\ntable {\r\n  font-family: arial, sans-serif;\r\n  border-collapse: collapse;\r\n  width: 100%;\r\n}\r\n\r\ntd, th {\r\n  border: 1px solid #dddddd;\r\n  text-align: left;\r\n  padding: 8px;\r\n}\r\n\r\ntr:nth-child(even) {\r\n  background-color: #dddddd;\r\n}\r\n</style>\r\n</head>\r\n<body>\r\n\r\n<h2>Login Information</h2>\r\n\r\n<table>  \r\n  <tr>\r\n    <th>Email</th>\r\n    <td>EMAIL</td>\r\n  </tr>\r\n  <tr>\r\n    <th>Mobile Number</th>\r\n    <td>MOBILE</td>\r\n  </tr>\r\n  <tr>\r\n    <th>Password</th>\r\n    <td>PASSWORD</td>\r\n  </tr>\r\n  <tr><td colspan=\"2\"><strong>Note: </strong>Please use Email/Mobile as username for login.</td></tr>\r\n</table>\r\n\r\n</body>\r\n</html>\r\n', '2020-08-11 04:38:18'),
(2, 'Forgot Password', 'Forgot Password', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n<style>\r\ntable {\r\n  font-family: arial, sans-serif;\r\n  border-collapse: collapse;\r\n  width: 100%;\r\n}\r\n\r\ntd, th {\r\n  border: 1px solid #dddddd;\r\n  text-align: left;\r\n  padding: 8px;\r\n}\r\n\r\ntr:nth-child(even) {\r\n  background-color: #dddddd;\r\n}\r\n</style>\r\n</head>\r\n<body>\r\n\r\n<h2>Please use below OTP</h2>\r\n\r\n<table>\r\n  <tr>\r\n    <th>Mobile Number</th>\r\n    <td>MOBILE</td>\r\n  </tr>\r\n  <tr>\r\n    <th>OTP</th>\r\n    <td>OTP</td>\r\n  </tr>\r\n</table>\r\n\r\n</body>\r\n</html>\r\n', '2020-08-02 04:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timesheets`
--

CREATE TABLE `tbl_timesheets` (
  `timesheetId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `projectUniqueId` int(11) NOT NULL,
  `workDate` date DEFAULT NULL,
  `standardHours` int(11) NOT NULL DEFAULT 0,
  `extraHours` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_timesheets`
--

INSERT INTO `tbl_timesheets` (`timesheetId`, `userUniqueId`, `projectUniqueId`, `workDate`, `standardHours`, `extraHours`, `status`, `created_at`, `updated_at`) VALUES
(1, 194, 1, '2021-05-30', 0, 0, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22'),
(2, 194, 1, '2021-05-31', 8, 2, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22'),
(3, 194, 1, '2021-06-01', 8, 2, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22'),
(4, 194, 1, '2021-06-02', 8, 2, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22'),
(5, 194, 1, '2021-06-03', 8, 2, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22'),
(6, 194, 1, '2021-06-04', 8, 2, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22'),
(7, 194, 1, '2021-06-05', 0, 0, 0, '2021-06-02 05:51:22', '2021-06-02 05:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timesheets_attachments`
--

CREATE TABLE `tbl_timesheets_attachments` (
  `timeSheetAutoId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `employeeId` varchar(11) NOT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` int(11) NOT NULL,
  `weekDate` date NOT NULL,
  `attachment1` varchar(255) DEFAULT NULL,
  `attachment2` varchar(255) DEFAULT NULL,
  `weekStatus` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_timesheets_attachments`
--

INSERT INTO `tbl_timesheets_attachments` (`timeSheetAutoId`, `userUniqueId`, `employeeId`, `businessUniqueId`, `businessId`, `weekDate`, `attachment1`, `attachment2`, `weekStatus`, `created_at`) VALUES
(1, 194, 'EMP004', 0, 0, '2021-05-30', '', '', 0, '2021-06-02 05:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userUniqueId` int(11) NOT NULL,
  `userTypeCode` varchar(40) COLLATE utf8_bin NOT NULL,
  `userType` varchar(55) COLLATE utf8_bin DEFAULT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `employeeId` varchar(50) COLLATE utf8_bin NOT NULL,
  `firstName` varchar(40) COLLATE utf8_bin NOT NULL,
  `lastName` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_bin NOT NULL,
  `workPhone` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `homePhone` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `emergenctContact1` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `emergenctContact2` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `maritalStatus` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(155) COLLATE utf8_bin NOT NULL,
  `address` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(155) COLLATE utf8_bin NOT NULL,
  `test_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ssnNumber` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `aadharNumber` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `passportNumber` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `gender` enum('Male','Female','Other') COLLATE utf8_bin DEFAULT NULL,
  `profilePic` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `userStatus` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '1',
  `passwordFlag` int(11) NOT NULL DEFAULT 0,
  `createdBy` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userUniqueId`, `userTypeCode`, `userType`, `businessUniqueId`, `businessId`, `employeeId`, `firstName`, `lastName`, `mobile`, `workPhone`, `homePhone`, `emergenctContact1`, `emergenctContact2`, `maritalStatus`, `dob`, `email`, `address`, `password`, `test_password`, `ssnNumber`, `aadharNumber`, `passportNumber`, `gender`, `profilePic`, `startDate`, `endDate`, `userStatus`, `passwordFlag`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, '1', 'Super Admin', 0, '0', '', 'Naga', 'Reddy', '9676326502', NULL, NULL, NULL, NULL, NULL, '2020-08-25', 'ynreddy1989@gmail.com', '432432', 'e10adc3949ba59abbe56e057f20f883e', '123456', '1234555553', '22322322344', '5435435', NULL, 'Logo.png', NULL, NULL, '1', 1, NULL, '2020-08-08 12:20:00', '2020-08-08 12:20:00'),
(191, '3', 'HR', 1, 'BIN001', 'EMP001', 'Sruthi', 'ch M', '3333333333', NULL, NULL, NULL, NULL, NULL, '1996-06-24', 'sruthi@gmail.com', 'Hyderabad', 'e10adc3949ba59abbe56e057f20f883e', '123456', '545465454', NULL, '54545454564', NULL, NULL, '2020-06-01', '2023-06-30', '1', 1, NULL, '2021-07-15 03:10:53', '2021-07-15 03:10:53'),
(192, '5', 'Accountant', 1, 'BIN001', 'EMP002', 'Sru', 'S', '3232323232', NULL, NULL, NULL, NULL, NULL, '1992-06-30', 'account@gmail.com', 'Hyderabad', 'e10adc3949ba59abbe56e057f20f883e', '123456', '45657687', NULL, '5457655454', 'Female', NULL, '2019-06-03', '2022-06-28', '1', 1, NULL, '2021-06-02 05:56:28', '2021-06-02 05:56:28'),
(193, '6', 'VISA', 1, 'BIN001', 'EMP003', 'Visa', 'V', '4545454545', NULL, NULL, NULL, NULL, NULL, '1991-06-25', 'visa@gmail.com', 'Hyderabad', 'c9d415163d05a8505f6a716e3939fdcc', 'R9NA69RZCF', '57488798', NULL, '5454545454', 'Male', NULL, '2013-06-02', '2021-06-30', '1', 0, NULL, '2021-06-02 05:10:32', '2021-06-02 05:10:32'),
(194, '4', 'Employee', 1, 'BIN001', 'EMP004', 'Deepu', 'Ch', '5655555555', '', '', '', '', 'Married', '2001-06-25', 'deepu@gmail.com', 'Hyderabad', 'e10adc3949ba59abbe56e057f20f883e', '123456', '5454545', NULL, '576876545', 'Female', NULL, '2016-06-01', '2023-06-30', '1', 1, NULL, '2021-06-02 05:36:12', '2021-06-02 05:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `userDocumentId` int(11) NOT NULL,
  `documentKey` varchar(20) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `documentTypeId` int(11) NOT NULL,
  `documentType` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `documentStatus` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_documents`
--

INSERT INTO `user_documents` (`userDocumentId`, `documentKey`, `userUniqueId`, `documentTypeId`, `documentType`, `attachment`, `documentStatus`, `created_at`, `updated_at`) VALUES
(1, 'VQ7BKS', 191, 2, 'Passport', 'logo copy.jpg', 0, '2021-06-02 05:01:38', '2021-06-02 05:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `vendorUniqueId` int(11) NOT NULL,
  `userTypeCode` varchar(40) COLLATE utf8_bin NOT NULL,
  `userType` varchar(55) COLLATE utf8_bin DEFAULT NULL,
  `businessUniqueId` int(11) NOT NULL,
  `businessId` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `vendorId` varchar(50) COLLATE utf8_bin NOT NULL,
  `vendorFirm` varchar(40) COLLATE utf8_bin NOT NULL,
  `contactName` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_bin NOT NULL,
  `email` varchar(155) COLLATE utf8_bin NOT NULL,
  `address` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `panNumber` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `aadharNumber` varchar(155) COLLATE utf8_bin DEFAULT NULL,
  `passportNumber` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `gender` enum('Male','Female','Other') COLLATE utf8_bin DEFAULT NULL,
  `profilePic` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vendorStatus` int(2) NOT NULL DEFAULT 0,
  `createdBy` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`vendorUniqueId`, `userTypeCode`, `userType`, `businessUniqueId`, `businessId`, `vendorId`, `vendorFirm`, `contactName`, `mobile`, `email`, `address`, `panNumber`, `aadharNumber`, `passportNumber`, `gender`, `profilePic`, `description`, `vendorStatus`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, '8', 'Vendor', 1, 'BIN001', 'VEN001', 'Vendor', 'Vendor', '5454545444', 'vendor@gmail.com', 'Hyderabad', NULL, NULL, NULL, 'Female', NULL, '', 1, NULL, '2021-06-02 05:12:35', '2021-06-02 05:12:35'),
(2, '8', 'Vendor', 1, 'BIN001', 'VEN002', 'Naga', 'nnnnn', '8787858585', 'mnmn@gmail.com', '350 Lemarc st', NULL, NULL, NULL, 'Male', NULL, 'Test', 0, NULL, '2021-07-15 03:12:18', '2021-07-15 03:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_review`
--

CREATE TABLE `weekly_review` (
  `weeklyReviewId` int(11) NOT NULL,
  `userUniqueId` int(11) NOT NULL,
  `projectUniqueId` int(11) NOT NULL,
  `supervisor` varchar(155) DEFAULT NULL,
  `weekDate` date NOT NULL,
  `description` text NOT NULL,
  `reviewStatus` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`businessUniqueId`);

--
-- Indexes for table `business_documents`
--
ALTER TABLE `business_documents`
  ADD PRIMARY KEY (`businessDocumentId`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientUniqueId`);

--
-- Indexes for table `education_documents`
--
ALTER TABLE `education_documents`
  ADD PRIMARY KEY (`educationDocumentId`);

--
-- Indexes for table `employee_education_details`
--
ALTER TABLE `employee_education_details`
  ADD PRIMARY KEY (`educationId`);

--
-- Indexes for table `employee_family_details`
--
ALTER TABLE `employee_family_details`
  ADD PRIMARY KEY (`employeeFamilyId`);

--
-- Indexes for table `employee_passport_details`
--
ALTER TABLE `employee_passport_details`
  ADD PRIMARY KEY (`passportUniqueId`);

--
-- Indexes for table `employee_visa_details`
--
ALTER TABLE `employee_visa_details`
  ADD PRIMARY KEY (`visaUniqueId`);

--
-- Indexes for table `employment_details`
--
ALTER TABLE `employment_details`
  ADD PRIMARY KEY (`employmentId`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projectUniqueId`);

--
-- Indexes for table `project_documents`
--
ALTER TABLE `project_documents`
  ADD PRIMARY KEY (`projectDocumentId`);

--
-- Indexes for table `sms_history`
--
ALTER TABLE `sms_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_codes`
--
ALTER TABLE `tbl_codes`
  ADD PRIMARY KEY (`tbl_codes_id`);

--
-- Indexes for table `tbl_communications`
--
ALTER TABLE `tbl_communications`
  ADD PRIMARY KEY (`commid`);

--
-- Indexes for table `tbl_timesheets`
--
ALTER TABLE `tbl_timesheets`
  ADD PRIMARY KEY (`timesheetId`);

--
-- Indexes for table `tbl_timesheets_attachments`
--
ALTER TABLE `tbl_timesheets_attachments`
  ADD PRIMARY KEY (`timeSheetAutoId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userUniqueId`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`userDocumentId`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`vendorUniqueId`);

--
-- Indexes for table `weekly_review`
--
ALTER TABLE `weekly_review`
  ADD PRIMARY KEY (`weeklyReviewId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `businessUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `business_documents`
--
ALTER TABLE `business_documents`
  MODIFY `businessDocumentId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `clientUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `education_documents`
--
ALTER TABLE `education_documents`
  MODIFY `educationDocumentId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_education_details`
--
ALTER TABLE `employee_education_details`
  MODIFY `educationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employee_family_details`
--
ALTER TABLE `employee_family_details`
  MODIFY `employeeFamilyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_passport_details`
--
ALTER TABLE `employee_passport_details`
  MODIFY `passportUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_visa_details`
--
ALTER TABLE `employee_visa_details`
  MODIFY `visaUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employment_details`
--
ALTER TABLE `employment_details`
  MODIFY `employmentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `projectUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_documents`
--
ALTER TABLE `project_documents`
  MODIFY `projectDocumentId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms_history`
--
ALTER TABLE `sms_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbl_codes`
--
ALTER TABLE `tbl_codes`
  MODIFY `tbl_codes_id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_communications`
--
ALTER TABLE `tbl_communications`
  MODIFY `commid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_timesheets`
--
ALTER TABLE `tbl_timesheets`
  MODIFY `timesheetId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_timesheets_attachments`
--
ALTER TABLE `tbl_timesheets_attachments`
  MODIFY `timeSheetAutoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `userDocumentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `vendorUniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `weekly_review`
--
ALTER TABLE `weekly_review`
  MODIFY `weeklyReviewId` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
