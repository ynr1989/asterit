var base_url = $('#baseUrl').val(); //"http://asterit.melu.me/"
function isNumberKey(evt)
{      
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}
 
function alphaOnly(event) {
var key = event.keyCode;
return ((key >= 65 && key <= 90) || key == 8 || key == 9 || key == 32);
};


function checkAlert() {
  var r = confirm("Are you sure you want logout?");
  if (r == true) {    
  } else {
   return false;
  }  
}


$(".userStatusChange").change(function () {
    var end = this.value;
    var userStatus = $(this).prop("checked"); 
    var userUniqueId = $(this).attr('data-userUniqueId');
    if(userStatus){
        userStatus = "1";
    }else{
        userStatus = "0";
    }
    var qData = {
        userStatus : userStatus,
        userUniqueId:userUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'userStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".clientStatusChange").change(function () {
    var end = this.value;
    var clientStatus = $(this).prop("checked"); 
    var clientUniqueId = $(this).attr('data-clientUniqueId');
    if(clientStatus){
        clientStatus = "1";
    }else{
        clientStatus = "0";
    }
    var qData = {
        clientStatus : clientStatus,
        clientUniqueId:clientUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'clientStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".vendorStatusChange").change(function () {
    var end = this.value;
    var vendorStatus = $(this).prop("checked"); 
    var vendorUniqueId = $(this).attr('data-vendorUniqueId');
    if(vendorStatus){
        vendorStatus = "1";
    }else{
        vendorStatus = "0";
    }
    var qData = {
        vendorStatus : vendorStatus,
        vendorUniqueId:vendorUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'vendorStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".businessStatusChange").change(function () {
    var end = this.value;
    var businessStatus = $(this).prop("checked"); 
    var businessUniqueId = $(this).attr('data-businessUniqueId');
    if(businessStatus){
        businessStatus = "1";
    }else{
        businessStatus = "0";
    }
    var qData = {
        businessStatus : businessStatus,
        businessUniqueId:businessUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'businessStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

$(".projectStatusChange").change(function () {
    var end = this.value;
    var projectStatus = $(this).prop("checked"); 
    var projectUniqueId = $(this).attr('data-projectUniqueId');
    if(projectStatus){
        projectStatus = "1";
    }else{
        projectStatus = "0";
    }
    var qData = {
        projectStatus : projectStatus,
        projectUniqueId:projectUniqueId
    }
    $.ajax({
    type: 'POST',
    url: base_url+'projectStatusUpdate',
    data: qData,
    dataType : "text",
        success: function(response) {            
           
        }
    });
});

function testAjax(){

    $.ajax({
        url: "https://astet/insertTestAjax",
        type: "POST",
        data: {
            login_latitude:'123'
        },
        cache: false,
        success: function(dataResult){
           /* var dataResult = JSON.parse(dataResult);
            if(dataResult.statusCode==200){
                $("#butsave").removeAttr("disabled");
                $('#fupForm').find('input:text').val('');
                $("#success").show();
                $('#success').html('Data added successfully !'); 

            }
            else if(dataResult.statusCode==201){
               //alert("Error occured !");
            }*/
            console.log(dataResult);
        }
    });
}

$(".createBusiness").submit(function(){
    var businessName = $.trim($(".businessName").val());
    var ownerFirstName = $.trim($(".ownerFirstName").val());
    var ownerLastName = $.trim($(".ownerLastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var businessUrl = $.trim($(".businessUrl").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var businessStartDate = $.trim($(".businessStartDate").val());
    var businessEndDate = $.trim($(".businessEndDate").val());
    var businessStatus = $.trim($(".businessStatus").val());
    var bankName = $.trim($(".bankName").val());
    var bankAccountNumber = $.trim($(".bankAccountNumber").val());
    var routingNumber = $.trim($(".routingNumber").val());
    var dob = $.trim($(".dob").val());
    var businessLogo = $.trim($(".businessLogo").val());
    var error_msg = '';

    if(businessName == '')
    {
        error_msg = 'Please Enter Business Name';
    }else if(ownerFirstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(ownerLastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }else if(address == ''){
        error_msg = 'Please Enter Address';
    }else if(businessUrl == ''){
        error_msg = 'Please Enter businessUrl';
    }else if(businessStartDate == ''){
        error_msg = 'Please Enter Start Date';
    }else if(businessEndDate == ''){
        error_msg = 'Please Enter End Date';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        businessName : businessName,
        ownerFirstName:ownerFirstName,
        ownerLastName:ownerLastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        businessUrl:businessUrl,
        businessStartDate:businessStartDate,
        businessEndDate:businessEndDate,
        ssnNumber:ssnNumber,
        businessStatus:businessStatus,
        bankName:bankName,
        bankAccountNumber:bankAccountNumber,
        routingNumber:routingNumber,
        dob:dob,
        businessLogo:businessLogo
    }

    var form = $('form')[0];  
    var formData = new FormData(form);


    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    
    url: base_url+'saveBusiness',
    data: formData,
    type: 'POST',
    contentType: false,  
    processData: false,
    //dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createBusiness").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.href = "businessList";
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editBusiness").submit(function(){
     var exbusinessLogo = $.trim($(".exbusinessLogo").val());
    var businessUniqueId = $.trim($(".businessUniqueId").val());
    var businessName = $.trim($(".businessName").val());
    var ownerFirstName = $.trim($(".ownerFirstName").val());
    var ownerLastName = $.trim($(".ownerLastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var businessUrl = $.trim($(".businessUrl").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var businessStartDate = $.trim($(".businessStartDate").val());
    var businessEndDate = $.trim($(".businessEndDate").val());
    var businessStatus = $.trim($(".businessStatus").val());
     var bankName = $.trim($(".bankName").val());
    var bankAccountNumber = $.trim($(".bankAccountNumber").val());
    var routingNumber = $.trim($(".routingNumber").val());
    var dob = $.trim($(".dob").val());
    var businessLogo = $.trim($(".businessLogo").val());
    var error_msg = '';

    if(businessName == '')
    {
        error_msg = 'Please Enter Business Name';
    }else if(ownerFirstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(ownerLastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }else if(address == ''){
        error_msg = 'Please Enter Address';
    }else if(businessUrl == ''){
        error_msg = 'Please Enter businessUrl';
    }else if(businessStartDate == ''){
        error_msg = 'Please Enter Start Date';
    }else if(businessEndDate == ''){
        error_msg = 'Please Enter End Date';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }


    var form = $('form')[0];  
    var formData = new FormData(form);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    url: base_url+'updateBusiness',
    data: formData,
    type: 'POST',
    contentType: false,     
    processData: false,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createBusiness").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.reload();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

/*$(".editBusiness").submit(function(){
     var exbusinessLogo = $.trim($(".exbusinessLogo").val());
    var businessUniqueId = $.trim($(".businessUniqueId").val());
    var businessName = $.trim($(".businessName").val());
    var ownerFirstName = $.trim($(".ownerFirstName").val());
    var ownerLastName = $.trim($(".ownerLastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var businessUrl = $.trim($(".businessUrl").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var businessStartDate = $.trim($(".businessStartDate").val());
    var businessEndDate = $.trim($(".businessEndDate").val());
    var businessStatus = $.trim($(".businessStatus").val());
     var bankName = $.trim($(".bankName").val());
    var bankAccountNumber = $.trim($(".bankAccountNumber").val());
    var routingNumber = $.trim($(".routingNumber").val());
    var dob = $.trim($(".dob").val());
    var businessLogo = $.trim($(".businessLogo").val());
    var error_msg = '';

    if(businessName == '')
    {
        error_msg = 'Please Enter Business Name';
    }else if(ownerFirstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(ownerLastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }else if(address == ''){
        error_msg = 'Please Enter Address';
    }else if(businessUrl == ''){
        error_msg = 'Please Enter businessUrl';
    }else if(businessStartDate == ''){
        error_msg = 'Please Enter Start Date';
    }else if(businessEndDate == ''){
        error_msg = 'Please Enter End Date';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        businessUniqueId:businessUniqueId,
        businessName : businessName,
        ownerFirstName:ownerFirstName,
        ownerLastName:ownerLastName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        businessUrl:businessUrl,
        businessStartDate:businessStartDate,
        businessEndDate:businessEndDate,
        ssnNumber:ssnNumber,
        businessStatus:businessStatus,
        bankName:bankName,
        bankAccountNumber:bankAccountNumber,
        routingNumber:routingNumber,
        dob:dob,
        exbusinessLogo:exbusinessLogo,
        businessLogo:businessLogo
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateBusiness',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                //$(".createBusiness").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});*/ 

$(".createEmployee").submit(function(){

    var ajaxData = new FormData();

$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.documentType').each(function(i) {
     ajaxData.append('documentType['+i+']', $(this).val());
    });

    var firstName = $.trim($(".firstName").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var dob = $.trim($(".dob").val());
    var startDate = $.trim($(".startDate").val());
    var endDate = $.trim($(".endDate").val());
    var createUserTypeCode = $.trim($(".createUserTypeCode").val());
    var createUserType = $.trim($(".createUserType").val());
    var rUrl = createUserType.toLowerCase()+"List";
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('firstName' , firstName);
    ajaxData.append('lastName' , lastName);
    ajaxData.append('email' , email);
    ajaxData.append('mobile' , mobile);
    ajaxData.append('gender' , gender);
    ajaxData.append('address' , address);
    ajaxData.append('ssnNumber' , ssnNumber);
    ajaxData.append('passportNumber' , passportNumber);
    ajaxData.append('userStatus' , userStatus);
    ajaxData.append('dob' , dob);
    ajaxData.append('startDate' , startDate);
    ajaxData.append('endDate' , endDate);
    ajaxData.append('createUserTypeCode' , createUserTypeCode);
    ajaxData.append('createUserType' , createUserType);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveEmployee',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href = rUrl;
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


$(".editEmployee").submit(function(){

var ajaxData = new FormData();

$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.documentType').each(function(i) {
     ajaxData.append('documentType['+i+']', $(this).val());
    });

    var firstName = $.trim($(".firstName").val());
    var userUniqueId = $.trim($(".userUniqueId").val());
    var lastName = $.trim($(".lastName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var passportNumber = $.trim($(".passportNumber").val());
    var userStatus = $.trim($(".userStatus").val());
    var dob = $.trim($(".dob").val());
    var startDate = $.trim($(".startDate").val());
    var endDate = $.trim($(".endDate").val());
    var createUserType = $.trim($(".createUserType").val());
    var error_msg = '';

   if(firstName == ''){
        error_msg = 'Please Enter First Name';
    }else if(lastName == ''){
        error_msg = 'Please Enter Last Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

ajaxData.append('firstName' , firstName);
ajaxData.append('lastName' , lastName);
ajaxData.append('userUniqueId' , userUniqueId);
ajaxData.append('email' , email);
ajaxData.append('mobile' , mobile);
ajaxData.append('gender' , gender);
ajaxData.append('address' , address);
ajaxData.append('ssnNumber' , ssnNumber);
ajaxData.append('passportNumber' , passportNumber);
ajaxData.append('userStatus' , userStatus);
ajaxData.append('dob' , dob);
ajaxData.append('startDate' , startDate);
ajaxData.append('endDate' , endDate);
ajaxData.append('createUserType' , createUserType);


    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateEmployee',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.reload();
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


//Client
$(".createClient").submit(function(){
    var clientFirm = $.trim($(".clientFirm").val());
    var contactName = $.trim($(".contactName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var clientStatus = $.trim($(".clientStatus").val());
    var description = $.trim($(".description").val());
    var error_msg = '';

   if(clientFirm == ''){
        error_msg = 'Please Enter Client Firm Name';
    }else if(contactName == ''){
        error_msg = 'Please Enter Contact Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        clientFirm : clientFirm,
        contactName:contactName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        ssnNumber:ssnNumber,
        clientStatus:clientStatus,
        description:description
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveClient',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createClient").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.href = "clientList";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editClient").submit(function(){
    var clientFirm = $.trim($(".clientFirm").val());
    var clientUniqueId = $.trim($(".clientUniqueId").val());
    var contactName = $.trim($(".contactName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var ssnNumber = $.trim($(".ssnNumber").val());
    var clientStatus = $.trim($(".clientStatus").val());
     var description = $.trim($(".description").val());
    var error_msg = '';

   if(clientFirm == ''){
        error_msg = 'Please Enter Firm Name';
    }else if(contactName == ''){
        error_msg = 'Please Enter Contact Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
     $(".error_message").show();
     $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        clientFirm : clientFirm,
        contactName:contactName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
        ssnNumber:ssnNumber,
        clientStatus:clientStatus,
        clientUniqueId:clientUniqueId,
        description:description
    }
    
    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateClient',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});



//Vendor
$(".createVendor").submit(function(){
    var vendorFirm = $.trim($(".vendorFirm").val());
    var contactName = $.trim($(".contactName").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var description = $.trim($(".description").val());
    var vendorStatus = $.trim($(".vendorStatus").val());
    var error_msg = '';

   if(vendorFirm == ''){
        error_msg = 'Please Enter Firm Name';
    }else if(contactName == ''){
        error_msg = 'Please Enter Contact Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        vendorFirm : vendorFirm,
        contactName:contactName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
       description:description,
        vendorStatus:vendorStatus
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveVendor',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createVendor").trigger('reset');
                $(".spinner_icon").hide();
               // $(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    window.location.href = "vendorList";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

$(".editVendor").submit(function(){
    var vendorFirm = $.trim($(".vendorFirm").val());
    var contactName = $.trim($(".contactName").val());
    var vendorUniqueId = $.trim($(".vendorUniqueId").val());
    var email = $.trim($(".email").val());
    var mobile = $.trim($(".mobile").val());
    var gender = $.trim($(".gender").val());
    var address = $.trim($(".address").val());
    var description = $.trim($(".description").val());
    var vendorStatus = $.trim($(".vendorStatus").val());
    var error_msg = '';

   if(vendorFirm == ''){
        error_msg = 'Please Enter Firm Name';
    }else if(contactName == ''){
        error_msg = 'Please Enter Contact Name';
    }else if(email == ''){
    error_msg = 'Please Enter Email Address';
    }else if(mobile == ''){
        error_msg = 'Please Enter Mobile';
    }else if(gender == ''){
        error_msg = 'Please Enter Gender';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    var qData = {
        vendorFirm : vendorFirm,
        contactName:contactName,
        email:email,
        mobile:mobile,
        gender:gender,
        address:address,
       description:description,
        vendorStatus:vendorStatus,
        vendorUniqueId:vendorUniqueId
    }

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateVendor',
    data: qData,
    dataType : "text",
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
               // $(".editEmployee").trigger('reset');
                $(".spinner_icon").hide();
                $(".success_message").show();
                $(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                }, 3000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


/*Add more buttons*/

$(document).ready(function() {
let itemCount = 0;
$(".add-item-content").on('click', function() {     
    itemCount++;

    var products = '<select name="documentType[]" id="documentType-'+itemCount+'" required class="form-control documentType">';
        $("#documentType-0 option").each(function()
        {
            products += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
        });
        products += "</select>"; 

    let item_data_content = `<div class="item-data item-dyna row" id="item-c-`+itemCount+`">
                                <div class="form-group col-md-5 col-sm-2 col-xs-8">
                                        <label>Doc Type</label>
                                        `+products+`
                                </div>
                                
                                <div class="form-group col-md-7 col-sm-2 col-xs-8">
                                        <label>Attachment</label>
                                        <input class="form-control attachment" id="attachment-`+itemCount+`" type="file" name="attachment[]" required>
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeItem('`+itemCount+`')"></i>
                                </div>
                            </div>`;
    $(".items-list").append(item_data_content);
})
});

function removeItem(itemNum) {
console.log("itemNum", itemNum);
$("#item-c-"+itemNum).css("display", "none");
}

/*Add more code done*/

/*Add more for project docs*/

$(document).ready(function() {
let projectCount = 0;
$(".add-projectItem-content").on('click', function() {     
    projectCount++;
    let item_projectItem_content = `<div class="item-data item-dyna row" id="item-c-`+projectCount+`">
                                 <div class="form-group col-md-5 col-sm-2 col-xs-8">
                                        <label>Doc Name</label>
                                        <input class="form-control docName" id="docName-`+projectCount+`" type="text" name="docName[]" required>
                                </div>
                                
                                <div class="form-group col-md-7 col-sm-2 col-xs-8">
                                        <label>Attachment</label>
                                        <input class="form-control attachment" id="attachment-`+projectCount+`" type="file" name="attachment[]" required>
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeProjectItem('`+projectCount+`')"></i>
                                </div>
                            </div>`;
    $(".projectItems-list").append(item_projectItem_content);
})


//Spouse Data start

let itemCount = 0;
$(".add-spouse-content").on('click', function() {     
    itemCount++;

    var familyGender = '<select name="gender[]" id="gender-'+itemCount+'" required class="form-control gender">';
        $("#gender-0 option").each(function()
        {
            familyGender += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
        });
        familyGender += "</select>"; 

        var familyType = '<select name="familyType[]" id="familyType-'+itemCount+'" required class="form-control familyType">';
        $("#familyType-0 option").each(function()
        {
            familyType += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
        });
        familyType += "</select>"; 

    let item_data_content = `<div class="spouse-data item-dyna row" id="spouse-c-`+itemCount+`">
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Family Type</label>
                                        `+familyType+`
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>First Name</label>
                                        <input class="form-control firstName" placeholder="First Name" id="firstName-`+itemCount+`" type="text" name="firstName[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Last Name</label>
                                        <input class="form-control lastName" placeholder="Last Name" id=lastName-`+itemCount+`" type="text" name="lastName[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>DOB</label>
                                        <input class="form-control dob dateField" placeholder="DOB" id=dob-`+itemCount+`" type="text" name="dob[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Gender</label>
                                        `+familyGender+`
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Email</label>
                                        <input class="form-control email" placeholder="Email Address" id=email-`+itemCount+`" type="email" name="email[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Mobile</label>
                                        <input maxlength="10" class="form-control mobile" placeholder="Mobile" id=mobile-`+itemCount+`" type="text" name="mobile[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Address</label>
                                        <input class="form-control address" placeholder="Address" id="address-`+itemCount+`" type="text" name="address[]" required>
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeSpouseItem('`+itemCount+`')"></i>
                                </div>
                            </div>`;
    $(".spouse-list").append(item_data_content);
})

//end

//Kids start

let kidsCount = 0;
$(".add-kids-content").on('click', function() {  
    kidsCount++;

    var childrenGender = '<select name="childrenGender[]" id="childrenGender-'+kidsCount+'" required class="form-control childrenGender">';
        $("#childrenGender-0 option").each(function()
        {
            childrenGender += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
        });
        childrenGender += "</select>"; 

    let kids_data_content = `<div class="kids-data item-dyna row" id="kids-c-`+kidsCount+`">
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>First Name</label>
                                        <input class="form-control childrenFirstName" placeholder="First Name" id="childrenFirstName-`+kidsCount+`" type="text" name="childrenFirstName[]" required>
                                </div>
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>Last Name</label>
                                        <input class="form-control childrenlastName" placeholder="Last Name" id=childrenlastName-`+kidsCount+`" type="text" name="childrenlastName[]" required>
                                </div>
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>DOB</label>
                                        <input class="form-control childrenDob dateField" placeholder="DOB" id=childrenDob-`+kidsCount+`" type="text" name="childrenDob[]" required>
                                </div>
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>Gender</label>
                                        `+childrenGender+`
                                </div>                              
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeKidsItem('`+kidsCount+`')"></i>
                                </div>
                            </div>`;
    $(".kids-list").append(kids_data_content);
})

//end

//Education Details Start

let eduCount = 0;
$(".add-education-content").on('click', function() {  
    eduCount++;

    let kids_data_content = `<div class="education-data item-dyna row" id="education-c-`+eduCount+`">
                                <div class="form-group col-md-2 col-sm-2 col-xs-8"> 
                                        <label>Qualification Title</label>
                                        <input class="form-control qualificationTitle" placeholder="Qualification Title" id="qualificationTitle-`+eduCount+`" type="text" name="qualificationTitle[]" required>
                                </div>
                                
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Specialization</label>
                                        <input class="form-control specialization" placeholder="Specialization" id=specialization-`+eduCount+`" type="text" name="specialization[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Institution</label>
                                        <input class="form-control institution" placeholder="Institution" id=institution-`+eduCount+`" type="text" name="institution[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Start Year</label>
                                        <input class="form-control startYear dateField" placeholder="Start Year" id=startYear-`+eduCount+`" type="text" name="startYear[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>End Year</label>
                                        <input class="form-control endYear dateField" placeholder="End Year" id=endYear-`+eduCount+`" type="text" name="endYear[]" required>
                                </div>                          
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachments</label>
                                        <input class="form-control attachment1" id=attachment1-`+eduCount+`" type="file" name="attachment1[]" required>
                                </div> 
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeEduItem('`+eduCount+`')"></i>
                                </div>
                            </div>`;
    $(".education-list").append(kids_data_content);
})

//End

//Employment Details 

let employment = 0;
$(".add-employment-content").on('click', function() {  
    employment++;

    let kids_data_content = `<div class="employment-data item-dyna row" id="employment-c-`+employment+`">
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>Company Name</label>
                                        <input class="form-control companyName" placeholder="Company Name" id="companyName-`+eduCount+`" type="text" name="companyName[]" required>
                                </div>
                               <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Role</label>
                                        <input class="form-control role" placeholder="Role" id=role-`+employment+`" type="text" name="role[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Technology</label>
                                        <input class="form-control technology" placeholder="Technology" id=technology-`+employment+`" type="text" name="technology[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>From Date</label>
                                        <input class="form-control fromDate dateField" placeholder="From Date" id=fromDate-`+employment+`" type="text" name="fromDate[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>To Date</label>
                                        <input class="form-control toDate dateField" placeholder="To Date" id=toDate-`+employment+`" type="text" name="toDate[]" required>
                                </div>
                                  
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Address</label>
                                        <input class="form-control address" placeholder="address" id=address-`+employment+`" type="text" name="address[]" required>
                                </div>                        
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>Attachment1</label>
                                        <input class="form-control attachment1" id=attachment1-`+employment+`" type="file" name="attachment1[]">
                                </div> 
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>Attachment2</label>
                                        <input class="form-control attachment2" id=attachment2-`+employment+`" type="file" name="attachment2[]">
                                </div> 
                                <div class="form-group col-md-3 col-sm-2 col-xs-8">
                                        <label>Attachment3</label>
                                        <input class="form-control attachment3" id=attachment3-`+employment+`" type="file" name="attachment3[]">
                                </div> 
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeEmpItem('`+employment+`')"></i>
                                </div>
                            </div>`;
    $(".employment-list").append(kids_data_content);
})
//end

//Passport Data start

let ppCount = 0;
$(".add-passport-content").on('click', function() {     
    ppCount++;

        var familyType = '<select name="employeeFamilyId[]" id="employeeFamilyId-'+ppCount+'" required class="form-control employeeFamilyId">';
        $("#employeeFamilyId-0 option").each(function()
        {
            familyType += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
        });
        familyType += "</select>"; 

    let item_data_content = `<div class="passport-data item-dyna row" id="passport-c-`+ppCount+`">
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Family Type</label>
                                        `+familyType+`
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>First Name</label>
                                        <input class="form-control firstName" placeholder="First Name" id="firstName-`+ppCount+`" type="text" name="firstName[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Last Name</label>
                                        <input class="form-control lastName" placeholder="Last Name" id=lastName-`+ppCount+`" type="text" name="lastName[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>passportNo</label>
                                        <input class="form-control passportNo" placeholder="Passport No" id=passportNo-`+ppCount+`" type="text" name="passportNo[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Issue Date</label>
                                        <input class="form-control issueDate dateField" placeholder="issueDate" id=issueDate-`+ppCount+`" type="text" name="issueDate[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Place Of Issue</label>
                                        <input class="form-control placeOfIssue" placeholder="Place Of Issue" id=placeOfIssue-`+ppCount+`" type="text" name="placeOfIssue[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Expire Date</label>
                                        <input class="form-control expireDate dateField" placeholder="Expire Date" id=expireDate-`+ppCount+`" type="text" name="expireDate[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Country</label>
                                        <input class="form-control country" placeholder="Country" id=country-`+ppCount+`" type="text" name="country[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachment1</label>
                                        <input class="form-control attachment1" placeholder="attachment1" id=attachment1-`+ppCount+`" type="file" name="attachment1[]">
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachment2</label>
                                        <input class="form-control Attachment2" placeholder="Attachment2" id=Attachment2-`+ppCount+`" type="file" name="Attachment2[]">
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachment3</label>
                                        <input class="form-control Attachment3" placeholder="Attachment3" id=Attachment3-`+ppCount+`" type="file" name="Attachment3[]">
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removePpItem('`+ppCount+`')"></i>
                                </div>
                            </div>`;
    $(".passport-list").append(item_data_content);
})

//end

//Visa Content

let vCount = 0;
$(".add-visa-content").on('click', function() {     
    vCount++;

        var vfamilyType = '<select name="vemployeeFamilyId[]" id="vemployeeFamilyId-'+vCount+'" required class="form-control vemployeeFamilyId">';
        $("#vemployeeFamilyId-0 option").each(function()
        {
            vfamilyType += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>'
        });
        vfamilyType += "</select>"; 

    let visa_data_content = `<div class="visa-data item-dyna row" id="visa-c-`+vCount+`">
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Family Type</label>
                                        `+vfamilyType+`
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Visa Type</label>
                                        <input class="form-control visaType" placeholder="Visa Type" id="visaType-`+vCount+`" type="text" name="visaType[]" required>
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Visa Number</label>
                                        <input class="form-control visaNumber" placeholder="Visa Number" id=visaNumber-`+vCount+`" type="text" name="visaNumber[]" required>
                                </div>                               
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Issue Date</label>
                                        <input class="form-control vissueDate dateField" placeholder="Issue Date" id=vissueDate-`+vCount+`" type="text" name="vissueDate[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Place Of Issue</label>
                                        <input class="form-control vplaceOfIssue" placeholder="Place Of Issue" id=vplaceOfIssue-`+vCount+`" type="text" name="vplaceOfIssue[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Expire Date</label>
                                        <input class="form-control vexpireDate dateField" placeholder="Expire Date" id=vexpireDate-`+vCount+`" type="text" name="vexpireDate[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Country</label>
                                        <input class="form-control vcountry" placeholder="Country" id=vcountry-`+vCount+`" type="text" name="vcountry[]" required>
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachment1</label>
                                        <input class="form-control attachment1" placeholder="Attachment1" id=attachment1-`+vCount+`" type="file" name="attachment1[]">
                                </div>
                                 <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachment2</label>
                                        <input class="form-control attachment2" placeholder="Attachment2" id=attachment2-`+vCount+`" type="file" name="attachment2[]">
                                </div>
                                <div class="form-group col-md-2 col-sm-2 col-xs-8">
                                        <label>Attachment3</label>
                                        <input class="form-control attachment3" placeholder="Attachment3" id=attachment3-`+vCount+`" type="file" name="attachment3[]">
                                </div>
                                
                                <div class="col-md-4" >
                                    <i class="fa fa-minus" onclick="removeVisaItem('`+vCount+`')"></i>
                                </div>
                            </div>`;
    $(".visa-list").append(visa_data_content);
})

//end

});

removeVisaItem

function removeVisaItem(itemNum) {
console.log("itemNum", itemNum);
$("#visa-c-"+itemNum).css("display", "none");
}

function removePpItem(itemNum) {
console.log("itemNum", itemNum);
$("#passport-c-"+itemNum).css("display", "none");
}


function removeEmpItem(itemNum) {
console.log("itemNum", itemNum);
$("#employment-c-"+itemNum).css("display", "none");
}


function removeEduItem(itemNum) {
console.log("itemNum", itemNum);
$("#education-c-"+itemNum).css("display", "none");
}

function removeProjectItem(itemNum) {
console.log("itemNum", itemNum);
$("#item-c-"+itemNum).css("display", "none");
}

function removeSpouseItem(itemNum) {
console.log("itemNum", itemNum);
$("#spouse-c-"+itemNum).css("display", "none"); 
}

function removeKidsItem(itemNum) {
console.log("itemNum", itemNum);
$("#kids-c-"+itemNum).css("display", "none");
}


/*add more for project docs code end*/

$('.userDocumentsData').click(function(){  
   var userUniqueId = $(this).attr("id");  
   $.ajax({  
        url: base_url+"getUserDocuments",  
        method:"post",  
        data:{userUniqueId:userUniqueId},  
        success:function(data){  
             $('#documents_details').html(data);  
             $('#dataModal').modal("show");  
        }  
   });  
});  

$('.businessDocumentsData').click(function(){  
   var userUniqueId = $(this).attr("id");  
   $.ajax({  
        url: base_url+"getUserDocuments",  
        method:"post",  
        data:{userUniqueId:userUniqueId},  
        success:function(data){  
             $('#documents_details').html(data);  
             $('#dataModal').modal("show");  
        }  
   });  
});  

 
$('.getDocumentInPopup').click(function(){  
   var docPath = $(this).attr("id");  
    //$('#documents_details').html('<iframe src="http://localhost/asterit/assets/businessLogos/aboutkid.png" />'); 
    $('#iframe').prop('src',"");
    $('#iframe').attr('src',docPath); 
    $('#docLink').prop('href',docPath); 
    $('#dataModalPreview').modal("show"); 
});  


//Row Expand Datatable
function format(value) {
      var extradata = value.split('#');
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
       '<tr class="expandRowUser">'+
            '<th>Start Date</th>'+
            '<th>Gender</th>'+
            '<th>DOB</th>'+
            '<th>Website</th>'+
             '<th>Address</th>'+
            '<th>Bank Name</th>'+
            '<th>Account Number</th>'+
            '<th>Routing Number</th>'+   
             '<th>Plan Amount</th>'+
        '</tr>'+
        '<tr>'+
        '<td>'+extradata[6]+'</td>'+
            '<td>'+extradata[0]+'</td>'+
            '<td>'+extradata[7]+'</td>'+
            '<td>'+extradata[1]+'</td>'+
            '<td>'+extradata[2]+'</td>'+
            '<td>'+extradata[3]+'</td>'+
            '<td>'+extradata[4]+'</td>'+
            '<td>'+extradata[5]+'</td>'+
            '<td>-</td>'+
            
        '</tr>'+
        
    '</table>';

      //return '<div>Hidden Value: ' + value + '</div>';
  }
 
      var businessTable = $('#businessPrintRowExpand').DataTable({
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ], /*Search Place holder Code Start*/
        "language": {
            "lengthMenu": '_MENU_ bản ghi trên trang',
                "search": '<i class="fa fa-search"></i>',
                "searchPlaceholder": "Search...",
                "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
            }
        }
        /*Search Place holder Code End*/
      });

      // Add event listener for opening and closing details
      $('#businessPrintRowExpand').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = businessTable.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(format(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });

 
//users Data Tables
function userFormat(value) {
      var extradata = value.split('#');
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
       '<tr class="expandRowUser">'+
       '<th>Start Date</th>'+
       '<th>End Date</th>'+
            '<th>Gender</th>'+
            '<th>DOB</th>'+ 
            '<th>Address</th>'+                     
        '</tr>'+
        '<tr>'+        
            '<td>'+extradata[0]+'</td>'+
            '<td>'+extradata[1]+'</td>'+
            '<td>'+extradata[2]+'</td>'+
            '<td>'+extradata[3]+'</td>'+  
            '<td>'+extradata[4]+'</td>'+            
        '</tr>'+
        
    '</table>';

      //return '<div>Hidden Value: ' + value + '</div>';
  }
 
      var userTable = $('#userPrintRowExpand').DataTable({
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ],
        /*Search Place holder Code Start*/
        "language": {
            "lengthMenu": '_MENU_ bản ghi trên trang',
                "search": '<i class="fa fa-search"></i>',
                "searchPlaceholder": "Search...",
                "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
            }
        }
        /*Search Place holder Code End*/
      });

      // Add event listener for opening and closing details
      $('#userPrintRowExpand').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = userTable.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(userFormat(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });


//Vendor Datatables

function vendorFormat(value) {
      var extradata = value.split('#');
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
       '<tr class="expandRowUser">'+
       '<th>Gender</th>'+
       '<th>Address</th>'+
        '<th>Description</th>'+
        '<th>Created Date</th>'+               
        '</tr>'+
        '<tr>'+        
            '<td>'+extradata[0]+'</td>'+
            '<td>'+extradata[1]+'</td>'+
            '<td>'+extradata[2]+'</td>'+
            '<td>'+extradata[3]+'</td>'+            
        '</tr>'+
        
    '</table>';

      //return '<div>Hidden Value: ' + value + '</div>';
  }
 
      var table = $('#vendorPrintRowExpand').DataTable({
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ],
         /*Search Place holder Code Start*/
        "language": {
            "lengthMenu": '_MENU_ bản ghi trên trang',
                "search": '<i class="fa fa-search"></i>',
                "searchPlaceholder": "Search...",
                "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
            }
        }
        /*Search Place holder Code End*/
      });

      // Add event listener for opening and closing details
      $('#vendorPrintRowExpand').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(vendorFormat(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });


/*Project datable Start*/


function projectFormat(value) {
      var extradata = value.split('#');
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
      '<tr>'+
        '<th>Client Name</th>'+
       '<th>Mobile</th>'+
        '<th>Email</th>'+  
         '<th>Bill rate</th>'+ 
        '<th>Net Days</th>'+ 
        '<th>Description</th>'+                     
        '</tr>'+
        '<tr>'+        
            '<td>'+extradata[3]+'</td>'+
            '<td>'+extradata[4]+'</td>'+
            '<td>'+extradata[5]+'</td>'+    
             '<td>'+extradata[6]+'</td>'+ 
            '<td>'+extradata[7]+'</td>'+ 
            '<td>'+extradata[8]+'</td>'+           
        '</tr>'+
       '<tr class="expandRowUser">'+
       '<th>Vendor Name</th>'+
       '<th>Mobile</th>'+
        '<th>Email</th>'+ 
                        
        '</tr>'+
        '<tr>'+        
            '<td>'+extradata[0]+'</td>'+
            '<td>'+extradata[1]+'</td>'+
            '<td>'+extradata[2]+'</td>'+                   
        '</tr>'+
        
    '</table>';

      //return '<div>Hidden Value: ' + value + '</div>';
  }
 
      var ptable = $('#projectPrintRowExpand').DataTable({
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ],
         /*Search Place holder Code Start*/
        "language": {
            "lengthMenu": '_MENU_ bản ghi trên trang',
                "search": '<i class="fa fa-search"></i>',
                "searchPlaceholder": "Search...",
                "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
            }
        }
        /*Search Place holder Code End*/
      });

      // Add event listener for opening and closing details
      $('#projectPrintRowExpand').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = ptable.row(tr);

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(projectFormat(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });


/*Project Datatable End*/


//Create Project Start

$(".createProject").submit(function(){

    var ajaxData = new FormData();

$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });

    var vendorUniqueId = $.trim($(".vendorUniqueId").val());
    var clientUniqueId = $.trim($(".clientUniqueId").val());
    var employeeUniqueId = $.trim($(".employeeUniqueId").val());
    var projectName = $.trim($(".projectName").val());
    var projectStatus = $.trim($(".projectStatus").val());
    var startDate = $.trim($(".startDate").val());
    var endDate = $.trim($(".endDate").val());
    var billRate = $.trim($(".billRate").val());
    var netDays = $.trim($(".netDays").val());
    var description = $.trim($(".description").val());
    var error_msg = '';

   /*if(vendorUniqueId == ''){
        error_msg = 'Please Select Vendor.';
    }else */if(clientUniqueId == ''){
        error_msg = 'Please Select Client';
    }else if(employeeUniqueId == ''){
    error_msg = 'Please Select Employee';
    }else if(projectName == ''){
        error_msg = 'Please Enter Project Name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('projectName' , projectName);
    //ajaxData.append('businessUniqueId' , businessUniqueId);
    ajaxData.append('vendorUniqueId' , vendorUniqueId);
    ajaxData.append('clientUniqueId' , clientUniqueId);
    ajaxData.append('employeeUniqueId' , employeeUniqueId);
    ajaxData.append('projectStatus' , projectStatus);
    ajaxData.append('startDate' , startDate);
    ajaxData.append('endDate' , endDate);
    ajaxData.append('billRate' , billRate);
    ajaxData.append('netDays' , netDays);
    ajaxData.append('description' , description);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'saveProject',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    window.location.href =  base_url+"projectList";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});


$(".editProject").submit(function(){
    var ajaxData = new FormData();
$.each($(".attachment"), function(i, obj) {
    $.each(obj.files,function(j, file){
        ajaxData.append('attachment['+i+']', file);
    })        
});

 $('.docName').each(function(i) {
     ajaxData.append('docName['+i+']', $(this).val());
    });
   var projectUniqueId = $.trim($(".projectUniqueId").val());
    var vendorUniqueId = $.trim($(".vendorUniqueId").val());
    var clientUniqueId = $.trim($(".clientUniqueId").val());
    var employeeUniqueId = $.trim($(".employeeUniqueId").val());
    var projectName = $.trim($(".projectName").val());
    var projectStatus = $.trim($(".projectStatus").val());
    var startDate = $.trim($(".startDate").val());
    var endDate = $.trim($(".endDate").val());
    var billRate = $.trim($(".billRate").val());
    var netDays = $.trim($(".netDays").val());
    var description = $.trim($(".description").val());
    var error_msg = '';

   /*if(vendorUniqueId == ''){
        error_msg = 'Please Select Vendor.';
    }else */if(clientUniqueId == ''){
        error_msg = 'Please Select Client';
    }else if(employeeUniqueId == ''){
    error_msg = 'Please Select Employee';
    }else if(projectName == ''){
        error_msg = 'Please Enter Project Name';
    }

    if(error_msg != '')
    {
    $(".error_message").show();
    $(".error_message").html(error_msg);
    return false;
    }

    ajaxData.append('projectName' , projectName);
    ajaxData.append('projectUniqueId' , projectUniqueId);
    ajaxData.append('vendorUniqueId' , vendorUniqueId);
    ajaxData.append('clientUniqueId' , clientUniqueId);
    ajaxData.append('employeeUniqueId' , employeeUniqueId);
    ajaxData.append('projectStatus' , projectStatus);
    ajaxData.append('startDate' , startDate);
    ajaxData.append('endDate' , endDate);
    ajaxData.append('billRate' , billRate);
    ajaxData.append('netDays' , netDays);
    ajaxData.append('description' , description);

    $(".spinner_icon").show();
    $(".error_message").hide();
    $(".success_message").hide();
    $.ajax({
    type: 'POST',
    url: base_url+'updateProject',
    contentType:false,
        processData: false,
        data: ajaxData,
        success: function(response) {            
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            { 
                $(".createEmployee").trigger('reset');
                $(".spinner_icon").hide();
                //$(".success_message").show();
                //$(".success_message").html(resultData.message);
                setTimeout(function() {
                    $(".success_message").hide();
                    //window.location.reload();
                    //window.location.href =  base_url+"projectList";
                }, 1000);
            }
            else
            {
                $(".spinner_icon").hide();
                $(".error_message").show();
                $(".error_message").html(resultData.message);
                return false;
            }
        }
    });
    return false;
});

//Create project End      


// $( ".dateField" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
$('body').on('focus', ".dateField", function(){
    console.log("here")
    $(this).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
});


$('#dttable').DataTable();