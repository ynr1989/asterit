<div class="page-head-line">Create <?php echo $createUserType; ?></div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
                       
                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" class="createProject" enctype="multipart/form-data">
                 <div  class="col-md-8 col-sm-12">

                  <div class="row">

                    <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Select Employee <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="employeeUniqueId" class="form-control employeeUniqueId" required="required">
                              <option value="">Select Employee</option>
                              <?php foreach ($employeeData as $data) { ?>                                
                              <option value="<?php echo $data['userUniqueId']; ?>"><?php echo $data['firstName']." - ".$data['lastName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                     <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Select Vendor <a href="<?php echo base_url('createVendor'); ?>" class="btn tabButtons addButton" style="margin:0px;padding:0px;"><i class="fa fa-plus fa-4x"></i></a> </label>
                        <div class="form-group">
                           <select name="vendorUniqueId" class="form-control vendorUniqueId typeahead">
                              <option value="">Select Vendor</option>
                              <?php foreach ($vendorData as $data) { ?>                                
                              <option value="<?php echo $data['vendorUniqueId']; ?>"><?php echo $data['vendorFirm']." - ".$data['contactName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                       <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Select Client  <a href="<?php echo base_url('createClient'); ?>" class="btn tabButtons addButton" style="margin:0px;padding:0px;"><i class="fa fa-plus fa-4x"></i></a><span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="clientUniqueId" class="form-control clientUniqueId" required="required">
                              <option value="">Select Client</option>
                              <?php foreach ($clientData as $data) { ?>                                
                              <option value="<?php echo $data['clientUniqueId']; ?>"><?php echo $data['clientFirm']." - ".$data['contactName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                       

                  <div class="col-md-4 col-12">
                          <label class="bmd-label-floating">Project Name <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="projectName" placeholder="Project Name" maxlength="25" class="form-control projectName">
                        </div>
                      </div> 


                         <div class="col-md-4 col-sm-12">
                           <label class="bmd-label-floating">Start Date <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="startDate" autocomplete="off" id="fdate" placeholder="Start Date" class="form-control startDate">
                        </div>
                      </div>
                        
                         <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">End Date </label>
                        <div class="form-group">
                          <input type="text" name="endDate" autocomplete="off" id="ldate" placeholder="End Date" class="form-control endDate">
                        </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Bill Rate ($)</label>
                        <div class="form-group">
                          <input type="text" name="billRate" autocomplete="off" id="billRate" placeholder="Bill Rate" class="form-control billRate">
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Net Days </label>
                        <div class="form-group">
                          <input type="text" name="netDays" autocomplete="off" id="netDays" placeholder="Net Days" class="form-control netDays">
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Description</label>
                        <div class="form-group">
                          <textarea  name="description" autocomplete="off" id="description" placeholder="Description" class="form-control description"></textarea>
                        </div>
                      </div>
                    

                    </div>
                   
                      
                     
                    
                    <div class="row">
                     
                        <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Status </label>
                         
                        <div class="form-group">
                           <select name="projectStatus" required class="form-control projectStatus">
                             <!--  <option value="">Select Status</option> -->
                              <option value="0">In Active</option>
                              <option value="1">Active</option>
                          </select>
                        </div>
                      </div>   
                      </div>

            </div>
            <div  class="col-md-4 col-12">

            <div class="add-item-data" style="margin:40px 0 0 20px; float:left"><i class="fa fa-plus add-projectItem-content"></i> </div>
                    <div class="projectItems-list">
                        <div class="item-data row">                                        
                            <div class="form-group col-md-5 col-sm-12">
                                    <label>Doc Type</label>
                                    <input class="form-control docName" id="docName-0" name="docName[]" type="text">                                   
                            </div>                                        
                            <div class="form-group col-md-7 col-sm-12">
                                    <label>Attachment</label>
                                    <input class="form-control attachment" id="attachment-0" name="attachment[]" type="file">
                            </div>                                        
                        </div>
                    </div>
            </div>

            </div>

                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url($cancelUrl); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        