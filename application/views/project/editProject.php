

<div class="page-head-line">Update Project</div>

            <div id="page-inner">
                <div class="row">
                   


            <div  class="col-md-8 col-sm-12">

                       <?php if($this->session->flashdata('message')!=''): ?>
                        <div class="alert alert-success">
                           <?php echo $this->session->flashdata('message'); ?>
                           </div>
                            <?php  endif; if($this->session->flashdata('message1')!=''): ?>
                               <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('message1'); ?>
                            </div><?php
                            endif; ?>
                            
                    <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>
                    <form method="post" action="#" class="editProject" enctype="multipart/form-data">
                  <input type="hidden" name="projectUniqueId" class="projectUniqueId"  value="<?php echo $projectInfo[0]['projectUniqueId']; ?>">

					         <div class="row">
                            
                       <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Select Vendor <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="vendorUniqueId" class="form-control vendorUniqueId typeahead" required="required">
                              <option value="">Select Vendor</option>
                              <?php foreach ($vendorData as $data) { ?>                                
                              <option <?php if($projectInfo[0]['vendorUniqueId'] == $data['vendorUniqueId']): echo 'selected'; endif; ?> value="<?php echo $data['vendorUniqueId']; ?>"><?php echo $data['vendorFirm']." - ".$data['contactName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                       <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Select Client <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="clientUniqueId" class="form-control clientUniqueId" required="required">
                              <option value="">Select Client</option>
                              <?php foreach ($clientData as $data) { ?>                                
                              <option <?php if($projectInfo[0]['clientUniqueId'] == $data['clientUniqueId']): echo 'selected'; endif; ?> value="<?php echo $data['clientUniqueId']; ?>"><?php echo $data['clientFirm']." - ".$data['contactName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                       <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Select Employee <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="employeeUniqueId" class="form-control employeeUniqueId" required="required">
                              <option value="">Select Employee</option>
                              <?php foreach ($employeeData as $data) { ?>                                
                              <option <?php if($projectInfo[0]['userUniqueId'] == $data['userUniqueId']): echo 'selected'; endif; ?> value="<?php echo $data['userUniqueId']; ?>"><?php echo $data['firstName']." - ".$data['lastName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 

                  <div class="col-md-4 col-12">
                          <label class="bmd-label-floating">Project Name <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="projectName" placeholder="Project Name" maxlength="25" class="form-control projectName" value="<?php echo $projectInfo[0]['projectName']; ?>">
                        </div>
                      </div> 


                         <div class="col-md-4 col-sm-12">
                           <label class="bmd-label-floating">Start Date <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="startDate" autocomplete="off" id="fdate" placeholder="Start Date" class="form-control startDate" value="<?php echo $projectInfo[0]['startDate']; ?>">
                        </div>
                      </div>
                        
                         <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">End Date </label>
                        <div class="form-group">
                          <input type="text" name="endDate" autocomplete="off" id="ldate" placeholder="End Date" class="form-control endDate" value="<?php echo $projectInfo[0]['endDate']; ?>">
                        </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Bill Rate ($)</label>
                        <div class="form-group">
                          <input type="text" name="billRate" autocomplete="off" id="billRate" placeholder="Bill Rate" class="form-control billRate" value="<?php echo $projectInfo[0]['billRate']; ?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Net Days </label>
                        <div class="form-group">
                          <input type="text" name="netDays" autocomplete="off" id="netDays" placeholder="Net Days" class="form-control netDays" value="<?php echo $projectInfo[0]['netDays']; ?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Description</label>
                        <div class="form-group">
                          <textarea  name="description" autocomplete="off" id="description" placeholder="Description" class="form-control description"><?php echo $projectInfo[0]['description']; ?></textarea>
                        </div>
                      </div>

                    </div>
                    
                    </div>
                
                <div class="col-md-4 col-sm-12">
            
              <table class="table table-striped table-bordered table-hover">
                <tr><th>Document Type</th><th>#</th></tr>
              
                <?php $docs = $this->mainModel->getProjectDocuments($projectInfo[0]['projectUniqueId']);
                foreach($docs as $docInfo){ ?>
                 <tr><td><?php echo $docInfo['documentName']; ?></td><td> <a href="<?php echo DOCUMENT_URL; ?><?php echo $docInfo['attachment']; ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>&nbsp; &nbsp; <a href="<?php echo base_url('deleteProjectDocument/'); ?>?projectDocumentId=<?php echo $docInfo['projectDocumentId']; ?>&documentKey=<?php echo $docInfo['documentKey']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td> </tr>
                <?php } ?>
                </table>
                <div class="add-item-data" style="margin:40px 0 0 20px; float:left">
                                    <i class="fa fa-plus add-projectItem-content"></i>
                                </div>
                                <div class="projectItems-list">
                        <div class="item-data row">                                        
                            <div class="form-group col-md-5 col-sm-12">
                                    <label>Doc Type</label>
                                    <input class="form-control docName" id="docName-0" name="docName[]" type="text">                                   
                            </div>                                        
                            <div class="form-group col-md-7 col-sm-12">
                                    <label>Attachment</label>
                                    <input class="form-control attachment" id="attachment-0" name="attachment[]" type="file">
                            </div>                                        
                        </div>
                    </div>

            </div>

            </div> <div class="row">
            <div class="col-md-12 col-sm-12 mb-5">
                       
                        <div>
                <form method="post" action="#" class="editEmployee" enctype="multipart/form-data">
                  <input type="hidden" class="userUniqueId"  value="<?php echo $userInfo[0]['userUniqueId']; ?>">
                   
                    
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('projectList'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                 <br/>
                 <br/>
                                  <br/>

              </div>
            </div>
            </div>
                
                 
            
                
                
                
                
                
                    
                <!-- /. ROW  -->
              


               </form>
            
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      