                      <!--<div class="page-head-line">Dashboard</div> -->
                       <!--  <h1 class="page-subhead-line">Notifications </h1> -->
            <div id="page-inner">
                <div class="row">
                     <?php if($userTypeCode == 1): ?>
                    <div class="col-md-4 col-6 text-center shdow rounded ">
                        <div class="mb-lightblue p-3 m-1">
                            <a href="<?php echo base_url('createBusiness'); ?>">
                                 <img src="<?php echo base_url(); ?>assets/img/dashboard/AddBusiness.png" width="64" height="64">
                                <h5>Add Business</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center ">
                        <div class="mb-voilet p-3 m-1">
                            <a href="<?php echo base_url('businessList'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/BusinessList.png" width="64" height="64">
                                <h5>Business List</h5>
                            </a>
                        </div>
                    </div>
                     <?php endif; ?>
                     <?php if($userTypeCode == 2): ?>
                    <div class="col-md-4 col-6 text-center  ">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('projectList'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/hr.png" width="64" height="64">
                                <h5>Projects</h5>
                            </a>
                        </div>
                    </div> 

                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-dull  p-3 m-1">
                            <a href="<?php echo base_url('employeeList'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/employee.png" width="64" height="64">
                                <h5>Employees (<?php echo $employeeCount; ?>)</h5>
                            </a>
                        </div>
                    </div>
                    <div  class="col-md-4 col-6 text-center ">
                        <div class="mb-pink  p-3 m-1">
                            <a href="<?php echo base_url('accountantList'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/accounts.png" width="64" height="64">
                                <h5>Accountant</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-lightblue  p-3 m-1">
                            <a href="<?php echo base_url('visaList'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/visa.png" width="64" height="64">
                                <h5>Visa</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-grey  p-3 m-1">
                            <a href="<?php echo base_url('benchList'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/bench.png" width="64" height="64">
                                <h5>Bench (<?php echo $benchCount; ?>)</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center">
                        <div class="mb-voilet  p-3 m-1">
                            <a href="<?php echo base_url('vendorList'); ?>">
                                <img class="img-fluid mt-3" src="<?php echo base_url(); ?>assets/img/dashboard/vendor.png" width="64" height="64">
                                <h5>Vendors</h5>
                            </a>
                        </div>
                    </div>
                  
                  <div class="col-md-4 col-6 text-center">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('clientList'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/dashboard/client.png" width="64" height="64">
                                <h5>Clients (<?php echo $clientCount; ?>)</h5>
                            </a>
                        </div>
                    </div>
                    
                    
                    <!--<div class="col-md-2 col-sm-2 col-xs-3">
                        <div class="main-box mb-voilet">
                            <a href="<?php echo base_url('clientList'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/Logo.png" width="229" height="54">
                                <h5>Clients</h5>
                            </a>
                        </div>
                    </div>-->
                    <?php endif; ?>
                     <?php if($userTypeCode == 3): ?>
                        <div class="col-md-3 col-sm-2  col-xs-3">
                        <div class="main-box mb-dull">
                            <a href="<?php echo base_url('employeeList'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/dashboard/employee.png" width="64" height="64">
                                <h5>Employees</h5>
                            </a>
                        </div>
                    </div>
                    
                        <div class="col-md-3 col-sm-2 col-xs-3">
                        <div class="main-box mb-pink">
                            <a href="<?php echo base_url('vendorList'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/dashboard/vendor.png" width="64" height="64">
                                <h5>Vendors</h5>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-2 col-xs-3">
                        <div class="main-box mb-red">
                            <a href="<?php echo base_url('clientList'); ?>">
                                <img src="<?php echo base_url(); ?>assets/img/dashboard/client.png" width="64" height="64">
                                <h5>Clients</h5>
                            </a>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if($userTypeCode == 4): ?>
                    <div class="col-md-4 col-6 text-center shdow rounded ">
                        <div class="mb-red p-3 m-1">
                            <a href="<?php echo base_url('personalDetails'); ?>">
                                 <img src="<?php echo base_url(); ?>assets/img/dashboard/personaldetails.png" width="64" height="64">
                                <h5>Personal Info</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center ">
                        <div class="mb-dull p-3 m-1">
                            <a href="<?php echo base_url('educationDetails'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/educationdetails.png" width="64" height="64">
                                <h5>Education Details</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center ">
                        <div class="mb-pink p-3 m-1">
                            <a href="<?php echo base_url('employmentDetails'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/employeedetails.png" width="64" height="64">
                                <h5>Employment Details</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center ">
                        <div class="mb-lightblue p-3 m-1">
                            <a href="<?php echo base_url('visaDetails'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/visadetails.png" width="64" height="64">
                                <h5>Visa Details</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 text-center ">
                        <div class="mb-voilet p-3 m-1">
                            <a href="<?php echo base_url('myProjects'); ?>">
                                 <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/dashboard/myprojects.png" width="64" height="64">
                                <h5>My Projects</h5>
                            </a>
                        </div>
                    </div>
                     <?php endif; ?>
                </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->