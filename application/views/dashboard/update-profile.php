
<div class="page-head-line">Update Profile</div>
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-7 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                <form method="post" action="<?php echo base_url('profileSubmit'); ?>"  enctype="multipart/form-data">
                   
                    <div class="row">
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" required name="firstName" placeholder="Name" class="form-control" value="<?php echo $userInfo[0]['firstName']; ?>">
                        </div>
                      </div>                     
                   
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" required name="lastName" placeholder="Name" class="form-control" value="<?php echo $userInfo[0]['lastName']; ?>">
                        </div>
                      </div>                     
                    </div>

                    <div class="row">
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="text" name="email" placeholder="email" class="form-control" value="<?php echo $userInfo[0]['email']; ?>" readonly>
                        </div>
                      </div>                     
                   
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Mobile</label>
                          <input type="text" readonly name="mobile" placeholder="mobile" class="form-control" value="<?php echo $userInfo[0]['mobile']; ?>">
                        </div>
                      </div>                     
                    
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">SSN Number</label>
                          <input type="text" name="ssnNumber" placeholder="ssnNumber" class="form-control" value="<?php echo $userInfo[0]['ssnNumber']; ?>">
                        </div>
                      </div>                     
                   
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Passport Number</label>
                          <input type="text"  name="passportNumber" placeholder="passportNumber" class="form-control" value="<?php echo $userInfo[0]['passportNumber']; ?>">
                        </div>
                      </div>                     
                    
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Address</label>
                          <input type="text" name="address" placeholder="Address" class="form-control" value="<?php echo $userInfo[0]['address']; ?>">
                        </div>
                      </div>   

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Date Of Birth</label>
                          <input type="text" name="dob" placeholder="Date Of Birth" id="ldate" class="form-control" value="<?php echo $userInfo[0]['dob']; ?>">
                        </div>
                      </div>                     
                   
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><?php if($userTypeCode==1): echo "Upload Logo"; else: echo "Profile Pic"; endif;?></label>
                          <input type="file" name="profilePic" placeholder="Profile Picture" class="form-control">
                        </div>
                      </div>                     
                    </div>
                
                   <a href="<?php echo base_url('dashboard'); ?>" class="btn btn-primary pull-right">Cancel</a> 
                    <button type="submit" class="btn btn-primary pull-right" style="margin-right:5px;">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <?php if(empty($userInfo[0]['profilePic'])){
                      $profilePic = base_url()."assets/"."noimage.png";
                    }else{
                      $profilePic = base_url()."assets/profilePics/".$userInfo[0]['profilePic'];
                    } ?>
                    <img class="img" src="<?php echo $profilePic; ?>" style="width: 100%;"/>
                  </a>
                </div>
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
     
    
      <script>
       $(".interested_section").hide();
       $(".rejection_section").hide();
       $(".chkConfirmedYes_section").hide();
       $(".chkConfirmedNo_section").hide();
         $(function () {
             
        $("#chkInterested").click(function () {
            if ($(this).is(":checked")) {
                $(".interested_section").show();
                $("#chkRejected").attr("disabled", true);
                $(".rejected_reason").removeAttr("required");
            } else {
                $(".interested_section").hide();
                $("#chkRejected").removeAttr("disabled");
            }
        });
        
        $("#chkRejected").click(function () {
            if ($(this).is(":checked")) {
                $("#chkInterested").attr("disabled", true);
                 $(".next_appointment").removeAttr("required");
                  $(".status").removeAttr("required");
                $(".rejection_section").show();
            } else {
                $(".rejection_section").hide();
                $("#chkInterested").removeAttr("disabled");
            }
        });
        
        
        //Confirmed Script
        
         $("#chkConfirmedYes").click(function () {
            if ($(this).is(":checked")) {
                $(".chkConfirmedYes_section").show();
                $("#chkConfirmedNo").attr("disabled", true);
                $(".not_confirmed_reason").removeAttr("required");
            } else {
                $(".chkConfirmedYes_section").hide();
                $("#chkConfirmedNo").removeAttr("disabled");
            }
        });
        
        $("#chkConfirmedNo").click(function () {
            if ($(this).is(":checked")) {
                $("#chkConfirmedYes").attr("disabled", true);
                // $(".next_appointment").removeAttr("required");
                  $(".confirmed_closing_status").removeAttr("required");
                $(".chkConfirmedNo_section").show();
            } else {
                $(".chkConfirmedNo_section").hide();
                $("#chkConfirmedYes").removeAttr("disabled");
            }
        });
        
        //end
        
    });
      </script>