
<div class="page-head-line">Create Ticket</div>
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                <form method="post" action="<?php echo base_url('saveTicket'); ?>"  enctype="multipart/form-data">
                   
                    <div class="row">

                       <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Employee</label>
                          <select class="form-control selectpicker" name="tdata[employeeUniqueId]" required="required" title="Category">
                                <option value="">Select Employee</option>
                              <?php  
                              foreach($allemployees as $drdata){ ?>
                               <option value="<?php echo $drdata['userUniqueId']; ?>"><?php echo $drdata['firstName']." ".$drdata['lastName']; ?></option>
                               <?php } ?>
                          </select>
                        </div>
                      </div>
                      </div>

                      <div class="row">
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Subject</label>
                          <input type="text" required name="tdata[title]" placeholder="Title" class="form-control">
                        </div>
                      </div>  
                      </div>                   
                   
                         <div class="row">
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Description</label>
                          <textarea class="form-control" placeholder="Description" required name="tdata[description]"></textarea>
                        </div>
                      </div>                     
                    </div>

                    <div class="row">
                         <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Attachments</label>
                          <input type="file" required name="attachments[]" multiple="multiple" placeholder="Title" class="form-control">
                        </div>
                      </div>  
                      </div> 
                
                  
                    <button type="submit" class="btn btn-primary ">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      