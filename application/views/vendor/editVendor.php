            <div id="page-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <h1 class="page-head-line">Update Vendor</h1>
                        <!-- <h1 class="page-subhead-line">
                        <strong><?php if($this->session->flashdata('message')!=''):
                            echo $this->session->flashdata('message')."<hr>";
                            endif; ?></strong></h1> -->

                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-7 col-sm-6">
               <div class="panel panel-info">
                       <!--  <div class="panel-heading">
                           Update Vendor
                        </div> -->
                        <div class="panel-body">
                <form method="post" action="#" class="editVendor" enctype="multipart/form-data">
                  <input type="hidden" class="vendorUniqueId"  value="<?php echo $vendorInfo[0]['vendorUniqueId']; ?>">
                   

                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Business ID <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" readonly  placeholder="First Name" maxlength="25" class="form-control" value="<?php echo $vendorInfo[0]['businessId']; ?>">
                        </div>
                      </div> 
                      <div class="col-md-6">
                        <label class="bmd-label-floating">Vendor ID <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" readonly maxlength="25" class="form-control" value="<?php echo $vendorInfo[0]['vendorId']; ?>">
                        </div>
                      </div>                      
                    </div>

                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Vendor Firm <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="vendorFirm" placeholder="Vendor Firm" maxlength="25" class="form-control vendorFirm" value="<?php echo $vendorInfo[0]['vendorFirm']; ?>">
                        </div>
                      </div> 
                      <div class="col-md-6">
                        <label class="bmd-label-floating">Contact name<span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="contactName" placeholder="Contact name" maxlength="25" class="form-control contactName" value="<?php echo $vendorInfo[0]['contactName']; ?>">
                        </div>
                      </div>                      
                    </div>

                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Email <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email" value="<?php echo $vendorInfo[0]['email']; ?>">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <label class="bmd-label-floating">Mobile <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control mobile" value="<?php echo $vendorInfo[0]['mobile']; ?>">
                        </div>
                      </div>                      
                    </div>                    
                      <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Gender <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php if($vendorInfo[0]['gender'] == 'Male'): echo 'selected'; endif; ?>>Male</option>
                              <option value="Female" <?php if($vendorInfo[0]['gender'] == 'Female'): echo 'selected'; endif; ?>>Female</option>
                              <option value="Other" <?php if($vendorInfo[0]['Other'] == 'Male'): echo 'selected'; endif; ?>>Other</option>
                          </select>
                        </div>
                      </div>  
                       <div class="col-md-6">
                        <label class="bmd-label-floating">Address </label>
                        <div class="form-group">
                          <input type="text" name="address" placeholder="Address" class="form-control address" value="<?php echo $userInfo[0]['address']; ?>">
                        </div>
                      </div>                    
                    </div>
                                    
                     
                     <div class="row">
                       <div class="col-md-6">
                          <label class="bmd-label-floating">Description </label>
                        <div class="form-group">
                          <textarea name="description" id="description" class="form-control description" placeholder="Description..."><?php echo $userInfo[0]['description']; ?></textarea>
                        </div>
                      </div>   
                              

                         <div class="col-md-6">
                          <label class="bmd-label-floating">Status </label>
                        <div class="form-group">
                           <select name="vendorStatus" required class="form-control vendorStatus">
                              <option value="0" <?php if($vendorInfo[0]['vendorStatus'] == 0): echo 'selected'; endif; ?>>In Active</option>
                              <option value="1" <?php if($vendorInfo[0]['vendorStatus'] == 1): echo 'selected'; endif; ?>>Active</option>
                          </select>
                        </div>
                      </div>   
                                         
                    </div>
                    
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('vendorList'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
  
      
    