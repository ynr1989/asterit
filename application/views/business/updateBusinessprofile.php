<div class="page-head-line">Update Business</div>

            <div id="page-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <h1 class="page-head-line"></h1>
                        <!--<h1 class="page-subhead-line">-->
                        <strong><?php if($this->session->flashdata('message')!=''): ?>
                            <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div>
                          <?php  endif; ?></strong></h1> 
<!-- 
                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>
 -->
                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-9 col-sm-6">
               <div class="panel ">
                      
                        <div class="panel-body">
                <form method="post" action="<?php echo base_url('submitBusinessProfile'); ?>"  enctype="multipart/form-data">
                  <input type="hidden" class="businessUniqueId"  value="<?php echo $businessInfo[0]['businessUniqueId']; ?>">
                    <div class="row">
                          <div class="col-md-3">
                        <div class="form-group">    
                        <label class="bmd-label-floating">Business ID</label>                     
                          <input type="text" required name="businessId" placeholder="BusinessId" class="form-control" readonly="readonly"
                           value="<?php echo $businessInfo[0]['businessId']; ?>">
                        </div>
                      </div>  
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Business Name</label> 
                          <input type="text" required name="businessName" placeholder="Business Name" maxlength="40" ondrop="return false;"
        onpaste="return false;" class="form-control businessName" value="<?php echo $businessInfo[0]['businessName']; ?>">
                        </div>
                      </div>
                       <div class="col-md-4">
                        <div class="form-group">  
                         <label class="bmd-label-floating">Owner First Name</label>                      
                          <input type="text" required name="ownerFirstName" placeholder="Owner First Name" maxlength="25" class="form-control ownerFirstName" value="<?php echo $businessInfo[0]['ownerFirstName']; ?>">
                        </div>
                      </div>                       
                    </div>

                    <div class="row">
                         
                      <div class="col-md-4">
                        <div class="form-group">
                           <label class="bmd-label-floating">Owner Last Name</label>
                          <input type="text" required name="ownerLastName" placeholder="Owner Last Name" maxlength="25" class="form-control ownerLastName" value="<?php echo $businessInfo[0]['ownerLastName']; ?>">
                        </div>
                      </div>                      
                   
                         <div class="col-md-4">
                        <div class="form-group">
                           <label class="bmd-label-floating">Email Address</label>
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email" value="<?php echo $businessInfo[0]['email']; ?>">
                        </div>
                      </div>
                       <div class="col-md-4">
                        <div class="form-group">
                           <label class="bmd-label-floating">Mobile</label>
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="11" onkeypress="return isNumberKey(event)" class="form-control mobile" value="<?php echo $businessInfo[0]['mobile']; ?>">
                        </div>
                      </div>                      
                    </div>                    
                      <div class="row">
                         <div class="col-md-4">
                        <div class="form-group">
                           <label class="bmd-label-floating">Gender</label>
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php if($businessInfo[0]['gender'] == 'Male'): echo 'selected'; endif; ?>>Male</option>
                              <option value="Female" <?php if($businessInfo[0]['gender'] == 'Female'): echo 'selected'; endif; ?>>Female</option>
                              <option value="Other" <?php if($businessInfo[0]['Other'] == 'Male'): echo 'selected'; endif; ?>>Other</option>
                          </select>
                        </div>
                      </div>  
                       <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Address</label>
                          <input type="text" required name="address" placeholder="Address" class="form-control address" value="<?php echo $businessInfo[0]['address']; ?>">
                        </div>
                      </div>  

                       <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Business URL</label>
                          <input type="text" required name="businessUrl" placeholder="BusinessUrl" class="form-control businessUrl" value="<?php echo $businessInfo[0]['businessUrl']; ?>">
                        </div>
                      </div>

                    </div>
                      <div class="row">
                        
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">SSN Number</label>
                          <input type="text" required name="ssnNumber" placeholder="SSN Number" class="form-control ssnNumber" value="<?php echo $businessInfo[0]['ssnNumber']; ?>">
                        </div>
                      </div>                        
                   
                         <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Business Start Date</label>
                          <input type="text" required name="businessStartDate" autocomplete="off" id="fdate" placeholder="Business Start Date" class="form-control businessStartDate" value="<?php echo $businessInfo[0]['businessStartDate']; ?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Business End Date</label>
                          <input type="text" required name="businessEndDate" autocomplete="off" id="ldate" placeholder="Business Start Date" class="form-control businessEndDate" value="<?php echo $businessInfo[0]['businessEndDate']; ?>">
                        </div>
                      </div>                     
                    </div>

                    <div class="row">
                         <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Bank Name</label>
                          <input type="text" required name="bankName" autocomplete="off" id="bankName" placeholder="Bank Name" class="form-control bankName" value="<?php echo $businessInfo[0]['bankName']; ?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                           <label class="bmd-label-floating">Account Number</label>
                          <input type="text" required name="bankAccountNumber" autocomplete="off" id="bankAccountNumber" placeholder="Account Number" class="form-control bankAccountNumber" value="<?php echo $businessInfo[0]['bankAccountNumber']; ?>">
                        </div>
                      </div>  
                       <div class="col-md-4">
                        <div class="form-group">
                           <label class="bmd-label-floating">Routing Number</label>
                          <input type="text" required name="routingNumber" autocomplete="off" id="routingNumber" placeholder="Routing Number" class="form-control routingNumber" value="<?php echo $businessInfo[0]['routingNumber']; ?>">
                        </div>
                      </div>   
                   
                    </div>
                     
                       <div class="row">
                       
                     <div class="col-md-4">
                         <label class="bmd-label-floating">Date Of Birth</label>
                        <div class="form-group">
                          <input type="text" required name="dob" autocomplete="off" id="dob" placeholder="Date Of Birth" class="form-control dob" value="<?php echo $businessInfo[0]['dob']; ?>">
                        </div>
                      </div>

                   
                         <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Business Logo</label>
                          <input type="file" name="businessLogo" placeholder="businessLogo" class="form-control">
                        </div>
                      </div>                     
                    </div>
                    
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6">
                <div class="card-body">
                 <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <?php if(empty($businessInfo[0]['businessLogo'])){
                      $businessInfo[0]['businessLogo'] = "Asterlogo.png";
                    } ?>
                    <img class="img" src="<?php echo base_url()."assets/businessLogos/".$businessInfo[0]['businessLogo']; ?>" style="width: 100%;"/>
                  </a>
                </div>
                <!--  <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div>  -->
              </div>

                </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
       </div>
      
    