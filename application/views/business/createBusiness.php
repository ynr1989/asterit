
<div class="page-head-line">Create Business</div>
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <!-- <h1 class="page-subhead-line">
                        <strong><?php if($this->session->flashdata('message')!=''):
                            echo $this->session->flashdata('message')."<hr>";
                            endif; ?></strong></h1> -->

                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                                <div class="panel-body">
                <form method="post" action="#" class="createBusiness" enctype="multipart/form-data">
                  
                    <div class="row">
                       
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="businessName" placeholder="Business Name" maxlength="40" ondrop="return false;"
        onpaste="return false;" class="form-control businessName">
                        </div>
                      </div>                      
                    
                         <div class="col-md-3">
                        <div class="form-group">                        
                          <input type="text" required name="ownerFirstName" placeholder="Owner First Name" maxlength="25" class="form-control ownerFirstName">
                        </div>
                      </div> 
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="ownerLastName" placeholder="Owner Last Name" maxlength="25" class="form-control ownerLastName">
                        </div>
                      </div>
                       <div class="col-md-3">
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email">
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                        
                       <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control mobile" >
                        </div>
                      </div>                      
                   
                         <div class="col-md-3">
                        <div class="form-group">
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Other">Other</option>
                          </select>
                        </div>
                      </div> 

                       <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="address" placeholder="Address" class="form-control address">
                        </div>
                      </div> 
                         <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="businessUrl" placeholder="BusinessUrl" class="form-control businessUrl">
                        </div>
                      </div> 
                                          
                    </div>            

                      <div class="row">


                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="ssnNumber" placeholder="SSN Number" maxlength="9" class="form-control ssnNumber">
                        </div>
                      </div>   

                         <div class="col-md-3">
                        <div class="form-group">
                          
                          <input type="text" required name="bankName" autocomplete="off" id="bankName" placeholder="Bank Name" class="form-control bankName" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          
                          <input type="text" required name="bankAccountNumber" autocomplete="off" id="bankAccountNumber" placeholder="Account Number" class="form-control bankAccountNumber" >
                        </div>
                      </div>  
                       <div class="col-md-3">
                        <div class="form-group">
                           
                          <input type="text" required name="routingNumber" autocomplete="off" id="routingNumber" placeholder="Routing Number" class="form-control routingNumber">
                        </div>
                      </div> </div> 
                     

                   <div class="row">
                         <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" required name="businessStartDate" autocomplete="off" id="fdate" placeholder="Business Start Date" class="form-control businessStartDate">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" name="businessEndDate" autocomplete="off" id="ldate" placeholder="Business End Date" class="form-control businessEndDate">
                        </div>
                      </div>
                      <div class="col-md-3"><!-- 
                         <label class="bmd-label-floating">Date Of Birth</label> -->
                        <div class="form-group">
                          <input type="text" required name="dob" autocomplete="off" id="dob" placeholder="Date Of Birth" class="form-control dob">
                        </div>
                      </div>

                       <div class="col-md-3">
                        <div class="form-group">
                           <select name="businessStatus" required class="form-control businessStatus">
                              <option value="0">In Active</option>
                              <option value="1">Active</option>
                          </select>
                        </div>                     
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Business Logo</label>
                          <input type="file" name="businessLogo" id="businessLogo" placeholder="businessLogo" class="form-control businessLogo">
                        </div>
                      </div>   

                  </div>

                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('businessList'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div> 
              </div>
            </div>
          </div>
        </div>
      
    