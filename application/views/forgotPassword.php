<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>Welcome to Aster IT</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/signup.css" rel="stylesheet" />
<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
 @media (min-width: 768px) {
 .bd-placeholder-img-lg {
 font-size: 3.5rem;
}
}
</style>

</head>
<body>
<div class="container-fluid">

  <div class="row">
  
    <div class="col-md-8 col-sm-12" style="background-color:#adcce9" ><img align="right" class="img-fluid" src="<?php echo base_url(); ?>assets/img/login-bg.jpg" alt=""></div>
                

    <div align="center" class="col-md-4 col-sm-12 loginformbg" style="background-color:#e7f7fc; height:100vh " >
      <form class="form-signin loginForm">
             
        <img class="mb-4 mt-3 img-fluid" src="<?php echo base_url(); ?>assets/img/Logo.png"> 
        <!-- <img class="mb-4 img-fluid" src="<?php echo base_url(); ?>assets/img/forgotpassword.jpg"> -->

                     <div  class="login_popup_spinner spinnerNewBg" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_popup alert innerAlert alert-danger" style="display:none;"></div>
            <div class="success_popup alert innerAlert alert-success" style="display:none;"></div>
               
               <h4 class="mb-3 font-weight-bold">Forgot Password</h4>
        <p>Enter following details.<br/> 
					<div class="form-group input-group">
                        <input type="password" style="display:none;">
                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                        <input type="text" autocomplete="off" id="loginMobile" maxlength="30" class="form-control" placeholder="Enter Email/Mobile" required/>
                    </div>
                    <div class="form-group input-group otpField">
                        <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                            <input type="text" name="loginOtp" autocomplete="off" id="loginOtp"  maxlength="7" class="form-control loginOtp"  placeholder="Enter OTP"/>
                    </div>
                    <div class="form-group input-group pwField">
                        <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                            <input type="password" autocomplete="off" id="fpassword"  maxlength="25" class="form-control fpassword"  placeholder="Enter Password"/>
                    </div>
                    <div class="form-group input-group cpwField">
                        <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                            <input type="password" autocomplete="off" id="confirmPassword"  maxlength="25" class="form-control confirmPassword"  placeholder="Enter Confirm Password"/>
                    </div>
                    <input type="hidden" id="otpValue" value="">
                       <button type="submit" class="btn btn-success">Submit 
						 <i class="fa fa-angle-right ml5"></i></button>
                            <hr />
                         Back to Login ? <a href="<?php echo base_url(); ?>">click here
                </form>
              </div>
  </div>
</div>
</div>
</body>
</html>



<script type="text/javascript">
    $(".otpField").hide();
    $(".pwField").hide();
    $(".cpwField").hide();
    $(".loginForm").submit(function(){
    var emailMobile = $.trim($("#loginMobile").val());
    var loginOtp = $.trim($("#loginOtp").val());
    var otpValue = $.trim($("#otpValue").val());
    var password = $.trim($("#fpassword").val());
    var confirmPassword = $.trim($("#confirmPassword").val());
    var error_msg = '';

    if(emailMobile == '')
    {
    error_msg = 'Please Enter Email/Mobile Number';
    }else if(loginOtp == '' && otpValue == '1')
    {
    error_msg = 'Please Enter OTP';
    }else if(password == '' && otpValue == '2')
    {
    error_msg = 'Please Enter Password';
    }else if(confirmPassword == '' && otpValue == '2')
    {
    error_msg = 'Please Enter confirm Password';
    }else if(confirmPassword != password && otpValue == '2')
    {
    error_msg = 'Password mismatch';
    }

    if(error_msg != '')
    {
    $(".success_popup").hide(); 
    $(".error_popup").show();
    $(".error_popup").html(error_msg);
    return false;
    } 

    var qData = {
        emailMobile : emailMobile,
        loginOtp:loginOtp,
        otpValue:otpValue,
        password:password,
        confirmPassword:confirmPassword
    }

    $(".login_popup_spinner").show();
    $(".error_popup").hide();
    $.ajax({
    type: 'POST',
    url: '<?php echo base_url('submitForgotPassword'); ?>',
    data: qData,
    dataType : "text",
        success: function(response) {
            var resultData = $.parseJSON(response);
            if(resultData.status == true)
            {            
                
                $(".login_popup_spinner").hide();
                $(".error_popup").hide();
                $(".success_popup").show();
                $(".success_popup").html(resultData.message);
                if(resultData.pStatus == true){
                    $(".otpField").hide();
                    $(".pwField").hide();
                    $(".cpwField").hide();
                     $("#otpValue").val("");
                     $(".loginForm").trigger('reset');
                     setTimeout(function() {
                    window.location.replace("<?php echo base_url(); ?>");
                }, 3000); 
                }else if(resultData.otpStatus == true){
                    $(".otpField").hide();
                    $(".pwField").show();
                    $(".cpwField").show();
                    $("#otpValue").val("2");
                }else{
                    $(".otpField").show();
                    $("#otpValue").val("1");
                }  

                             
            }
            else
            {
                $(".success_popup").hide();               
                $(".login_popup_spinner").hide();
                $(".error_popup").show();
                $(".error_popup").html(resultData.message);
                return false;
            }
        }
    });
    return false;
    });

</script>