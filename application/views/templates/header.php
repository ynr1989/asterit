<!DOCTYPE html>
<html lang="en">
<?php
date_default_timezone_set('Asia/Kolkata');
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title>Aster IT</title>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/> 
    <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>  
    <input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>">
    <!-- BOOTSTRAP STYLES-->

   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/leftmenu.css" rel="stylesheet" />    
  
   <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

   


   <!--  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB44WeQWgKqCH2h6nOTuQj31S7iR3N92eA&callback=initMap"
    async defer></script> -->

    <!--Date Picker scripts-->

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
 <!--Datepicker scripts-->
    <link href='<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css' rel='stylesheet' type='text/css' />
    

    <link href="<?php echo base_url(); ?>assets/css/leftmenu.css" rel="stylesheet"> 
<style>
td.details-control {
    background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
}


.switch {
  position: relative;
  display: inline-block;
  width: 35px;
  height: 20px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 15px;
  width: 15px;
  left: 2px;
  bottom: 3px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(15px);
  -ms-transform: translateX(15px);
  transform: translateX(15px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.btntab {
    display: inline-block;
    font-weight: 600;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: 10px 5px;
    font-size: 15px;
    line-height: 1.2;
    border-radius: .45rem;
  margin:3px !important;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    text-decoration: none !important;
}


a{
  text-decoration: none !important;
}
</style>
</head>

<body>
<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars fa-3x"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#"><?php echo $businessName; ?><?php if($userTypeCode==2): echo "<br>".$businessId; endif; ?></a>
        <div id="close-sidebar">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
            alt="User picture">
        </div>
        <div class="user-info">
          <span class="user-name">
            <strong><?php echo $userName; ?></strong>
          </span>
          <span class="user-role"><?php echo $userType; ?> <?php if($employeeId): echo $employeeId; endif;  ?></span>
        
        </div>
      </div>
     
      <div class="sidebar-menu">
        <ul>
          <li>
            <a href="<?php echo base_url('dashboard'); ?>">
              <img src="<?php echo base_url(); ?>assets/img/icons/dashboard.png" width="32" height="32">
              <span>Dashboard</span>
            </a>
          </li>
          <?php if($userTypeCode == 1): ?>
          <li class="sidebar-dropdown">
            <a href="#">
              <img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Manage Business</span>
              <!-- <span class="badge badge-pill badge-danger">3</span> -->
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li><a href="<?php echo base_url('createBusiness'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"> Add Business </a></li>
                <li><a href="<?php echo base_url('businessList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/BusinessList.png" width="32" height="32">Business List</a> </li>               
              </ul>
            </div>
          </li>
           <?php endif; ?>
            <?php if($userTypeCode == 2): ?>
              <li><a href="<?php echo base_url('hrList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Manage Employees</span> </a></li>

              <li><a href="<?php echo base_url('employeeList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Manage Consultant</span> </a></li>

               <li><a href="<?php echo base_url('projectList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Manage Projects</span> </a></li>

              <li><a href="<?php echo base_url('timesheets'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Timesheets</span> </a></li>

              <li><a href="<?php echo base_url('viewWeeklyReviews'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Manage Reviews</span> </a></li>
              
           <?php endif; ?>

           <?php if($userTypeCode == 6): ?>
              <li><a href="<?php echo base_url('employeeList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Employee List</span> </a></li>
               <li><a href="<?php echo base_url('tickets'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Tickets</span> </a></li> 
           <?php endif; ?>

           <?php if($userTypeCode == 3 || $userTypeCode == 5): ?>
             <li><a href="<?php echo base_url('hrList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Manage Employees</span> </a></li>

              <li><a href="<?php echo base_url('employeeList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">
              <span>Manage Consultant</span> </a></li>
              <?php if($userTypeCode == 3): ?>
               <li><a href="<?php echo base_url('projectList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Manage Projects</span> </a></li>
            <?php endif; ?>

              <li><a href="<?php echo base_url('timesheets'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Timesheets</span> </a></li>

           <?php endif; ?>
           <?php if($userTypeCode == 4): ?>
              <li><a href="<?php echo base_url('personalDetails'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Personal Details</span> </a></li>

               <li><a href="<?php echo base_url('addTimesheet'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Add TimeSheet</span> </a></li>

              <li><a href="<?php echo base_url('addWeeklyReview'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32"></i>
              <span>Add Reviews</span> </a></li>

           <?php endif; ?>
           <?php if($userTypeCode == 2): ?>

            <li class="sidebar-dropdown">
            <a href="#">
              <img src="<?php echo base_url(); ?>assets/img/icons/myaccount.png" width="32" height="32">
              <span>My Account</span>
              </a>
            <div class="sidebar-submenu">
              <ul>
                <li><a href="<?php echo base_url('change-password'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Change Password </a></li>
                <li><a href="<?php echo base_url('updateBusinessprofile'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/updateprofile.png" width="32" height="32">Update Profile</a> </li>               
              </ul>
            </div>
          </li>
                <?php else: ?>
                   <li class="sidebar-dropdown">
                    <a href="#">
                       <img src="<?php echo base_url(); ?>assets/img/icons/myaccount.png" width="32" height="32">
                      <span>My Account</span>
                      <!-- <span class="badge badge-pill badge-danger">3</span> -->
                    </a>
                    <div class="sidebar-submenu">
                      <ul>
                        <li><a href="<?php echo base_url('change-password'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Change Password </a></li>
                        <li><a href="<?php echo base_url('updateProfile'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/updateprofile.png" width="32" height="32">Update Profile</a> </li>               
                      </ul>
                    </div>
                  </li>
                <?php endif; ?>                        
                    <li>
                        <a  href="<?php echo base_url('logout'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/LogoutLeft.png" width="32" height="32">Logout</a>
                    </li>

        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
  </nav>
  <!-- sidebar-wrapper  -->
  <main class="page-content">
  
    <div class="container-fluid hbg ">
    <div class="row ">
      <div align="left" class="col-6 col-sm-6"><img class="img-fluid ml-2" style="width: 229px;height: 54px;" src="<?php echo $businessLogo; ?>" width="229" height="54"></div>
      <div align="right" class="col-6 col-sm-6"><a  href="<?php echo base_url('logout'); ?>"><img class="img-fluid mr-2 mt-2" src="<?php echo base_url(); ?>assets/img/icons/Logout.png" width="32" height="32"></a></div>
     </div>
 </div>
 
 
 <div class="row" style="background-color:#FFF">
    <div class="col-12 col-sm-12">
     

    


  

