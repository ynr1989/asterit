<!DOCTYPE html>
<html class="no-icon-fonts" lang="en">
<head>
<?php
date_default_timezone_set('Asia/Kolkata');
?>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Aster IT</title>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/> 
    <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>  
    <input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>">
    <!-- BOOTSTRAP STYLES-->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="<?php echo base_url(); ?>assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <!--Datepicker scripts-->
    <link href='<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css' rel='stylesheet' type='text/css' />


   <!--  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB44WeQWgKqCH2h6nOTuQj31S7iR3N92eA&callback=initMap"
    async defer></script> -->

    <!--Date Picker scripts-->

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <!--end-->

 <style type="text/css">
  .mandatory-label{
    color:red;
  }
</style>


</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>"><?php echo $businessName; ?> <?php echo " - ".$businessId; ?></a>-->
            <img src="<?php echo base_url(); ?>assets/img/Logo.png" width="229" height="54"></div>

            <div class="header-right">

            <!-- <a href="my-tasks" class="btn btn-info" title="New Message"><b><?php //if($taskreadcount>0): echo $taskreadcount; endif; ?> </b><i class="fa fa-envelope-o fa-2x"></i></a> -->
                <!--  <a href="#" class="btn btn-primary" title="New Task"><b>40 </b><i class="fa fa-bars fa-2x"></i></a>-->
                <!--<a href="<?php echo base_url('logout'); ?>" class="btn btn-danger" title="Logout"><i class="fa fa-sign-out fa-2x"></i></a>-->
				<a href="<?php echo base_url('logout'); ?>"> <img src="<?php echo base_url(); ?>assets/img/icons/Logout.png" width="32" height="32"></a>

            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    
                   <!--  <li>
                        <div class="user-img-div">
                            <img src="<?php echo $businessLogo; ?>" class="img-thumbnail" /> 

                            <div class="inner-text">                             

                              &nbsp;&nbsp;&nbsp;&nbsp;
                               
                                 Role: <?php echo $userType; ?> <div id="pulse"></div><br>
                                <?php echo $userName; ?>
                            <br />
                            </div>
                        </div>
                    </li> -->
                    
                    <li>
                        <a class="<?php if($pageid == 1): echo "active-menu-top"; endif; ?>" href="<?php echo base_url('dashboard'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Dashboard.png" width="32" height="32">Dashboard</a>
                    </li>
                <?php if($userTypeCode == 1): ?>
                   <li>
                        <a href="#"><img src="<?php echo base_url(); ?>assets/img/icons/managebusiness.png" width="32" height="32"></i>Manage Business<span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level"> 
                            <li>
                                <a href="<?php echo base_url('createBusiness'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/AddBusiness.png" width="32" height="32"></i>Create Business</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('businessList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/BusinessList.png" width="32" height="32"></i>Business List</a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>

                 <?php if($userTypeCode == 2): ?>
                    <li>
                        <a class="<?php if($pageid == 1): echo "active-menu-top"; endif; ?>" href="<?php echo base_url('hrList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">Manage Users</a>
                    </li>
                     
                    
                <?php endif; ?>

                <?php if($userTypeCode == 3): ?>
                    <li>
                        <a class="<?php if($pageid == 1): echo "active-menu-top"; endif; ?>" href="<?php echo base_url('employeeList'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/managedServices.png" width="32" height="32">Manage Users</a>
                        
                    </li>
                    
                  <?php endif; ?>  
                <?php if($userTypeCode == 2): ?>
                     <li>
                        <a href="#"><img src="<?php echo base_url(); ?>assets/img/icons/Settings.png" width="32" height="32"></i>Settings <span class="fa fa-angle-down text-right"></span></a>
                         <ul class="nav nav-second-level"> 
                            <li>
                                <a href="<?php echo base_url('change-password'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Change Password</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('updateBusinessprofile'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/updateprofile.png" width="32" height="32">Update Profile</a>
                            </li>
                        </ul>
                    </li>
                <?php else: ?>
                     <li>
                        <a href="#"><img src="<?php echo base_url(); ?>assets/img/icons/Settings.png" width="32" height="32">Settings <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level"> 
                            <li>
                                <a href="<?php echo base_url('change-password'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/Changepassword.png" width="32" height="32">Change Password</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('updateProfile'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/updateprofile.png" width="32" height="32">Update Profile</a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>                        
                    <li>
                        <a  href="<?php echo base_url('logout'); ?>"><img src="<?php echo base_url(); ?>assets/img/icons/LogoutLeft.png" width="32" height="32"></i>Logout</a>
                    </li>
                </ul>
            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        

