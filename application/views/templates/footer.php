<!-- /. WRAPPER  -->

<style>
   /*.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   color: white;
   text-align: center;
   background: url(../img/headerStrips.png) no-repeat center top;
}*/


</style>

<!-- <div class="row ">
   
    <div class="col-md-12 col-sm-6 footer" style="margin-top:30px" align="right" id="footer-sec">Copyright @ 2020 Aster IT All Rights Reserved |   
       Powered by <a href="https://smydatasol.com/" target="_blank">Smydata</a> 
    </div>
    </div> --> 
    
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->

    <!-- BOOTSTRAP SCRIPTS -->

    <script src="<?php echo base_url(); ?>assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/typeahead.js" charset="UTF-8"></script>

<!--Print scripts end-->
<script type="text/javascript"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script><script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"  type="text/javascript"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"  type="text/javascript"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"  type="text/javascript"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/asterit.js" ></script>

<!-- <script src="<?php echo base_url(); ?>assets/js/buttons.print.js"  type="text/javascript"></script> -->
<!--Print scripts end-->

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div  align="left"  class="modal-header">  
                     <div align="left"><h4 class="modal-title">Documents List</h4> </div>
                <a href="#" class="close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/img/close.png" width="24" height="24" /></a>
                </div>  
                <div class="modal-body" id="documents_details">  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div> 


 <!--IFrame display-->

<div id="dataModalPreview" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div  align="left"  class="modal-header">  
                     <div align="left"><h4 class="modal-title">Document Preview</h4> 
                      <span><a href="" id="docLink" download><i class="fa fa-download" aria-hidden="true"></i></a></span></div>
                <a href="#" class="close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/img/close.png" width="24" height="24" /></a>
                </div>  
                
                <iframe id="iframe" src="" style="height: 400px;width:100%;"> 
                </iframe>
                           
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div> 

 <!--Iframe code end--> 

    <script type="text/javascript">
    $(document).ready(function () {
     var table = $('#example').DataTable( {
               "pageLength": 10,
                "pagingType": "full_numbers"
          });
        $('#example').removeClass( 'display' ).addClass('table table-striped table-bordered');


    var table = $('#printexample').DataTable({
       "aaSorting": [],
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ]
      });

    });
    </script>

    <script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });

 
 jQuery(function($){
$( "#fdate" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
$( "#ldate" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
$( "#dob" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
$( "#dob" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});
$( "#dob-1" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});

$( ".dateFld" ).datepicker({dateFormat: 'yy-mm-dd', changeYear: true});

});

 
jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
}); 

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});

});
</script>



</body>
</html>