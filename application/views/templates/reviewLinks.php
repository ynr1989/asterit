<?php
$segment = $this->uri->segment(1);
$getId = $this->input->get('userUniqueId');
$getUserInfo = '';
if($getId!=''){
	$getUserInfo = "?userUniqueId=$getId";
}
?>
<?php if($userTypeCode == 4): ?>
<a href="<?php echo base_url('addWeeklyReview'); ?><?php echo $getUserInfo; ?>" type="button" class="btntab <?php if($segment=='addWeeklyReview'): echo'btn-1'; else: echo 'btn-2'; endif; ?> tabButtons">Add Week Review</a>
<?php endif; ?>
<a href="<?php echo base_url('viewWeeklyReviews'); ?><?php echo $getUserInfo; ?>" class="btntab <?php if($segment=='viewWeeklyReviews'): echo'btn-1'; else: echo 'btn-2'; endif; ?> tabButtons">View Week Reviews</a>

<?php if($userTypeCode == 3): ?>

<a href="<?php echo base_url('employeeList'); ?>" class="btntab <?php if($segment=='myProjects'): echo'btn-1'; else: echo 'btn-2'; endif; ?> tabButtons">Go Back</a>
<?php endif; ?>
