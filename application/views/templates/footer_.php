<!-- /. WRAPPER  -->
    <div id="footer-sec">
        &copy; 2020 Aster IT <!-- | Design By : <a href="#" >Naga</a> -->
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->

    <!-- BOOTSTRAP SCRIPTS -->

    <script src="<?php echo base_url(); ?>assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/typeahead.js" charset="UTF-8"></script>

<!--Print scripts end-->
<script type="text/javascript"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script><script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"  type="text/javascript"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"  type="text/javascript"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"  type="text/javascript"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/asterit.js" ></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/buttons.print.js"  type="text/javascript"></script> -->
<!--Print scripts end-->


    <script type="text/javascript">
    $(document).ready(function () {
     var table = $('#example').DataTable( {
               "pageLength": 10,
                "pagingType": "full_numbers"
          });
        $('#example').removeClass( 'display' ).addClass('table table-striped table-bordered');


    var table = $('#printexample').DataTable({
       "aaSorting": [],
        dom: 'Bfrtip',
        dom: 'Bfrtip',
        buttons: [
          {
              extend: 'excel'
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },            
            {
              extend: 'print',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ]});

    });
    </script>

    <script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });



$( "#fdate" ).datepicker({dateFormat: 'yy-mm-dd'});
$( "#ldate" ).datepicker({dateFormat: 'yy-mm-dd'});
</script>



</body>
</html>