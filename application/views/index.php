<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>Welcome to Aster IT</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/signup.css" rel="stylesheet" />
<style>
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
 @media (min-width: 768px) {
 .bd-placeholder-img-lg {
 font-size: 3.5rem; 
}
}
</style>

</head>
<body>
<div class="container-fluid">

  <div class="row">
  
    <div class="col-md-8 col-sm-12" style="background-color:#adcce9" ><img align="right" class="img-fluid" src="<?php echo base_url(); ?>assets/img/login-bg.jpg" alt=""></div>
                

    <div align="center" class="col-md-4 col-sm-12 loginformbg" style="background-color:#e7f7fc; height:100vh " >
      <form class="form-signin loginForm">
        <img class="mb-4 mt-3 img-fluid" src="<?php echo base_url(); ?>assets/img/Logo.png">
       <!--   <img class="mb-4 img-fluid" src="<?php echo base_url(); ?>assets/img/loginimg.jpg"> -->

         <div  class="login_popup_spinner spinnerNewBg" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_popup alert innerAlert alert-danger" style="display:none;"></div>
            <div class="success_popup alert innerAlert alert-success" style="display:none;"></div>

        <h2 class="h3 mb-3 font-weight-normal">Welcome to HRM Portal</h2>
        <label for="inputEmail" class="sr-only">Email/Mobile</label>
        <input type="text" id="loginMobile" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="loginPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me">
            Remember me </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
         <p class="mt-1 mb-2 text-muted"><a href="<?php echo base_url('business-signup'); ?>">New Business ?</a></p>
        <p class="mt-1 mb-2 text-muted"><a href="<?php echo base_url('forgotPassword'); ?>">Forgot Password ?</a></p>
      </form>
    </div>
  </div>
</div>
</div>
</body>
</html>


<script type="text/javascript">
    $(".loginForm").submit(function(){
    var emailMobile = $.trim($("#loginMobile").val());
    var password = $.trim($("#loginPassword").val());
    var error_msg = '';

    if(emailMobile == '')
    {
    error_msg = 'Please Enter Email/Mobile Number';
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    }

    if(password == '')
    {
    error_msg = 'Please Enter Password';
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    }

   /* if(mobile.length <10 && mobile!="")
    {
    error_msg = 'Please enter valid mobile number';
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    }*/

    if(error_msg != '')
    {
    $(".error_popup").show();
    $(".error_popup").html(error_msg);
    return false;
    }

    var qData = {
        emailMobile : emailMobile,
        password:password
    }

    $(".login_popup_spinner").show();
    $(".error_popup").hide();
    $.ajax({
    type: 'POST',
    url: '<?php echo base_url('loginAction'); ?>',
    data: qData,
    dataType : "text",
        success: function(resultData) {
            if(resultData == 'success')
            {            
                /*$("#myModal").modal('hide');
                $("#myModal-otp").modal('show');
                setTimeout(function() {
                    $(".resendotp").show();
                }, 3000);*/
                window.location.reload();
            }
            else
            {
                $(".login_popup_spinner").hide();
                $(".error_popup").show();
                $(".error_popup").html(resultData);
                return false;
            }
        }
    });
    return false;
    });

</script>
