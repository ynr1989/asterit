<style type="text/css">

  </style>
  <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                      

                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                          <div class="row">
<?php
$editedDate = $_GET['date'];
     ?>
   
      <div class="col-md-2 col-sm-6">
         <a href="#">
    <time style="margin:0px" datetime="2014-09-20" class="icon" style="background: #beadad;">
  <em>Week<?php //echo date('D', $time=strtotime($ddd)); ?></em>
  <strong><?php echo date('M', $time=strtotime($editedDate)); ?></strong>
  <span><?php echo date('d', $time=strtotime($editedDate)); ?></span>
</time></a></div>


   </div>
  <?php if($_GET['date']) {
  $myProjects = $this->mainModel->getMyProjects($loginUniqueId);
  $projectDetails = $this->mainModel->getTimeSheetDataByDate($loginUniqueId,$_GET['date']);
   ?>
    <table style="width:100%">
  <form method="post" action="<?php echo base_url('saveTimesheet'); ?>" class="adddTimesheet" enctype="multipart/form-data">
 <div class="row" >
     <div class="col-md-3 col-sm-12"><br>
                          <label class="bmd-label-floating">Select Project <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="projectUniqueId" class="form-control" required="required">
                              <!-- <option value="">Select Project</option> -->
                              <?php foreach ($myProjects as $projectInfo) { ?>
                              <option value="<?php echo $projectInfo['projectUniqueId']; ?>" <?php if($projectDetails['projectUniqueId'] == $projectInfo['projectUniqueId']): echo 'selected'; endif; ?> ><?php echo $projectInfo['projectName']; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>  
               </div>       
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">
    <?php $totalStandardHours = 0;
    $totalOtHours = 0;
    $totalHours = 0;
$th1="<tr>";
$td1="<tr>";
$td2="<tr>"; ?>
  

                                <?php 
                                $th1.= "<th>Date</th>";
                                 $td1.= "<td>ST/Hr</td>";
                                 $td2.= "<td>OT/Hr </td>";
                                 for($i=0;$i<7;$i++){
                                  $currentDate = $_GET['date'];
                                  $nextDate = date('Y-m-d', strtotime($currentDate. " + $i days")); 
                                  $week = date('D', strtotime($currentDate. " + $i days")); 
                                  $timesheetData = $this->mainModel->getTimeSheetDataByDate($loginUniqueId,$nextDate);
                                  $defaultValue = '';
                                  if($week == 'Sun' || $week == 'Sat'){
                                    $bColor = 'background-color:orange';
                                    $defaultValue = "0";
                                  }else{
                                    $bColor = '';
                                  }

                                  if(empty($timesheetData['standardHours'])){
                                    $timesheetData['standardHours'] = $defaultValue;
                                  }

                                  if(empty($timesheetData['extraHours'])){
                                    $timesheetData['extraHours'] = $defaultValue;
                                  }

                                  $totalStandardHours += $timesheetData['standardHours'];
                                  $totalOtHours += $timesheetData['extraHours'];
                                 /*  $th1.="<th scope='col' id='dateheader'>".$nextDate."</th>";
    $td1.="<td>".$timesheetData['standardHours']."</td>";
    $td2.="<td>".$timesheetData['extraHours']."</td>";*/
   
     ?>
                                        <input type="hidden" name="timesheetId[]" value="<?php echo $timesheetData['timesheetId'];?>">
                                         <input value="<?php echo $nextDate; ?>" type="hidden" name="workDate[]">
                                        <div class="row" >
                                       <?php $th1.= ' <th scope="col" id="dateheader" style="'.$bColor.'"><div class="form-group">
                                               '.$nextDate ."<br>$week".'
                                        </div></th>'; 
                                        $td1.= '<td> <div class="form-group">
                                                <input class="form-control shours thours" value= "'.$timesheetData['standardHours'].'" placeholder="Standard Hours" name="standardHours[]" type="text" maxlength="2" onkeypress="return isNumberKey(event)" required>
                                        </div></td>';
                                        $td2.= '<td><div class="form-group">
                                                <input class="form-control othours thours" value="'.$timesheetData['extraHours'].'" placeholder="OT" name="extraHours[]" type="text" maxlength="2" onkeypress="return isNumberKey(event)">
                                        </div></td>' ; ?>
                                    </div>
                                        <?php } 
                                        $totalHours = $totalStandardHours+$totalOtHours;
                                        $th1.= ' <th scope="col" id="dateheader" border="'.$bColor.'"><div class="form-group">
                                               Total
                                        </div></th>'; 
                                         $td1.= '<td> <div class="form-group">
                                                <input class="form-control shoursTotal" value="'.$totalStandardHours.'" readonly="readonly">
                                        </div></td>';
                                         $td2.= '<td><div class="form-group">
                                                <input class="form-control otTotalhours" value="'.$totalOtHours.'" placeholder="OT">
                                        </div></td>' ;
                                        $th1.="</tr>";
$td1.="</tr>";
$td2.="</tr>";

$td3.= '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>Total Hours</td><td class="totalhours">'.$totalHours.'</td></t6>';

echo $result="<table id='syncresults' border='1' class='table-responsive'>".$th1.$td1.$td2.$td3."</table>";
$attachmentData = $this->mainModel->getWeekAttachment($_GET['date'],$loginUniqueId);
 ?>  
  <br> 
  <input type="hidden" name="attachment1Old" value="<?php echo $attachmentData[0]['attachment1']; ?>">
  <input type="hidden" name="attachment2Old" value="<?php echo $attachmentData[0]['attachment2']; ?>">
<div class="row">
  <div class="col-md-3 col-sm-12">
    <input type="hidden" name="weekDate" value="<?php echo $_GET['date']; ?>">
    <label>Attachment 1</label>
 <input type="file" name="attachment1" class="form-control">
</div>
<div class="col-md-3 col-sm-12">
  <label>Attachment 2</label>
  <input type="file" name="attachment2" class="form-control">
  </div>
  
                                 
                                       <div class="col-md-4 col-sm-12"><br>
                                         <button type="submit" class="btn btn-primary pull-right" style="margin-left:40px;">Submit</button>
                    <a href="<?php echo base_url('viewTimesheet'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </div>
                                   </form> 
                                   </table>    
                               <?php } ?>
</div>
</div>
</div>

<script type="text/javascript">
$(document).on("change", ".shours", function() {
    var sum = 0;
    $(".shours").each(function(){
        sum += +$(this).val();
    });
    $(".shoursTotal").val(sum);
});

$(document).on("change", ".othours", function() {
    var sum = 0;
    $(".othours").each(function(){
        sum += +$(this).val();
    });
    $(".otTotalhours").val(sum);
});

$(document).on("change", ".thours", function() {
    var sum = 0;
    $(".thours").each(function(){
        sum += +$(this).val();
    });
    $(".totalhours").html(sum);
});


$(".adddTimesheet").submit(function(){
    var shoursTotal = $.trim($(".shoursTotal").val());
    var msg = '';
    if(shoursTotal<40){
      var msg = "Hours is lessthan 40. is it ok?";
    }
    if(shoursTotal>40){
      var msg = "Hours is greaterthan 40. is it ok?";
    }
    var c = confirm(msg);
    return c; 
});
</script>