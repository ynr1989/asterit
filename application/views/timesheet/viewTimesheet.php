<style type="text/css">


time.icon
{
  font-size: 1em; /* change icon size */
  display: block;
  position: relative;
  width: 7em;
  height: 7em;
  background-color: #fff;
  margin: 2em auto; 
  border-radius: 0.6em;
  box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
  overflow: hidden;
  -webkit-backface-visibility: hidden;
  -webkit-transform: rotate(0deg) skewY(0deg);
  -webkit-transform-origin: 50% 10%;
  transform-origin: 50% 10%;
}

time.icon *
{
  display: block;
  width: 100%;
  font-size: 1em;
  font-weight: bold;
  font-style: normal;
  text-align: center;
}

time.icon strong
{
  position: absolute;
  top: 0;
  padding: 0.4em 0;
  color: #fff;
  background-color: #fd9f1b;
  border-bottom: 1px dashed #f37302;
  box-shadow: 0 2px 0 #fd9f1b;
}

time.icon em
{
  position: absolute;
  bottom: 0.3em;
  color: #fd9f1b;
}

time.icon span
{
  width: 100%;
  font-size: 2.8em;
  letter-spacing: -0.05em;
  padding-top: 1.1em;
  color: #2f2f2f;
}

time.icon:hover, time.icon:focus
{
  -webkit-animation: swing 0.6s ease-out;
  animation: swing 0.6s ease-out;
}

@-webkit-keyframes swing {
  0%   { -webkit-transform: rotate(0deg)  skewY(0deg); }
  20%  { -webkit-transform: rotate(12deg) skewY(4deg); }
  60%  { -webkit-transform: rotate(-9deg) skewY(-3deg); }
  80%  { -webkit-transform: rotate(6deg)  skewY(-2deg); }
  100% { -webkit-transform: rotate(0deg)  skewY(0deg); }
}

@keyframes swing {
  0%   { transform: rotate(0deg)  skewY(0deg); }
  20%  { transform: rotate(12deg) skewY(4deg); }
  60%  { transform: rotate(-9deg) skewY(-3deg); }
  80%  { transform: rotate(6deg)  skewY(-2deg); }
  100% { transform: rotate(0deg)  skewY(0deg); }
}

th,td{
  text-align: center !important;
}
  </style>
  <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                      <?php  if($userTypeCode == 4 || $userTypeCode == 3): ?>
                       <?php $this->load->view('./templates/timeSheetLinks',$data); ?>
                     <?php endif; ?>
                     <?php if($userTypeCode !=4 ): ?>
                       <?php $this->load->view('./templates/personalLinks',$data); ?>
                     <?php endif; ?>
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                         
                                <table class="table table-striped table-bordered table-hover" id="example">
                                  <thead>
<tr><th>Client</th><th>Start Date</th><th>End Date</th><th>ST</th><th>OT</th><th>Attachments</th><th>Status</th>
  <?php if($userTypeCode == 4){ ?>
<th>Actions</th>
<?php } ?>
</tr>
</thead>
<tbody>
<?php


$date = date("Y-m-");
//$str = '2020-08-';
$str = $date;

if($_GET['userUniqueId']){
  $loginUniqueId = $_GET['userUniqueId'];
}

$clientInfo = $this->mainModel->getProjectClientInfo($loginUniqueId);
  echo "<strong>Project Start Date: </strong>".$clientInfo[0]['startDate']." &nbsp;&nbsp; "."<strong>End Date : </strong>".$clientInfo[0]['startDate']."</br></br>";
  for($i2=1; $i2<31; $i2++)
  {
 

    // echo '<br>',
      $ddd = $str.$i2;
    // echo '',
      $date = date('Y M D d', $time = strtotime($ddd) );

    //if( strpos($date, 'Sat') || strpos($date, 'Sun') )
    if(strpos($date, 'Sun') )
    {
      $date1 = date('Y M D d', $time = strtotime($ddd) );
      $sdate = date('Y-m-d', $time = strtotime($ddd) ); ?>
    <?php //echo $date1;

 $timeSheetData = $this->mainModel->getTimeSheetsDataByWeek($loginUniqueId,$sdate); 
 
 $endDate = date('Y-m-d', $time=strtotime($ddd. ' + 6 days'));

$attachmentData = $this->mainModel->getWeekAttachment(date('Y-m-d', $time=strtotime($ddd)),$loginUniqueId);
     ?>
   <tr><td><?php echo $clientInfo[0]['clientFirm']; ?></td>
    <td><?php echo date('Y-m-d', $time=strtotime($ddd)); ?></td>
    <td><?php echo  $endDate; ?></td>
    <td><?php echo $timeSheetData['totalHours']; ?></td>
  <td><?php echo $timeSheetData['totalExtraHours']; ?></td>
  
<td><?php
  if($attachmentData[0]['attachment1'] != '' ):
  $document = base_url()."assets/timesheetAttachments/".$attachmentData[0]['attachment1']; ?>
  <a href="#"><span class="getDocumentInPopup" id="<?php echo $document; ?>"><i class="fa fa-eye" aria-hidden="true"></i></span></a>

  <?php endif;
if($attachmentData[0]['attachment2']!=""):
  $document1 = base_url()."assets/timesheetAttachments/".$attachmentData[0]['attachment2']; ?>
  <a href="#"><span class="getDocumentInPopup" id="<?php echo $document1; ?>"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
<?php endif; ?>
</td>
<td><?php if(($userTypeCode == 2 || $userTypeCode == 3 || $userTypeCode == 5) && $attachmentData[0]['timeSheetAutoId']!=""){ 
   if($attachmentData[0]['weekStatus'] == 0){ ?>
                   <a href="<?php echo base_url('approveComment'); ?>?timeSheetAutoId=<?php echo $attachmentData[0]['timeSheetAutoId']; ?>" style="text-decoration: none;">Approve</a>
                <?php }else{ ?>
                        <a href="<?php echo base_url('RejectComment'); ?>?timeSheetAutoId=<?php echo $attachmentData[0]['timeSheetAutoId']; ?>" style="text-decoration: none;">Cancel</a>
                <?php } 
  }else{
    if($attachmentData[0]['timeSheetAutoId']!=""){
       if($attachmentData[0]['weekStatus'] == 0):echo "Pending"; else: echo "Approved"; endif;
    }
   
  }
  
   ?></td>
<?php if($userTypeCode == 4){ ?>
  <td>
    <?php  if($attachmentData[0]['weekStatus'] == 0): ?>
<a href="<?php echo base_url('editTimesheet'); ?>?date=<?php echo date('Y-m-d', $time=strtotime($ddd)); ?>"><i class="fa fa-edit"></i></a>
<?php else: ?>
<a href="<?php echo base_url('checkTimesheet'); ?>?date=<?php echo date('Y-m-d', $time=strtotime($ddd)); ?>"><i class="fa fa-eye"></i></a>
  <?php endif; echo ' </td>'; } ?>
 
</tr> 
     


   <?php } 
  }
  ?>
  </tbody>
</table>

  <?php if($_GET['date']) {
  $myProjects = $this->mainModel->getMyProjects($loginUniqueId);
  $projectDetails = $this->mainModel->getTimeSheetDataByDate($loginUniqueId,$_GET['date']);
   ?>
           

                               <?php } ?>
</div>
</div>
</div>