<style type="text/css">


time.icon
{
  font-size: 1em; /* change icon size */
  display: block;
  position: relative;
  width: 7em;
  height: 7em;
  background-color: #fff;
  margin: 2em auto;
  border-radius: 0.6em;
  box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
  overflow: hidden;
  -webkit-backface-visibility: hidden;
  -webkit-transform: rotate(0deg) skewY(0deg);
  -webkit-transform-origin: 50% 10%;
  transform-origin: 50% 10%;
}

time.icon *
{
  display: block;
  width: 100%;
  font-size: 1em;
  font-weight: bold;
  font-style: normal;
  text-align: center;
}

time.icon strong
{
  position: absolute;
  top: 0;
  padding: 0.4em 0;
  color: #fff;
  background-color: #fd9f1b;
  border-bottom: 1px dashed #f37302;
  box-shadow: 0 2px 0 #fd9f1b;
}

time.icon em
{
  position: absolute;
  bottom: 0.3em;
  color: #fd9f1b;
}

time.icon span
{
  width: 100%;
  font-size: 2.8em;
  letter-spacing: -0.05em;
  padding-top: 1.1em;
  color: #2f2f2f;
}

time.icon:hover, time.icon:focus
{
  -webkit-animation: swing 0.6s ease-out;
  animation: swing 0.6s ease-out;
}

@-webkit-keyframes swing {
  0%   { -webkit-transform: rotate(0deg)  skewY(0deg); }
  20%  { -webkit-transform: rotate(12deg) skewY(4deg); }
  60%  { -webkit-transform: rotate(-9deg) skewY(-3deg); }
  80%  { -webkit-transform: rotate(6deg)  skewY(-2deg); }
  100% { -webkit-transform: rotate(0deg)  skewY(0deg); }
}

@keyframes swing {
  0%   { transform: rotate(0deg)  skewY(0deg); }
  20%  { transform: rotate(12deg) skewY(4deg); }
  60%  { transform: rotate(-9deg) skewY(-3deg); }
  80%  { transform: rotate(6deg)  skewY(-2deg); }
  100% { transform: rotate(0deg)  skewY(0deg); }
}

th,td{
  text-align: center !important;
}
  </style>
  <div id="page-inner">
                 
                   <form>

            <div class="row">
                  
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="userUniqueId" id="userUniqueId" title="userUniqueId" required="required"> 
                        <option value="">Select Employee</option>
                        <?php foreach($employeeData as $empdata): ?>
                          <option <?php if(isset($_GET) && $_GET['userUniqueId'] == $empdata['userUniqueId']): echo "selected"; endif; ?>  value="<?=$empdata['userUniqueId']?>"><?=$empdata['employeeId']." - ".ucwords($empdata['firstName'])." ".ucwords($empdata['lastName'])?></option>
                        <?php endforeach; ?>
                  </select>
                </div>
              </div> 

              <div class="col-md-2">
                <div class="form-group">
                   <button type="submit" class="btn btn-primary ">Submit</button>
                  <a href="<?php echo base_url('viewWeeklyReviews'); ?>" class="btn btn-primary pull-right" role="button">Reset</a>
                </div>
              </div>

            </div>
           
          </form> 

                <div class="row">
                    <div class="col-md-12">
                      <?php  if($userTypeCode == 4 || $userTypeCode == 3): ?>
                       <?php $this->load->view('./templates/reviewLinks',$data); ?>
                     <?php endif; ?>
                     <?php if($userTypeCode !=4 ): ?>
                       <?php //$this->load->view('./templates/personalLinks',$data); ?>
                     <?php endif; ?>
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                         
                                <table class="table table-striped table-bordered table-hover" id="example">
                                  <thead>
<tr><th>Client</th><th>Start Date</th>
  <th>End Date</th>
  <th>Description</th><th>Status</th>
  <?php if($userTypeCode == 4){ ?>
<th>Actions</th>
<?php } ?></tr>
</thead>
<tbody>
<?php
if($_GET['userUniqueId']){
  $loginUniqueId = $_GET['userUniqueId'];
}
 
$getWeeklyReviewData = $this->mainModel->getWeeklyReviewUsersData($loginUniqueId); 

$clientInfo = $this->mainModel->getProjectClientInfo($loginUniqueId);
  echo "<strong>Start Date: </strong>".$clientInfo[0]['startDate']." <br> "."<strong>End Date : </strong>".$clientInfo[0]['startDate']."</br>";
  foreach($getWeeklyReviewData as $data)
  {
     ?>
   <tr><td><?php echo $clientInfo[0]['clientFirm']; ?></td>
    <td><?php echo $data['weekDate']; ?></td>
    <td><?php echo date('Y-m-d', $time=strtotime($data['weekDate']. ' + 6 days')); ?></td>
    <td><?php echo $data['description']; ?></td>
  <td><?php 
if(($userTypeCode == 2 || $userTypeCode == 3 || $userTypeCode == 5)){ 
   if($data['reviewStatus'] == 0){ ?>
                   <a href="<?php echo base_url('approveReview'); ?>?weeklyReviewId=<?php echo $data['weeklyReviewId']; ?>" style="text-decoration: none;">Approve</a>
                <?php }else{ ?>
                        <a href="<?php echo base_url('RejectReview'); ?>?weeklyReviewId=<?php echo $data['weeklyReviewId']; ?>" style="text-decoration: none;">Cancel</a>
                <?php } 
  }else{    
       if($data['reviewStatus'] == 0):echo "Pending"; else: echo "Approved"; endif;
  }
   ?></td>
<?php if($userTypeCode == 4){ ?>
  <td>
    <?php  if($data['reviewStatus'] == 0): ?>
<a href="<?php echo base_url('addWeeklyReview'); ?>?date=<?php echo date('Y-m-d', $time=strtotime($data['weekDate'])); ?>"><i class="fa fa-edit"></i></a>
<?php else: ?>
<a href="<?php echo base_url('addWeeklyReview'); ?>?date=<?php echo date('Y-m-d', $time=strtotime($data['weekDate'])); ?>"><i class="fa fa-eye"></i></a>
  <?php endif; echo ' </td>'; } ?>
</tr>
<?php }
  ?>
  </tbody>
</table>

</div>
</div>
</div>