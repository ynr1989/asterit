<style type="text/css">

  </style>
  <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                       <?php $this->load->view('./templates/timeSheetLinks',$data); ?>

                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                          <div class="row">
<?php
//echo date('Y-m-d',strtotime('last sunday'))."<br>";
//echo "Last Last Sunday". date('Y-m-d',strtotime('last sunday -7 days')); 
  $sun_days = array();
  $sun_days_time = array();
  $lm_date = date("Y-m-",strtotime("-1 month"));
  $str1 = $lm_date;

  for($i2=15; $i2<31; $i2++)
  {
    // echo '<br>',
      $ddd = $str1.$i2;
    // echo '',
      $date = date('Y M D d', $time = strtotime($ddd) );

    //if( strpos($date, 'Sat') || strpos($date, 'Sun') )
    if(strpos($date, 'Sun') )
      //echo date("Y-m-d") .">=".date('Y-m-d', $time = strtotime($ddd))."<br>";
    //if(strpos($date, 'Sun') && date("Y-m-d") >= date('Y-m-d', $time = strtotime($ddd)))
    {
      $date1 = date('Y M D d', $time = strtotime($ddd) );
      $sdate = date('Y-m-d', $time = strtotime($ddd) ); 
      array_push($sun_days, $sdate);
      array_push($sun_days_time, $ddd);
    }
  }
  $sun_days = array_slice($sun_days, -2);
  $sun_days_time = array_slice($sun_days_time, -2);

  $date = date("Y-m-");
//$str = '2020-08-';
$str = $date;
  for($i2=1; $i2<31; $i2++)
  {
    // echo '<br>',
      $ddd = $str.$i2;
    // echo '',
      $date = date('Y M D d', $time = strtotime($ddd) );

    //if( strpos($date, 'Sat') || strpos($date, 'Sun') )
    if(strpos($date, 'Sun') )
      //echo date("Y-m-d") .">=".date('Y-m-d', $time = strtotime($ddd))."<br>";
    //if(strpos($date, 'Sun') && date("Y-m-d") >= date('Y-m-d', $time = strtotime($ddd)))
    {
      $date1 = date('Y M D d', $time = strtotime($ddd) );
      $sdate = date('Y-m-d', $time = strtotime($ddd) );
      array_push($sun_days, $sdate);
      array_push($sun_days_time, $ddd);
       ?>
    <?php }
  }
  ?>
    <?php for($su_index = 0; $su_index < count($sun_days); $su_index++){
      $hereDate = date('Y-m-d', $time=strtotime($sun_days_time[$su_index]));
$sHoursByDates = $this->mainModel->getStandardHoursByDates($loginUniqueId,$hereDate, date('Y-m-d', strtotime($hereDate. " + 7 days")) );
if($sHoursByDates<=0):
     ?>
   
      <div class="col-md-2 col-sm-6">
         <a href="<?php echo base_url('addTimesheet'); ?>?date=<?php echo $sun_days[$su_index]; ?>">
    <time style="margin:0px" datetime="2014-09-20" class="icon" <?php if($sun_days[$su_index] == $_GET['date']): echo' style="background: #beadad;"'; endif; ?>>
  <em>Week<?php //echo date('D', $time=strtotime($ddd)); ?></em>
  <strong><?php echo date('M', $time=strtotime($sun_days_time[$su_index])); ?></strong>
  <span><?php echo date('d', $time=strtotime($sun_days_time[$su_index])); ?></span>
</time></a></div>
<?php endif; } ?>

   </div>
  <?php if($_GET['date']) {
  $myProjects = $this->mainModel->getMyProjects($loginUniqueId);
  $projectDetails = $this->mainModel->getTimeSheetDataByDate($loginUniqueId,$_GET['date']);
   ?>
    <table style="width:100%">
  <form method="post" action="<?php echo base_url('saveTimesheet'); ?>" class="adddTimesheet" enctype="multipart/form-data">
 <div class="row" >
     <div class="col-md-3 col-sm-12"><br>
                          <label class="bmd-label-floating">Select Project <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="projectUniqueId" class="form-control" required="required">
                              <!-- <option value="">Select Project</option> -->
                              <?php foreach ($myProjects as $projectInfo) { ?>
                              <option value="<?php echo $projectInfo['projectUniqueId']; ?>" <?php if($projectDetails['projectUniqueId'] == $projectInfo['projectUniqueId']): echo 'selected'; endif; ?> ><?php echo $projectInfo['projectName']; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>  
               </div>       
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">
    <?php $totalStandardHours = 0;
    $totalOtHours = 0;
    $totalHours = 0;
$th1="<tr>";
$td1="<tr>";
$td2="<tr>";   
                                $th1.= "<th>Date</th>";
                                 $td1.= "<td>ST/Hr</td>";
                                 $td2.= "<td>OT/Hr </td>";
                                 for($i=0;$i<7;$i++){
                                  $currentDate = $_GET['date'];
                                  $nextDate = date('Y-m-d', strtotime($currentDate. " + $i days")); 
                                  $week = date('D', strtotime($currentDate. " + $i days")); 
                                  $timesheetData = $this->mainModel->getTimeSheetDataByDate($loginUniqueId,$nextDate);
                                  $defaultValue = '';
                                  if($week == 'Sun' || $week == 'Sat'){
                                    $bColor = 'background-color:orange';
                                    $defaultValue = "0";
                                  }else{
                                    $bColor = '';
                                  }

                                  if(empty($timesheetData['standardHours'])){
                                    $timesheetData['standardHours'] = $defaultValue;
                                  }

                                  if(empty($timesheetData['extraHours'])){
                                    $timesheetData['extraHours'] = $defaultValue;
                                  }

                                  $totalStandardHours += $timesheetData['standardHours'];
                                  $totalOtHours += $timesheetData['extraHours'];
                                 /*  $th1.="<th scope='col' id='dateheader'>".$nextDate."</th>";
    $td1.="<td>".$timesheetData['standardHours']."</td>";
    $td2.="<td>".$timesheetData['extraHours']."</td>";*/
   
     ?>
                                        <input type="hidden" name="timesheetId[]" value="<?php echo $timesheetData['timesheetId'];?>">
                                         <input value="<?php echo $nextDate; ?>" type="hidden" name="workDate[]">
                                        <div class="row" >
                                       <?php $th1.= ' <th scope="col" id="dateheader" style="'.$bColor.'"><div class="form-group">
                                               '.$nextDate ."<br>$week".'
                                        </div></th>'; 
                                        $td1.= '<td> <div class="form-group">
                                                <input class="form-control shours thours" value= "'.$timesheetData['standardHours'].'" placeholder="Standard Hours" name="standardHours[]" type="text" maxlength="2" onkeypress="return isNumberKey(event)" required>
                                        </div></td>';
                                        $td2.= '<td><div class="form-group">
                                                <input class="form-control othours thours" value="'.$timesheetData['extraHours'].'" placeholder="OT" name="extraHours[]" type="text" maxlength="2" onkeypress="return isNumberKey(event)">
                                        </div></td>' ; ?>
                                    </div>
                                        <?php } 
                                        $totalHours = $totalStandardHours+$totalOtHours;
                                        $th1.= ' <th scope="col" id="dateheader" border="'.$bColor.'"><div class="form-group">
                                               Total
                                        </div></th>'; 
                                         $td1.= '<td> <div class="form-group">
                                                <input class="form-control shoursTotal" value="'.$totalStandardHours.'" readonly="readonly">
                                        </div></td>';
                                         $td2.= '<td><div class="form-group">
                                                <input class="form-control otTotalhours" value="'.$totalOtHours.'" placeholder="OT">
                                        </div></td>' ;
                                        $th1.="</tr>";
$td1.="</tr>";
$td2.="</tr>";

$td3.= '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>Total Hours</td><td class="totalhours">'.$totalHours.'</td></t6>';

echo $result="<table id='syncresults' border='1' class='table-responsive'>".$th1.$td1.$td2.$td3."</table>";
$attachmentData = $this->mainModel->getWeekAttachment($_GET['date'],$loginUniqueId);
 ?>  
  <br> 
  <input type="hidden" name="attachment1Old" value="<?php echo $attachmentData[0]['attachment1']; ?>">
  <input type="hidden" name="attachment2Old" value="<?php echo $attachmentData[0]['attachment2']; ?>">
<div class="row">
  <div class="col-md-3 col-sm-12">
    <input type="hidden" name="weekDate" value="<?php echo $_GET['date']; ?>">
    <label>Attachment 1</label>
 <input type="file" name="attachment1" class="form-control">
</div>
<div class="col-md-3 col-sm-12">
  <label>Attachment 2</label>
  <input type="file" name="attachment2" class="form-control">
  </div>
  
                                 
                                       <div class="col-md-4 col-sm-12"><br>
                                         <button type="submit" class="btn btn-primary pull-right" style="margin-left:40px;">Submit</button>
                    <a href="<?php echo base_url('viewTimesheet'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </div>
                                   </form> 
                                   </table>    
                               <?php } ?>
</div>
</div>
</div>

<script type="text/javascript">
$(document).on("change", ".shours", function() {
    var sum = 0;
    $(".shours").each(function(){
        sum += +$(this).val();
    });
    $(".shoursTotal").val(sum);
});

$(document).on("change", ".othours", function() {
    var sum = 0;
    $(".othours").each(function(){
        sum += +$(this).val();
    });
    $(".otTotalhours").val(sum);
});

$(document).on("change", ".thours", function() {
    var sum = 0;
    $(".thours").each(function(){
        sum += +$(this).val();
    });
    $(".totalhours").html(sum);
});


$(".adddTimesheet").submit(function(){
    var shoursTotal = $.trim($(".shoursTotal").val());
    var msg = '';
    if(shoursTotal<40){
      var msg = "Hours is lessthan 40. is it ok?";
    }
    if(shoursTotal>40){
      var msg = "Hours is greaterthan 40. is it ok?";
    }
    var c = confirm(msg);
    return c; 
});
</script>