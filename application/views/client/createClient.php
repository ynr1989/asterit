<div class="page-head-line">Create Client</div>

            <div id="page-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        
                        <!-- <h1 class="page-subhead-line">
                        <strong><?php if($this->session->flashdata('message')!=''):
                            echo $this->session->flashdata('message')."<hr>";
                            endif; ?></strong></h1> -->

                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-9 col-sm-6">
               <div class="panel">
                        
                      
                <form method="post" action="#" class="createClient" enctype="multipart/form-data">
                  
                    <div class="row">
                         <!-- <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="text" required name="businessId" placeholder="BusinessId" class="form-control" readonly="readonly"
                           value="<?php echo $this->mainModel->generateBusinessID(); ?>">
                        </div>
                      </div>  -->
                      
                      </div>                      
                    

                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Client Firm <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="clientFirm" placeholder="Client Firm" maxlength="25" class="form-control clientFirm">
                        </div>
                      </div> 
                      <div class="col-md-6">
                        <label class="bmd-label-floating">Contact Name <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="contactName" placeholder="Contact Name" maxlength="25" class="form-control contactName">
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Email Address <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <label class="bmd-label-floating">Mobile Number <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control mobile" >
                        </div>
                      </div>                      
                    </div>                    
                      <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Gender <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Other">Other</option>
                          </select>
                        </div>
                      </div>  
                       <div class="col-md-6">
                        <label class="bmd-label-floating">Address <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" name="address" placeholder="Address" class="form-control address">
                        </div>
                      </div>                    
                    </div>
                      <!-- <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Passport Number </label>
                        <div class="form-group">
                          <input type="text" name="passportNumber" maxlength="12" placeholder="Passport Number" class="form-control passportNumber">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="bmd-label-floating">PAN Number </label>
                        <div class="form-group">
                          <input type="text" name="panNumber" maxlength="12" placeholder="PAN Number" class="form-control panNumber">
                        </div>
                      </div>                        
                    </div>    -->                   
                     
                       <div class="row">
                         <div class="col-md-6">
                          <label class="bmd-label-floating">Description </label>
                        <div class="form-group">
                          <textarea name="description" id="description" class="form-control description" placeholder="Description..."></textarea>
                        </div>
                      </div>   

                         <div class="col-md-6">
                          <label class="bmd-label-floating">Status </label>
                        <div class="form-group">
                           <select name="vendorStatus" required class="form-control vendorStatus">
                             <!--  <option value="">Select Status</option> -->
                              <option value="0">In Active</option>
                              <option value="1">Active</option>
                          </select>
                        </div>
                      </div>   
                                         
                    </div>
                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url('clientList'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
    
      
    