            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <?php //$this->load->view('./templates/personalLinks',$data); ?>
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                 <?php $fTypes = $this->mainModel->getTypes('FAMILY_TYPE'); ?>
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                     
                <form method="post" action="<?php echo base_url('visaDetailsSubmit'); ?>"  enctype="multipart/form-data">
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">

                      <div class="add-visa-data">
                               <i class="fa fa-plus add-visa-content"></i> Add Visa
                                </div><br>                    
                                <div class="row">
                                <div class="visa-list">                                    
                                </div>  
                                  
                                <?php if( count($visaDetails) > 0 ) {
                                  foreach($visaDetails AS $row){ ?>          
                                      
                                      <input type="hidden" name="visaUniqueId[]" value="<?php echo $row['visaUniqueId'];?>">
                                     <input type="hidden" name="attachment11[]" value="<?php echo $row['attachment1'];?>">
                                      <input type="hidden" name="attachment22[]" value="<?php echo $row['attachment2'];?>">
                                      <input type="hidden" name="attachment33[]" value="<?php echo $row['attachment3'];?>">
                                      <div class="row">
                                       <div class="form-group col-md-2 col-sm-3 col-xs-8">

                                      <label>Select Family</label>
                                      <select class="form-control" name="vemployeeFamilyId[]" id="vemployeeFamilyId-0" required>
                                          <option value="">Select Type</option>
                                          <option <?php if($row['employeeFamilyId'] == '0'): echo 'selected'; endif; ?> value="0">Self</option>
                                          <?php foreach($familyDetails as $fType) {?>
                                            <option <?php if($fType['employeeFamilyId'] == $row['employeeFamilyId']): echo 'selected'; endif; ?> value="<?php echo $fType['employeeFamilyId'];?>"><?php echo $fType['firstName']." ".$fType['lastName'];?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Visa Type</label>
                                                <input class="form-control visaType" id="visaType-0" placeholder="Visa Type" type="text" name="visaType[]" value="<?php echo $row['visaType']; ?>">
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Visa Number</label>
                                                <input class="form-control visaNumber" id="visaNumber-0" placeholder="Visa Number" name="visaNumber[]" type="text" value="<?php echo $row['visaNumber']; ?>">
                                        </div>

                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Issue Date</label>
                                                <input class="form-control vissueDate dateField" id="vissueDate-0" placeholder="Issue Date" type="text" name="vissueDate[]" value="<?php echo $row['issueDate']; ?>" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Place Of Issue</label>
                                                <input class="form-control vplaceOfIssue" id="vplaceOfIssue-0" placeholder="Place Of Issue No" name="vplaceOfIssue[]" type="text" value="<?php echo $row['placeOfIssue']; ?>">
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Expire Date</label>
                                                <input class="form-control vexpireDate dateField" id="vexpireDate-0" placeholder="Expire Date" type="text" name="vexpireDate[]"  value="<?php echo $row['expireDate']; ?>">
                                        </div>
                                         <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Country</label>
                                                <input class="form-control vcountry" placeholder="Country" id="vcountry-0" type="text" name="vcountry[]" value="<?php echo $row['country']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment1</label>
                                                <input class="form-control attachment1" placeholder="" id="attachment1-0" type="file" name="attachment1[]" value="<?php echo $row['']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment2</label>
                                                <input class="form-control attachment2" placeholder="" id="attachment2-0" type="file" name="attachment2[]" value="<?php echo $row['']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment3</label>
                                                <input class="form-control attachment3" placeholder="" id="attachment3-0" type="file" name="attachment3[]" value="<?php echo $row['']; ?>">
                                        </div>

                                        <a onclick="return confirm('Are you sure to delete?')" href="<?php echo base_url('deleteUserVisa/?visaUniqueId='.$row['visaUniqueId'] );?>" >&nbsp;X</a>
                                      </div>
                                        <?php } }else{ ?>  
                                            <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                 <label>Select Family</label>
                                      <select class="form-control" name="vemployeeFamilyId[]" id="vemployeeFamilyId-0" required>
                                          <option value="">Select Type</option>
                                          <option value="0">Self</option>
                                          <?php foreach($familyDetails as $fType) {?>
                                            <option value="<?php echo $fType['employeeFamilyId'];?>"><?php echo $fType['firstName']." ".$fType['firstName'];?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Visa Type</label>
                                                <input class="form-control visaType" id="visaType-0" placeholder="Visa Type" type="text" name="visaType[]" >
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Visa Number</label>
                                                <input class="form-control visaNumber" id="visaNumber-0" placeholder="Visa Number" name="visaNumber[]" type="text" >
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Issue Date</label>
                                                <input class="form-control vissueDate dateField" id="vissueDate-0" placeholder="Issue Date" type="text" name="vissueDate[]" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Place Of Issue</label>
                                                <input class="form-control vplaceOfIssue" id="vplaceOfIssue-0" placeholder="Place Of Issue No" name="vplaceOfIssue[]" type="text">
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Expire Date</label>
                                                <input class="form-control vexpireDate dateField" id="vexpireDate-0" placeholder="Expire Date" type="text" name="vexpireDate[]" >
                                        </div>
                                         <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Country</label>
                                                <input class="form-control vcountry" placeholder="Country" id="vcountry-0" type="text" name="vcountry[]">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment1</label>
                                                <input class="form-control attachment1" placeholder="" id="attachment1-0" type="file" name="attachment1[]">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment2</label>
                                                <input class="form-control attachment2" placeholder="" id="attachment2-0" type="file" name="attachment2[]">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment3</label>
                                                <input class="form-control attachment3" placeholder="" id="attachment3-0" type="file" name="attachment3[]">
                                        </div>
                                        <?php } ?>                                    
                                   
                            </div>

                  <a href="<?php echo base_url('visaDetails'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-profile">
                <!-- <div class="card-avatar">
                  <a href="#pablo">
                    <?php if(empty($userInfo[0]['profilePic'])){
                      $profilePic = base_url()."assets/"."noimage.png";
                    }else{
                      $profilePic = base_url()."assets/profilePics/".$userInfo[0]['profilePic'];
                    } ?>
                    <img class="img" src="<?php echo $profilePic; ?>" style="width: 100%;"/>
                  </a>
                </div> -->
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
     
    
      <script>
       /*$(".add-kids-data").hide();
       $(".add-spouse-data").hide();
      $(".maritalStatus").on('change', function() {
            console.log("changed")
            let maritalStatus = $(this).val();
            if(maritalStatus == 'Yes'){
              $(".add-kids-data").show();
                $(".add-spouse-data").show();
            }else{
              $(".add-kids-data").hide();
                $(".add-spouse-data").hide();
            }
            
      });*/
      </script>