<!-- 
<div class="page-head-line">Education Details</div> -->
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                       <?php //$this->load->view('./templates/personalLinks',$data); ?>

                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                            <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                <form method="post" action="<?php echo base_url('educationDetailsSubmit'); ?>"  enctype="multipart/form-data">
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">

                      <div  class="col-md-12 col-12">
                      <div class="row">
                                <div class="add-education-data" style="margin-left:18px;">
                                    <i class="fa fa-plus add-education-content"></i>
                                </div>
                                <div class="education-list11">
                                    <div class="education-data11 row">
                                        
                                    <?php $k=0; if( count($educationDetails) > 0 ) { $k++;
                                    foreach($educationDetails AS $row){ ?>
                                     <input type="hidden" name="educationId[]" value="<?php echo $row['educationId'];?>">
                                 <input type="hidden" name="attachment11[]" value="<?php echo $row['attachment1'];?>">
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Qualification Title</label>
                                                <input class="form-control qualificationTitle" placeholder="Qualification Title" id="qualificationTitle-0" type="text" name="qualificationTitle[]" value="<?php echo $row['qualificationTitle']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Specialization</label>
                                                <input class="form-control specialization" placeholder="Specialization" id="specialization-0" type="text" name="specialization[]" value="<?php echo $row['specialization']; ?>">
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Institution</label>
                                                <input class="form-control institution" placeholder="Institution" id="institution-0" name="institution[]" type="text" value="<?php echo $row['institution']; ?>">
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Start year</label>
                                                <input class="form-control dateField startYear" id="startYear-<?php echo $row['educationId']; ?>0" type="text" name="startYear[]" placeholder="Start year" value="<?php echo $row['startYear']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>End year</label>
                                                <input class="form-control dateField endYear" placeholder="End year" id="endYear-<?php echo $row['educationId']; ?>0" type="text" name="endYear[]" value="<?php echo $row['endYear']; ?>" >
                                        </div>                                       
                                         <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachments</label>
                                                <input class="form-control educationDocuments" id="educationDocuments-0" type="file" name="educationDocuments[]" value="<?php echo $row['firstName']; ?>">
                                        </div>
                                        <a onclick="return confirm('Are you sure to delete?')" href="<?php echo base_url('deleteEducation/?educationId='.$row['educationId'] );?>" >&nbsp;X</a> 
                                        <?php } } ?>                                       
                                    </div>
                                </div>


                                <div class="education-list">
                                    <div class="education-data row">
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Qualification Title</label>
                                                <input class="form-control qualificationTitle" placeholder="Qualification Title" id="qualificationTitle-0" type="text" name="qualificationTitle[]"> 
                                        </div>  
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Specialization</label>
                                                <input class="form-control specialization" placeholder="Specialization" id="specialization-0" type="text" name="specialization[]">
                                        </div>                                       
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Institution</label>
                                                <input class="form-control institution" placeholder="Institution" id="institution-0" name="institution[]" type="text">
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Start year</label>
                                                <input class="form-control dateField startYear" id="startYear-0" type="text" name="startYear[]" placeholder="Start year">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>End year</label>
                                                <input class="form-control dateField endYear" placeholder="End year" id="endYear-0" type="text" name="endYear[]" >
                                        </div>                                       
                                         <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachments</label>
                                                <input class="form-control attachment1" id="attachment1-0" type="file" name="attachment1[]">
                                        </div>
                                        
                                    </div>
                                </div>


                            </div>
                     </div>
                    



                     <div class="col-md-12 col-12">
                     
                      <!-- <div class="add-kids-data">
                                Add Kids    <i class="fa fa-plus add-kids-content"></i>
                                </div><br>  -->                   
                                <div class="row">
                                <div class="kids-list">                                    
                                </div>  

                                <div class="kids1-data row">   
                                <?php if( count($kidsetails) > 0 ) {
                                  foreach($kidsetails AS $row){ ?>          
                                   <input type="hidden" name="childrenId[]" value="<?php echo $row['childrenId'];?>">                           
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Children First Name</label>
                                                <input class="form-control childrenFirstName" id="childrenFirstName-0" type="text" name="childrenFirstName[]" value="<?php echo $row['firstName']; ?>">
                                        </div>                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>LastName</label>
                                                <input class="form-control childrenlastName" id="childrenlastName-0" name="childrenlastName[]" type="text" value="<?php echo $row['lastName']; ?>">
                                        </div>
                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>DOB</label>
                                                <input class="form-control childrenDob dateField" id="childrenDob-0" type="text" name="childrenDob[]"  value="<?php echo $row['dob']; ?>">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Gender</label> 
                                                 <select name="childrenGender[]" class="form-control childrenGender" id="childrenGender-0" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php if($row['gender'] == 'Male'): echo 'selected'; endif; ?>>Male</option>
                              <option value="Female" <?php if($row['gender'] == 'Female'): echo 'selected'; endif; ?>>Female</option>
                              <option value="Other" <?php if($row['gender'] == 'Other'): echo 'selected'; endif; ?>>Other</option>
                          </select>
                                        </div> 

                                        <a onclick="return confirm('Are you sure to delete?')" href="<?php echo base_url('deleteChildren/?childrenId='.$row['childrenId'] );?>" >&nbsp;X</a>

                                        <?php } } ?>                                      
                                    </div>

                            </div>
                     </div>

                
                  <a href="<?php echo base_url('educationDetails'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>
     
    
      <script>
      /* $(".add-kids-data").hide();
       $(".add-spouse-data").hide();
      $(".maritalStatus").on('change', function() {
            console.log("changed")
            let maritalStatus = $(this).val();
            if(maritalStatus == 'Yes'){
              $(".add-kids-data").show();
                $(".add-spouse-data").show();
            }else{
              $(".add-kids-data").hide();
                $(".add-spouse-data").hide();
            }
            
      });*/
      </script>