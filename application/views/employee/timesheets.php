<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  </div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
                     <?php if($userTypeCode ==5 OR $userTypeCode == 2 OR $userTypeCode == 3){ ?>                  
            <form>

            <div class="row">
                  
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="year" id="year" title="Category">
                       <!--  <option value="">Select Year</option> -->
                        <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                  </select>
                </div>
              </div>
             
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="month" id="month" title="Category">
                        <option value="">Select Month</option>
                      <?php  
                     $monthArray = array(
                    "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                    "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                );
                      foreach ($monthArray as $monthNumber=>$month) { ?>
                       <option value="<?php echo $monthNumber; ?>" <?php if($monthNumber == $drdata['id']): echo "selected"; endif; ?>><?php echo $month; ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div>
              <?php
              $usersInfo = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
              ?>
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="userUniqueId" id="userUniqueId" title="Category">
                        <option value="">Select Employee</option>
                      <?php  
                      foreach ($usersInfo as $usersInfos) { ?>
                       <option value="<?php echo $usersInfos['userUniqueId']; ?>" <?php if(isset($_GET) && $_GET['userUniqueId'] == $usersInfos['userUniqueId']): echo "selected"; endif; ?>><?php echo $usersInfos['firstName']." ".$usersInfos['lastName']; ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div>


              <div class="col-md-2">
                <div class="form-group">
                   <button type="submit" class="btn btn-primary ">Submit</button>
				   <a href="<?php echo base_url('timesheets'); ?>" class="btn btn-primary pull-right" role="button">Reset</a>
                </div>
              </div>

            </div>
           
          </form>
            <br><br>
          <?php } ?>

                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <!-- <table class="table table-striped table-bordered table-hover" id="userPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Employee ID</th>
                        <th>First Name</th>
			                  <th>Last Name</th>	                        
                        <th>Standard Hours</th>
                        <th>Extra Hours</th>
                        <th>Total Hours</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($employeeData as $data):
                          if($_GET['year']==""){
                            $year = date("Y");
                          }else{
                             $year = $_GET['year'];
                          }

                          if($_GET['month']!=""){
                            $month = $_GET['month'];
                          }else{
                             $month = '';
                          }

                          $standardHours = $this->mainModel->getTotalHours($data['userUniqueId'],$year,$month);
                          $extraHours = $this->mainModel->getTotalExtraHours($data['userUniqueId'],$year,$month);
                           ?>
                        <tr data-child-value="<?php echo $data['startDate']; ?> # <?php echo $data['endDate']; ?> # <?php echo $data['gender']; ?> # <?php echo $data['dob']; ?> # <?php echo $data['address']; ?>">
                           <td class="details-control"><?php //echo $i; ?></td>
                          <td><?php echo $data['employeeId']; ?></td>
                          <td><?php echo $data['firstName']; ?></td>
                          <td><?php echo $data['lastName']; ?></td>
                          <td><?php echo $standardHours; ?></td>
                          <td><?php echo $extraHours; ?></td>
                          <td><?php echo $standardHours+$extraHours; ?></td>
                         </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table> -->


                     <table class="table table-striped table-bordered table-hover" id="example">
                                  <thead>
<tr><th>Employee ID</th>
                        <th>Name</th><th>Start Date</th><th>End Date</th><th>ST</th><th>OT</th><th>Attachments</th><th>Status</th>
  <?php if($userTypeCode == 4){ ?>
<th>Actions</th>
<?php } ?>
</tr>
</thead>
<tbody>
<?php


$date = date("Y-m-");
//$str = '2020-08-';
$str = $date;

if($_GET['userUniqueId']){
  $loginUniqueId = $_GET['userUniqueId'];


//$clientInfo = $this->mainModel->getProjectClientInfo($loginUniqueId);
  //echo "<strong>Project Start Date: </strong>".$clientInfo[0]['startDate']." &nbsp;&nbsp; "."<strong>End Date : </strong>".$clientInfo[0]['startDate']."</br></br>";
  for($i2=1; $i2<31; $i2++)
  {
 

    // echo '<br>',
      $ddd = $str.$i2;
    // echo '',
      $date = date('Y M D d', $time = strtotime($ddd) );

    //if( strpos($date, 'Sat') || strpos($date, 'Sun') )
    if(strpos($date, 'Sun') )
    {
      $date1 = date('Y M D d', $time = strtotime($ddd) );
      $sdate = date('Y-m-d', $time = strtotime($ddd) ); ?>

<?php
$userInformation = $this->mainModel->getUserInfo($loginUniqueId);
 
$timeSheetData = $this->mainModel->getTimeSheetsDataByWeek($loginUniqueId,$sdate); 
 
$endDate = date('Y-m-d', $time=strtotime($ddd. ' + 6 days'));

$attachmentData = $this->mainModel->getWeekAttachment(date('Y-m-d', $time=strtotime($ddd)),$loginUniqueId);
     ?>
   <tr>
<td><?php echo $userInformation[0]['employeeId']; ?></td>
    <td><?php echo $userInformation[0]['firstName']." ".$userInformation[0]['lasrName']; ?></td>
    <td><?php echo date('Y-m-d', $time=strtotime($ddd)); ?></td>
    <td><?php echo  $endDate; ?></td>
    <td><?php echo $timeSheetData['totalHours']; ?></td>
  <td><?php echo $timeSheetData['totalExtraHours']; ?></td>
  
<td><?php
  if($attachmentData[0]['attachment1'] != '' ):
  $document = base_url()."assets/timesheetAttachments/".$attachmentData[0]['attachment1']; ?>
  <a href="#"><span class="getDocumentInPopup" id="<?php echo $document; ?>"><i class="fa fa-eye" aria-hidden="true"></i></span></a>

  <?php endif;
if($attachmentData[0]['attachment2']!=""):
  $document1 = base_url()."assets/timesheetAttachments/".$attachmentData[0]['attachment2']; ?>
  <a href="#"><span class="getDocumentInPopup" id="<?php echo $document1; ?>"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
<?php endif; ?>
</td>
<td><?php if(($userTypeCode == 2 || $userTypeCode == 3 || $userTypeCode == 5) && $attachmentData[0]['timeSheetAutoId']!=""){ 
   if($attachmentData[0]['weekStatus'] == 0){ ?>
                   <a href="<?php echo base_url('approveComment'); ?>?timeSheetAutoId=<?php echo $attachmentData[0]['timeSheetAutoId']; ?>" style="text-decoration: none;">Approve</a>
                <?php }else{ ?>
                        <a href="<?php echo base_url('RejectComment'); ?>?timeSheetAutoId=<?php echo $attachmentData[0]['timeSheetAutoId']; ?>" style="text-decoration: none;">Cancel</a>
                <?php } 
  }else{
    if($attachmentData[0]['timeSheetAutoId']!=""){
       if($attachmentData[0]['weekStatus'] == 0):echo "Pending"; else: echo "Approved"; endif;
    }
   
  }
  
   ?></td>
<?php if($userTypeCode == 4){ ?>
  <td>
    <?php  if($attachmentData[0]['weekStatus'] == 0): ?>
<a href="<?php echo base_url('editTimesheet'); ?>?date=<?php echo date('Y-m-d', $time=strtotime($ddd)); ?>"><i class="fa fa-edit"></i></a>
<?php else: ?>
<a href="<?php echo base_url('checkTimesheet'); ?>?date=<?php echo date('Y-m-d', $time=strtotime($ddd)); ?>"><i class="fa fa-eye"></i></a>
  <?php endif; echo ' </td>'; } ?>
 
</tr>
     


   <?php } 
  }
}
  ?>
  </tbody>
</table>

  <?php if($_GET['date']) {
  $myProjects = $this->mainModel->getMyProjects($loginUniqueId);
  $projectDetails = $this->mainModel->getTimeSheetDataByDate($loginUniqueId,$_GET['date']);
   ?>
           

                               <?php } ?>

                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>