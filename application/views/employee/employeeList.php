<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                        <!-- <h1 class="page-head-line">Employees List</h1> -->
                      <?php
                     if($this->uri->segment('1')!='employeeList'):
                     $this->load->view('./templates/tabLinks',$data);
                     endif; ?>

                      <a href="<?php echo base_url($createurl); ?>" class="btn btn-2 tabButtons addButton"><i class="fa fa-plus fa-4x"></i></a>
                    </div></div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
                     <?php if($user_type ==1 OR $user_type == 2 OR $user_type == 3 OR ($user_type == 4 && $team_lead_status ==1)){ ?>                  
            <form>

            <div class="row">
                  
              <?php if($user_type != 4): ?>             
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control selectpicker" name="loc" id="search_types_option" title="Category">
                        <option value="">Select Location</option>
                      <?php $row = $this->admModel->getUniqueLocations(); 
                      $get_location = $this->input->get('loc');
                      $get_location = str_replace("%2F", "/",$get_location);
                      foreach($row as $drdata){
						if($drdata['location']!=""):
						  ?>
                       <option value="<?php echo $drdata['location']; ?>" <?php if($drdata['location'] == $get_location): echo "selected"; endif; ?>><?php echo $drdata['location']; ?></option>
                       <?php endif; } ?>
                  </select>
                </div>
              </div>
              <?php endif; ?>

              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control selectpicker" name="user" id="search_types_option" title="Category">
                        <option value="">Select User</option>
                      <?php  
                      if($team_lead_status == 1){
                        $row = $this->admModel->getMarketingUsersByLead($uid); 
                      }else{
                        $row = $this->admModel->getAllUsers(); 
                      }
                      $get_user_id = $this->input->get('user');
                      foreach($row as $drdata){ ?>
                       <option value="<?php echo $drdata['id']; ?>" <?php if($get_user_id == $drdata['id']): echo "selected"; endif; ?>><?php echo $drdata['first_name']." ".$drdata['last_name']; ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                   <input class="form-control" name="FromDate" placeholder="From Date" readonly="readonly" type="text" id="fdate" autocomplete="off" value="<?php if($this->input->get('FromDate')!=''): echo $this->input->get('FromDate'); endif; ?>">
                </div>
              </div>
              <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                   <input class="form-control" name="toDate" placeholder="To Date" readonly="readonly" type="text" id="tdate" autocomplete="off">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                   <button type="submit" class="btn btn-primary ">Submit</button>
				   <a href="<?php echo base_url('leaves-list'); ?>" class="btn btn-info pull-right" role="button">Reset</a>
                </div>
              </div>

            </div>
           
          </form>
            <br><br>
          <?php } ?>
 
                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="userPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Employee ID</th>
                        <th>First Name</th>
			                  <th>Last Name</th>	                        
                        <th>Email</th>
                        <th>Mobile</th>		
                        <th>Documents</th>                  
                        <th>Actions</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($employeeData as $data): ?>
                        <tr data-child-value="<?php echo $data['startDate']; ?> # <?php echo $data['endDate']; ?> # <?php echo $data['gender']; ?> # <?php echo $data['dob']; ?> # <?php echo $data['address']; ?>">
                           <td class="details-control"><?php //echo $i; ?></td>
                          <td><?php echo $data['employeeId']; ?></td>
                          <td><?php echo $data['firstName']; ?></td>
                          <td><?php echo $data['lastName']; ?></td>
                          <td><?php echo $data['email']; ?></td>
                          <td><?php echo $data['mobile']; ?></td>   
                          <td><!-- <input type="button" name="view" value="View Documents" id="<?php echo $data["userUniqueId"]; ?>" class="btn btn-info btn-xs userDocumentsData" /> -->
                            <?php $docInfo = $this->mainModel->getUserDocuments($data['userUniqueId']);
                            foreach($docInfo as $docInfos){  
                            $document = base_url()."assets/documents/".$docInfos["attachment"]; ?>
                            <a href="#" title="<?php echo $docInfos["documentType"]; ?>"><span class="getDocumentInPopup" id="<?php echo $document; ?>"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                          <?php } ?>
                          </td>
                           <td>
                             <?php if($data['userTypeCode'] == 4): ?>
                               <a title="View More" style="font-weight: 900;font-size: 23px;margin-right: 7px;" href="<?php echo base_url("personalDetails"); ?>?userUniqueId=<?php echo $data['userUniqueId']; ?>" style="text-decoration: none;"><i class="fas fa-angle-double-right"></i></a>
                            <?php endif; ?>

                            <label class="switch" data-on="On" data-off="Off">
                              <input type="checkbox" name="userStatus" data-userUniqueId="<?php echo $data['userUniqueId']; ?>" class="userStatusChange" <?php if($data['userStatus'] == 1){ echo 'checked'; } ?>>
                              <span class="slider round"></span>
                            </label>&nbsp;&nbsp;
                           
                          <a title="Edit User" href="<?php echo base_url($editUrl); ?>?userUniqueId=<?php echo $data['userUniqueId']; ?>&token=<?php echo $data['token']; ?>" style="text-decoration: none;"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;

                        <a  onclick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url($deleteUrl); ?>?userUniqueId=<?php echo $data['userUniqueId']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td> </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>