<!-- 
<div class="page-head-line">Education Details</div> -->
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                       <?php //$this->load->view('./templates/personalLinks',$data); ?>

                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                <form method="post" action="<?php echo base_url('employmentDetailsSubmit'); ?>"  enctype="multipart/form-data">
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">

                     <div class="col-md-12 col-12">             
                                <div class="row">
                                <div class="kids-list">                                    
                                </div>  

                                <div class="kids1-data row">   
                                <?php if(count($employmentDetails) > 0 ) {
                                  foreach($employmentDetails AS $row){  ?>          
                                    <input type="hidden" name="attachment11[]" value="<?php echo $row['attachment1'];?>">
                                      <input type="hidden" name="attachment22[]" value="<?php echo $row['attachment2'];?>">
                                      <input type="hidden" name="attachment33[]" value="<?php echo $row['attachment3'];?>">
                                   <input type="hidden" name="employmentId[]" value="<?php echo $row['employmentId'];?>">                           
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Company Name</label>
                                                <input class="form-control companyName" placeholder="Company Name" id="companyName-0" type="text" value="<?php echo $row['companyName']; ?>" name="companyName[]"> 
                                              

                                        </div> 
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Role</label>
                                                <input class="form-control" value="<?php echo $row['role']; ?>" placeholder="Role" id="role-0" type="text" name="role[]" >
                                        </div>

                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Technology/Tools</label>
                                                <input class="form-control" value="<?php echo $row['technology']; ?>" placeholder="Technology" id="technology-0" type="text" name="technology[]" >
                                        </div>
                                       
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>From Date</label>
                                                <input class="form-control fromDate dateField"  value="<?php echo $row['fromDate']; ?>" placeholder="From Date" id="fromDate-<?php echo $row['employmentId'];?>0" type="text" name="fromDate[]" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>To Date</label>
                                                <input class="form-control toDate dateField" value="<?php echo $row['toDate']; ?>" placeholder="To Date" id="toDate-<?php echo $row['employmentId'];?>0" type="text" name="toDate[]" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Address</label>
                                                <input class="form-control address" value="<?php echo $row['address']; ?>" placeholder="Address" id="address-0" name="address[]" type="text">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Attachment1</label>
                                                <input class="form-control attachment1"  placeholder="" id="attachment1-0" type="file" name="attachment1[]">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Attachment2</label>
                                                <input class="form-control attachment2" value="<?php echo $row['attachment2']; ?>" placeholder="" id="attachment2-0" type="file" name="attachment2[]">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Attachment3</label>
                                                <input class="form-control attachment3" value="<?php echo $row['attachment3']; ?>" placeholder="" id="attachment3-0" type="file" name="attachment3[]">
                                        </div>

                                        <a onclick="return confirm('Are you sure to delete?')" href="<?php echo base_url('deleteEmployment/?employmentId='.$row['employmentId'] );?>" >&nbsp;X</a>

                                        <?php } } ?>                                      
                                    </div>

                            </div>
                     </div>

                      <div  class="col-md-12 col-12">
                      <div class="row">
                                <div class="add-employment-data" style="margin-left:18px;">
                                    <i class="fa fa-plus add-employment-content"></i>
                                </div>
                                <div class="employment-list">
                                    <div class="employment-data row">
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Company Name</label>
                                                <input class="form-control companyName" placeholder="Company Name" id="companyName-0" type="text" name="companyName[]"> 
                                              

                                        </div>                                        
                                       <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Role</label>
                                                <input class="form-control" placeholder="Role" id="role-0" type="text" name="role[]" >
                                        </div>

                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>From Date</label>
                                                <input class="form-control fromDate dateField" placeholder="From Date" id="fromDate-0" type="text" name="fromDate[]" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>To Date</label>
                                                <input class="form-control toDate dateField" placeholder="To Date" id="toDate-0" type="text" name="toDate[]" >
                                        </div>

                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Technology/Tools</label>
                                                <input class="form-control" placeholder="Technology" id="technology-0" type="text" name="technology[]" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Address</label>
                                                <input class="form-control address" placeholder="Address" id="address-0" name="address[]" type="text">
                                        </div>
                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Attachment1</label>
                                                <input class="form-control attachment1"  placeholder="" id="attachment1-0" type="file" name="attachment1[]">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Attachment2</label>
                                                <input class="form-control attachment2" value="<?php echo $row['attachment2']; ?>" placeholder="" id="attachment2-0" type="file" name="attachment2[]">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Attachment3</label>
                                                <input class="form-control attachment3" value="<?php echo $row['attachment3']; ?>" placeholder="" id="attachment3-0" type="file" name="attachment3[]">
                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                     </div>

                
                  <a href="<?php echo base_url('employmentDetails'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>
     
    
      <script>
      /* $(".add-kids-data").hide();
       $(".add-spouse-data").hide();
      $(".maritalStatus").on('change', function() {
            console.log("changed")
            let maritalStatus = $(this).val();
            if(maritalStatus == 'Yes'){
              $(".add-kids-data").show();
                $(".add-spouse-data").show();
            }else{
              $(".add-kids-data").hide();
                $(".add-spouse-data").hide();
            }
            
      });*/
      </script>