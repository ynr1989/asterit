<!-- <div class="page-head-line">Employee List</div> -->
            <div id="page-inner">
                <div class="row">
                  </div>
                <!-- /. ROW  -->
              
            <div class="row">
                <div class="col-md-12 pt-3">
                    
                     <?php if($userTypeCode ==5 OR $userTypeCode == 2 OR $userTypeCode == 3){ ?>                  
            <form>

            <div class="row">
                  
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="year" id="year" title="Category">
                       <!--  <option value="">Select Year</option> -->
                        <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                  </select>
                </div>
              </div>
             
              <div class="col-md-3">
                <div class="form-group">
                  <select class="form-control" name="month" id="month" title="Category">
                        <option value="">Select Month</option>
                      <?php  
                     $monthArray = array(
                    "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                    "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                );
                      foreach ($monthArray as $monthNumber=>$month) { ?>
                       <option value="<?php echo $monthNumber; ?>" <?php if($monthNumber == $drdata['id']): echo "selected"; endif; ?>><?php echo $month; ?></option>
                       <?php } ?>
                  </select>
                </div>
              </div>


              <div class="col-md-2">
                <div class="form-group">
                   <button type="submit" class="btn btn-primary ">Submit</button>
				   <a href="<?php echo base_url('timesheets'); ?>" class="btn btn-primary pull-right" role="button">Reset</a>
                </div>
              </div>

            </div>
           
          </form>
            <br><br>
          <?php } ?>

                    <div class="panel">
                    	<?php if($this->session->flashdata('message')!=''): ?>
                    	<div class="success_message alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                    <?php endif; ?>
                       
                        <div class="">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="userPrintRowExpand" >
                      <thead class=" text-primary">
                        <th>#</th>
                        <th>Employee ID</th>
                        <th>First Name</th>
			                  <th>Last Name</th>	                        
                        <th>Standard Hours</th>
                        <th>Extra Hours</th>
                        <th>Total Hours</th>
                      </thead>
                      <tbody>
                          <?php $i=1; foreach($employeeData as $data):
                          if($_GET['year']==""){
                            $year = date("Y");
                          }else{
                             $year = $_GET['year'];
                          }

                          if($_GET['month']!=""){
                            $month = $_GET['month'];
                          }else{
                             $month = '';
                          }

                          $standardHours = $this->mainModel->getTotalHours($data['userUniqueId'],$year,$month);
                          $extraHours = $this->mainModel->getTotalExtraHours($data['userUniqueId'],$year,$month);
                           ?>
                        <tr data-child-value="<?php echo $data['startDate']; ?> # <?php echo $data['endDate']; ?> # <?php echo $data['gender']; ?> # <?php echo $data['dob']; ?> # <?php echo $data['address']; ?>">
                           <td class="details-control"><?php //echo $i; ?></td>
                          <td><?php echo $data['employeeId']; ?></td>
                          <td><?php echo $data['firstName']; ?></td>
                          <td><?php echo $data['lastName']; ?></td>
                          <td><?php echo $standardHours; ?></td>
                          <td><?php echo $extraHours; ?></td>
                          <td><?php echo $standardHours+$extraHours; ?></td>
                         </tr>
                        <?php $i++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
       </div>