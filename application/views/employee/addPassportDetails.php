            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <?php //$this->load->view('./templates/personalLinks',$data); ?>
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                <form method="post" action="<?php echo base_url('passportDetailsSubmit'); ?>"  enctype="multipart/form-data">
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">

                    <div class="row">
                    <div class="add-spouse-data" style="margin-left:18px;">
                                  <i class="fa fa-plus add-passport-content"></i> Add Passport
                                </div>
                                <div class="passport-list">                                    
                                  </div>
                    </div>

                      <div class="row">                               
                                 <?php $fTypes = $this->mainModel->getTypes('FAMILY_TYPE'); ?>
                                  <?php  if( count($passportDetails) > 0 ) {
                                  foreach($passportDetails AS $row){ ?>
                                     <input type="hidden" name="passportUniqueId[]" value="<?php echo $row['passportUniqueId'];?>">
                                      <input type="hidden" name="attachment11[]" value="<?php echo $row['attachment1'];?>">
                                      <input type="hidden" name="attachment22[]" value="<?php echo $row['attachment2'];?>">
                                      <input type="hidden" name="attachment33[]" value="<?php echo $row['attachment3'];?>">
                                       <div class="form-group col-md-2 col-sm-3 col-xs-8">

                                      <label>Select Family</label>
                                      <select class="form-control" name="employeeFamilyId[]" id="employeeFamilyId-0" required>
                                          <option value="">Select Type</option>
                                          <option <?php if($row['employeeFamilyId'] == '0'): echo 'selected'; endif; ?> value="0">Self</option>
                                          <?php foreach($familyDetails as $fType) {?>
                                            <option <?php if($fType['employeeFamilyId'] == $row['employeeFamilyId']): echo 'selected'; endif; ?> value="<?php echo $fType['employeeFamilyId'];?>"><?php echo $fType['firstName']." ".$fType['lastName'];?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>First Name</label>
                                                <input class="form-control firstName" id="firstName-0" placeholder="First Name" type="text" name="firstName[]" value="<?php echo $row['firstName']; ?>">
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Last Name</label>
                                                <input class="form-control lastName" id="lastName-0" placeholder="Last Name" name="lastName[]" type="text" value="<?php echo $row['lastName']; ?>">
                                        </div>

                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Passport No</label>
                                                <input class="form-control passportNo" id="passportNo-0" placeholder="Passport No" name="passportNo[]" type="text" value="<?php echo $row['passportNo']; ?>">
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Issue Date</label>
                                                <input class="form-control issueDate dateField" id="issueDate-0" placeholder="Issue Date" type="text" name="issueDate[]" value="<?php echo $row['issueDate']; ?>" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Place Of Issue</label>
                                                <input class="form-control placeOfIssue" id="placeOfIssue-0" placeholder="Place Of Issue No" name="placeOfIssue[]" type="text" value="<?php echo $row['placeOfIssue']; ?>">
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Expire Date</label>
                                                <input class="form-control expireDate dateField" id="expireDate-0" placeholder="Expire Date" type="text" name="expireDate[]"  value="<?php echo $row['expireDate']; ?>">
                                        </div>
                                         <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Country</label>
                                                <input class="form-control country" placeholder="Country" id="country-0" type="text" name="country[]" value="<?php echo $row['country']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment1</label>
                                                <input class="form-control attachment1" placeholder="" id="attachment1-0" type="file" name="attachment1[]" value="<?php echo $row['']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment2</label>
                                                <input class="form-control attachment2" placeholder="" id="attachment2-0" type="file" name="attachment2[]" value="<?php echo $row['']; ?>">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment3</label>
                                                <input class="form-control attachment3" placeholder="" id="attachment3-0" type="file" name="attachment3[]" value="<?php echo $row['']; ?>">
                                        </div>

                                         <a onclick="return confirm('Are you sure to delete?')" href="<?php echo base_url('deletePassport/?passportUniqueId='.$row['passportUniqueId'] );?>" >&nbsp;X</a>
                                    </div>
                                </div>
                              <?php } }else { ?> 
                                 <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                 <label>Select Family</label>
                                      <select class="form-control" name="employeeFamilyId[]" id="employeeFamilyId-0" required>
                                          <option value="">Select Type</option>
                                          <option value="0">Self</option>
                                          <?php foreach($familyDetails as $fType) {?>
                                            <option value="<?php echo $fType['employeeFamilyId'];?>"><?php echo $fType['firstName']." ".$fType['lastName'];?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>First Name</label>
                                                <input class="form-control firstName" id="firstName-0" placeholder="First Name" type="text" name="firstName[]">
                                        </div>                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Last Name</label>
                                                <input class="form-control lastName" id="lastName-0" placeholder="Last Name" name="lastName[]" type="text">
                                        </div>

                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Passport No</label>
                                                <input class="form-control passportNo" id="passportNo-0" placeholder="Passport No" name="passportNo[]" type="text">
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Issue Date</label>
                                                <input class="form-control issueDate dateField" id="issueDate-0" placeholder="Issue Date" type="text" name="issueDate[]" >
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Place Of Issue</label>
                                                <input class="form-control placeOfIssue" id="placeOfIssue-0" placeholder="Place Of Issue No" name="placeOfIssue[]" type="text">
                                        </div>
                                        
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Expire Date</label>
                                                <input class="form-control expireDate dateField" id="expireDate-0" placeholder="Expire Date" type="text" name="expireDate[]" >
                                        </div>
                                         <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Country</label>
                                                <input class="form-control country" placeholder="Country" id="country-0" type="text" name="country[]">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment1</label>
                                                <input class="form-control attachment1" placeholder="" id="attachment1-0" type="file" name="attachment1[]">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment2</label>
                                                <input class="form-control attachment2" placeholder="" id="attachment2-0" type="file" name="attachment2[]">
                                        </div>
                                        <div class="form-group col-md-2 col-sm-3 col-xs-8">
                                                <label>Attachment3</label>
                                                <input class="form-control attachment3" placeholder="" id="attachment3-0" type="file" name="attachment3[]">
                                        </div>
                                      <?php } ?>

                                </div>
                           <!--  </div> -->
                     </div> 


                  <a href="<?php echo base_url('visaDetails'); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-profile">
                <!-- <div class="card-avatar">
                  <a href="#pablo">
                    <?php if(empty($userInfo[0]['profilePic'])){
                      $profilePic = base_url()."assets/"."noimage.png";
                    }else{
                      $profilePic = base_url()."assets/profilePics/".$userInfo[0]['profilePic'];
                    } ?>
                    <img class="img" src="<?php echo $profilePic; ?>" style="width: 100%;"/>
                  </a>
                </div> -->
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
     
    
      <script>
       /*$(".add-kids-data").hide();
       $(".add-spouse-data").hide();
      $(".maritalStatus").on('change', function() {
            console.log("changed")
            let maritalStatus = $(this).val();
            if(maritalStatus == 'Yes'){
              $(".add-kids-data").show();
                $(".add-spouse-data").show();
            }else{
              $(".add-kids-data").hide();
                $(".add-spouse-data").hide();
            }
            
      });*/
      </script>