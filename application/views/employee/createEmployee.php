<div class="page-head-line">Create <?php echo $createUserType; ?></div>

            <div id="page-inner">
            <div class="row">
            
         <div class="panel">
                
                       
                              <div  class="spinner_icon" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/img/timer.gif">
            </div>
            <div class="error_message alert alert-danger" style="display:none;"></div>
            <div class="success_message alert alert-success" style="display:none;"></div>

                

             <form method="post" action="#" class="createEmployee" enctype="multipart/form-data">
                  <input type="hidden" name="createUserTypeCode" class="createUserTypeCode" value="<?php echo $createUserTypeCode; ?>">
                  <input type="hidden" name="createUserType" class="createUserType" value="<?php echo $createUserType; ?>">
                <div  class="col-md-8 col-sm-12">

                  <div class="row">
                  <div class="col-md-4 col-12">
                          <label class="bmd-label-floating">First name <span class="mandatory-label">*</span></label>
                        <div class="form-group">                        
                          <input type="text" required name="firstName" placeholder="First Name" maxlength="25" class="form-control firstName">
                        </div>
                      </div> 
                      <div class="col-md-4 col-sm-12">
                        <label class="bmd-label-floating">Last name <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="lastName" placeholder="Last Name" maxlength="25" class="form-control lastName">
                        </div>
                      </div>  
             
                         <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Email Address <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="email" required name="email" placeholder="Email Address" maxlength="45" class="form-control email">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                       <div class="col-md-4 col-sm-12">
                        <label class="bmd-label-floating">Mobile Number <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" required name="mobile" placeholder="Mobile Number" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control mobile" >
                        </div>
                      </div>  
                      
                       <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Gender <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="gender" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Other">Other</option>
                          </select>
                        </div>
                      </div>  
                       <div class="col-md-4 col-sm-12">
                        <label class="bmd-label-floating">Address <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                          <input type="text" name="address" placeholder="Address" class="form-control address">
                        </div>
                      </div>  
                      </div>
                      
                      <div class="row">
                      <div class="col-md-4 ccol-sm-12">
                          <label class="bmd-label-floating">Passport Number </label>
                        <div class="form-group">
                          <input type="text" name="passportNumber" maxlength="12" placeholder="Passport Number" class="form-control passportNumber">
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-12">
                        <label class="bmd-label-floating">SSN Number </label>
                        <div class="form-group">
                          <input type="text" name="ssnNumber" maxlength="9" placeholder="SSN Number" class="form-control ssnNumber">
                        </div>
                      </div>      
                      
                      
                         <div class="col-md-4 col-sm-12">
                           <label class="bmd-label-floating">Start Date </label>
                        <div class="form-group">
                          <input type="text" required name="startDate" autocomplete="off" id="fdate" placeholder="Start Date" class="form-control startDate">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">End Date </label>
                        <div class="form-group">
                          <input type="text" name="endDate" autocomplete="off" id="ldate" placeholder="End Date" class="form-control endDate">
                        </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-12">
                         <label class="bmd-label-floating">Date Of Birth</label>
                        <div class="form-group">
                          <input type="text" required name="dob" autocomplete="off" id="dob" placeholder="Date Of Birth" class="form-control dob">
                        </div>
                      </div>
                      
                        <div class="col-md-4 col-sm-12">
                          <label class="bmd-label-floating">Status </label>
                         
                        <div class="form-group">
                           <select name="userStatus" required class="form-control userStatus">
                             <!--  <option value="">Select Status</option> -->
                              <option value="0">In Active</option>
                              <option value="1">Active</option>
                          </select>
                        </div>
                      </div>   
                      </div>

            </div>
            <div  class="col-md-4 col-12">

                <div class="add-item-data" style="margin:40px 0 0 20px; float:left"><i class="fa fa-plus add-item-content"></i> </div>
                                <div class="items-list">
                                    <div class="item-data row">                                        
                                        <div class="form-group col-md-5 col-sm-12">
                                                <label>Doc Type</label>
                                               <?php $docs = $this->mainModel->getTypes('DOCUMENT_TYPE'); ?>
                                                 <select class="form-control documentType" name="documentType[]" id="documentType-0">
                                                  <option value="">Select Type</option>
                                                  <?php foreach($docs as $docInfo) {?>
                                                    <option value="<?php echo $docInfo['code'];?>"><?php echo $docInfo['value'];?></option>
                                                  <?php } ?>
                                                </select>
                                        </div>                                        
                                        <div class="form-group col-md-7 col-sm-12">
                                                <label>Attachment</label>
                                                <input class="form-control attachment" id="attachment-0" name="attachment[]" type="file">
                                        </div>                                        
                                    </div>
                                </div>

            </div>

            </div>

                  
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button>
                    <a href="<?php echo base_url($cancelUrl); ?>"  class="btn btn-primary pull-right">Cancel</a> 
                    </form>    
               
                    <div class="clearfix"></div>
                  
                </div>
              </div>

        