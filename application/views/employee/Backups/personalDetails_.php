
<div class="page-head-line">Personal Details</div>
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                            <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-12 col-sm-6">
               <div class="panel">
                       <div class="panel-body">
                <form method="post" action="<?php echo base_url('personalDetailsSubmit'); ?>"  enctype="multipart/form-data">
                     <input type="hidden" class="userUniqueId" name="userUniqueId" value="<?php echo $userInfo[0]['userUniqueId']; ?>">
                    <div class="row">
                         <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" required name="pdetails[firstName]" placeholder="Name" class="form-control" value="<?php echo $userInfo[0]['firstName']; ?>">
                        </div>
                      </div>                     
                   
                         <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" required name="pdetails[lastName]" placeholder="Name" class="form-control" value="<?php echo $userInfo[0]['lastName']; ?>">
                        </div>
                      </div> 

                      <div class="col-md-3">
                         <label class="bmd-label-floating">Date Of Birth</label>
                        <div class="form-group">
                          <input type="text" required name="pdetails[dob" value="<?php echo $userInfo[0]['dob']; ?>" autocomplete="off" id="dob]" placeholder="Date Of Birth" class="form-control dob">
                        </div>
                      </div>                     
                    
                       <div class="col-md-3">
                          <label class="bmd-label-floating">Gender <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="pdetails[gender]" class="form-control gender" required="required">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php if($userInfo[0]['gender'] == 'Male'): echo 'selected'; endif; ?>>Male</option>
                              <option value="Female" <?php if($userInfo[0]['gender'] == 'Female'): echo 'selected'; endif; ?>>Female</option>
                              <option value="Other" <?php if($userInfo[0]['Other'] == 'Male'): echo 'selected'; endif; ?>>Other</option>
                          </select>
                        </div>
                      </div>     

                       <div class="col-md-3">
                        <label class="bmd-label-floating">Address </label>
                        <div class="form-group">
                          <input type="text" name="pdetails[address]" placeholder="Address" class="form-control address" value="<?php echo $userInfo[0]['address']; ?>">
                        </div>
                      </div>  

                       <div class="col-md-3">
                          <label class="bmd-label-floating">Email </label>
                        <div class="form-group">
                          <input type="email" readonly name="pdetails[email]" placeholder="Email Address" maxlength="45" class="form-control email" value="<?php echo $userInfo[0]['email']; ?>">
                        </div>
                      </div>             
                   
                         <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Mobile</label>
                          <input type="text" readonly name="pdetails[mobile]" onkeypress="return isNumberKey(event)" placeholder="mobile" class="form-control" value="<?php echo $userInfo[0]['mobile']; ?>">
                        </div>
                      </div> 

                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Work Phone</label>
                          <input type="text" name="pdetails[workPhone]" placeholder="workPhone" onkeypress="return isNumberKey(event)" class="form-control" value="<?php echo $userInfo[0]['workPhone']; ?>">
                        </div>
                      </div> 

                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Home Phone</label>
                          <input type="text" name="pdetails[homePhone]" onkeypress="return isNumberKey(event)" placeholder="homePhone" class="form-control" value="<?php echo $userInfo[0]['homePhone']; ?>">
                        </div>
                      </div>    

                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Emergency Contact1</label>
                          <input type="text" name="pdetails[emergenctContact1]" onkeypress="return isNumberKey(event)" placeholder="Emergency Contact1" class="form-control" value="<?php echo $userInfo[0]['emergenctContact1']; ?>">
                        </div>
                      </div>    

                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Emergency Contact2</label>
                          <input type="text" name="pdetails[emergenctContact2]" onkeypress="return isNumberKey(event)" placeholder="Emergency Contact2" class="form-control" value="<?php echo $userInfo[0]['emergenctContact2']; ?>">
                        </div>
                      </div>       

                      <div class="col-md-3">
                          <label class="bmd-label-floating">Select Marital Status <span class="mandatory-label">*</span></label>
                        <div class="form-group">
                           <select name="pdetails[maritalStatus]" class="form-control maritalStatus" required="required">
                              <option value="">Select Marital Status</option>
                              <option value="No" <?php if($userInfo[0]['maritalStatus'] == 'No'): echo 'selected'; endif; ?>>No</option>
                              <option value="Yes" <?php if($userInfo[0]['maritalStatus'] == 'Yes'): echo 'selected'; endif; ?>>Yes</option>                             
                          </select>
                        </div>
                      </div>               
                    

                    </div>

                    <div  class="col-md-12 col-12">
                      <div class="row">
                                <div class="add-spouse-data" style="margin-left:18px;">
                                    <i class="fa fa-plus add-spouse-content"></i>
                                </div>
                                <div class="spouse-list">
                                    <div class="spouse-data row">
                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Spuse First Name</label>
                                                <input class="form-control firstName" id="firstName-0" type="text" name="firstName[]"> 
                                              

                                        </div>                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>LastName</label>
                                                <input class="form-control lastName" id="lastName-0" name="lastName[]" type="text">
                                        </div>
                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>DOB</label>
                                                <input class="form-control dob" id="dob-0" type="text" name="dob[]" >
                                        </div>
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Gender</label>
                                                 <select name="gender" class="form-control gender" id="gender-0">
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Email</label>
                                                <input class="form-control email" id="email-0" type="text" name="email[]">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Mobile</label>
                                                <input class="form-control mobile" id="mobile-0" type="text" name="mobile[]">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Address</label>
                                                <input class="form-control address" id="address-0" type="text" name="address[]">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                     </div>



                     <div class="col-md-12 col-12">
                      <div class="add-kids-data">
                                    <i class="fa fa-plus add-kids-content"></i>
                                </div><br>
                      <div class="row">
                                
                                <div class="kids-list">
                                    <div class="kids-data row">
                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Children First Name</label>
                                                <input class="form-control childrenFirstName" id="childrenFirstName-0" type="text" name="childrenFirstName[]"> 
                                              

                                        </div>                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>LastName</label>
                                                <input class="form-control childrenlastName" id="childrenlastName-0" name="childrenlastName[]" type="text">
                                        </div>
                                        
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>DOB</label>
                                                <input class="form-control childrenDob" id="childrenDob-0" type="text" name="childrenDob[]" >
                                        </div>
                                        <div class="form-group col-md-3 col-sm-3 col-xs-8">
                                                <label>Gender</label>
                                                 <select name="childrenGender" class="form-control childrenGender" id="childrenGender-0" >
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                     </div>

                
                  
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-profile">
                <!-- <div class="card-avatar">
                  <a href="#pablo">
                    <?php if(empty($userInfo[0]['profilePic'])){
                      $profilePic = base_url()."assets/"."noimage.png";
                    }else{
                      $profilePic = base_url()."assets/profilePics/".$userInfo[0]['profilePic'];
                    } ?>
                    <img class="img" src="<?php echo $profilePic; ?>" style="width: 100%;"/>
                  </a>
                </div> -->
                <!-- <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
     
    
      <script>
       $(".interested_section").hide();
       $(".rejection_section").hide();
       $(".chkConfirmedYes_section").hide();
       $(".chkConfirmedNo_section").hide();
         $(function () {
             
        $("#chkInterested").click(function () {
            if ($(this).is(":checked")) {
                $(".interested_section").show();
                $("#chkRejected").attr("disabled", true);
                $(".rejected_reason").removeAttr("required");
            } else {
                $(".interested_section").hide();
                $("#chkRejected").removeAttr("disabled");
            }
        });
        
        $("#chkRejected").click(function () {
            if ($(this).is(":checked")) {
                $("#chkInterested").attr("disabled", true);
                 $(".next_appointment").removeAttr("required");
                  $(".status").removeAttr("required");
                $(".rejection_section").show();
            } else {
                $(".rejection_section").hide();
                $("#chkInterested").removeAttr("disabled");
            }
        });
        
        
        //Confirmed Script
        
         $("#chkConfirmedYes").click(function () {
            if ($(this).is(":checked")) {
                $(".chkConfirmedYes_section").show();
                $("#chkConfirmedNo").attr("disabled", true);
                $(".not_confirmed_reason").removeAttr("required");
            } else {
                $(".chkConfirmedYes_section").hide();
                $("#chkConfirmedNo").removeAttr("disabled");
            }
        });
        
        $("#chkConfirmedNo").click(function () {
            if ($(this).is(":checked")) {
                $("#chkConfirmedYes").attr("disabled", true);
                // $(".next_appointment").removeAttr("required");
                  $(".confirmed_closing_status").removeAttr("required");
                $(".chkConfirmedNo_section").show();
            } else {
                $(".chkConfirmedNo_section").hide();
                $("#chkConfirmedYes").removeAttr("disabled");
            }
        });
        
        //end
        
    });
      </script>