<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Homecontroller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*$route['login'] = 'Servicescontroller/login';
$route['businessLogin'] = 'Servicescontroller/businessLogin';
$route['signup'] = 'Servicescontroller/signup';
//$route['createBusiness'] = 'Servicescontroller/createBusiness';
$route['getBusinessList'] = 'Servicescontroller/getBusinessList';
$route['verify'] = 'Servicescontroller/mobileEmailVerify';
$route['sendSms'] = 'Servicescontroller/sendSms';
$route['otpVerify'] = 'Servicescontroller/otpVerify';
$route['changePassword'] = 'Servicescontroller/userChangePassword';
$route['getProfile'] = 'Servicescontroller/getProfile';
$route['forgotPassword'] = 'Servicescontroller/forgotPassword';
$route['getUsers'] = 'Servicescontroller/getUsers';
$route['updateProfile'] = 'Servicescontroller/updateProfile';
$route['getBusinessProfile'] = 'Servicescontroller/getBusinessProfile';
$route['businessUpdateProfile'] = 'Servicescontroller/businessUpdateProfile';
$route['getTypes'] = 'Servicescontroller/getTypes';
$route['deleteUser'] = 'Servicescontroller/deleteUser';
$route['uploadProfilePic'] = 'Servicescontroller/uploadProfilePic';
$route['uploadBusinessLogo'] = 'Servicescontroller/uploadBusinessLogo';*/

//Web
$route['login'] = 'Homecontroller/login';
$route['business-signup'] = 'Homecontroller/businessSignup';
$route['saveNewBusiness'] = 'Homecontroller/saveNewBusiness';
$route['forgotPassword'] = 'Homecontroller/forgotPassword';
$route['submitForgotPassword'] = 'Homecontroller/submitForgotPassword';
$route['loginAction'] = 'Homecontroller/loginAction';
$route['dashboard'] = 'Dashboardcontroller/dashboard';
$route['logout'] = 'Dashboardcontroller/logout';
$route['change-password'] = "Dashboardcontroller/changePassword";
$route['submitChangePassword'] = "Dashboardcontroller/submitChangePassword";
$route['updateProfile'] = "Dashboardcontroller/updateProfile";
$route['updateBusinessprofile'] = "Dashboardcontroller/updateBusinessProfile";
$route['updateBusiness'] = 'Dashboardcontroller/updateBusiness';
$route['profileSubmit'] = "Dashboardcontroller/profileSubmit";
$route['submitBusinessProfile'] = "Dashboardcontroller/submitBusinessProfile";

$route['userStatusUpdate'] ='Dashboardcontroller/userStatusUpdate';
$route['clientStatusUpdate'] ='Dashboardcontroller/clientStatusUpdate';
$route['vendorStatusUpdate'] ='Dashboardcontroller/vendorStatusUpdate';
$route['businessStatusUpdate'] ='Dashboardcontroller/businessStatusUpdate';
$route['projectStatusUpdate'] ='Dashboardcontroller/projectStatusUpdate';

$route['deleteDocument'] ='Dashboardcontroller/deleteDocument';

$route['createBusiness'] = 'Dashboardcontroller/createBusiness';
$route['saveBusiness'] = 'Dashboardcontroller/saveBusiness';
$route['businessList'] = 'Dashboardcontroller/businessList';
$route['editBusiness'] = 'Dashboardcontroller/editBusiness';
$route['updateBusiness'] = 'Dashboardcontroller/updateBusiness';
$route['deleteBusiness'] ='Dashboardcontroller/deleteBusiness';

$route['createEmployee'] ='Dashboardcontroller/createEmployee';
$route['saveEmployee'] = 'Dashboardcontroller/saveEmployee';
$route['updateEmployee'] ='Dashboardcontroller/updateEmployee';
$route['deleteEmployee'] ='Dashboardcontroller/deleteEmployee';
$route['editEmployee'] ='Dashboardcontroller/editEmployee';
$route['employeeList'] = 'Dashboardcontroller/employeeList';

$route['createHr'] ='Dashboardcontroller/createHr';
$route['saveHr'] = 'Dashboardcontroller/saveHr'; 
$route['updateHr'] ='Dashboardcontroller/updateHr';
$route['deleteHr'] ='Dashboardcontroller/deleteHr';
$route['editHr'] ='Dashboardcontroller/editHr';
$route['hrList'] = 'Dashboardcontroller/hrList';

$route['createAccountant'] ='Dashboardcontroller/createAccountant';
$route['saveAccountant'] = 'Dashboardcontroller/saveAccountant';
$route['updateAccountant'] ='Dashboardcontroller/updateAccountant';
$route['deleteAccountant'] ='Dashboardcontroller/deleteAccountant';
$route['editAccountant'] ='Dashboardcontroller/editAccountant';
$route['accountantList'] = 'Dashboardcontroller/accountantList';

$route['createVisa'] ='Dashboardcontroller/createVisa';
$route['saveVisa'] = 'Dashboardcontroller/saveVisa';
$route['updateVisa'] ='Dashboardcontroller/updateVisa';
$route['deleteVisa'] ='Dashboardcontroller/deleteVisa';
$route['editVisa'] ='Dashboardcontroller/editVisa';
$route['visaList'] = 'Dashboardcontroller/visaList';

$route['createBench'] ='Dashboardcontroller/createBench';
$route['saveBench'] = 'Dashboardcontroller/saveBench';
$route['updateBench'] ='Dashboardcontroller/updateBench';
$route['deleteBench'] ='Dashboardcontroller/deleteBench';
$route['editBench'] ='Dashboardcontroller/editBench';
$route['benchList'] = 'Dashboardcontroller/benchList';

$route['createVendor'] ='Dashboardcontroller/createVendor';
$route['saveVendor'] = 'Dashboardcontroller/saveVendor';
$route['updateVendor'] ='Dashboardcontroller/updateVendor';
$route['deleteVendor'] ='Dashboardcontroller/deleteVendor';
$route['editVendor'] ='Dashboardcontroller/editVendor';
$route['vendorList'] = 'Dashboardcontroller/vendorList';

$route['createVendor'] ='Dashboardcontroller/createVendor';
$route['saveVendor'] = 'Dashboardcontroller/saveVendor';
$route['updateVendor'] ='Dashboardcontroller/updateVendor';
$route['deleteVendor'] ='Dashboardcontroller/deleteVendor';
$route['editVendor'] ='Dashboardcontroller/editVendor';
$route['vendorList'] = 'Dashboardcontroller/vendorList';

$route['createClient'] ='Dashboardcontroller/createClient';
$route['saveClient'] = 'Dashboardcontroller/saveClient';
$route['updateClient'] ='Dashboardcontroller/updateClient';
$route['deleteClient'] ='Dashboardcontroller/deleteClient';
$route['editClient'] ='Dashboardcontroller/editClient';
$route['clientList'] = 'Dashboardcontroller/clientList';

$route['projectList'] ='Dashboardcontroller/projectList';
$route['createProject'] ='Dashboardcontroller/createProject';
$route['saveProject'] ='Dashboardcontroller/saveProject';
$route['deleteProject'] ='Dashboardcontroller/deleteProject';
$route['editProject'] ='Dashboardcontroller/editProject';
$route['updateProject'] ='Dashboardcontroller/updateProject';
$route['deleteProjectDocument'] ='Dashboardcontroller/deleteProjectDocument';

$route['getUserDocuments'] = 'Dashboardcontroller/getUserDocuments';
$route['documentPreview'] = 'Dashboardcontroller/documentPreview';


$route['passportDetails'] = 'Dashboardcontroller/passportDetails';
$route['personalDetails'] = 'Dashboardcontroller/personalDetails';
$route['educationDetails'] = 'Dashboardcontroller/educationDetails';
$route['employmentDetails'] = 'Dashboardcontroller/employmentDetails';
$route['visaDetails'] = 'Dashboardcontroller/visaDetails';
$route['myProjects'] = 'Dashboardcontroller/myProjects';

$route['addPersonalDetails'] = 'Dashboardcontroller/addPersonalDetails';
$route['addEducationDetails'] = 'Dashboardcontroller/addEducationDetails';
$route['addEmploymentDetails'] = 'Dashboardcontroller/addEmploymentDetails';
$route['addVisaDetails'] = 'Dashboardcontroller/addVisaDetails';
$route['addPassportDetails'] = 'Dashboardcontroller/addPassportDetails';

$route['educationDetailsSubmit'] = 'Dashboardcontroller/educationDetailsSubmit';
$route['employmentDetailsSubmit'] = 'Dashboardcontroller/employmentDetailsSubmit';
$route['personalDetailsSubmit'] = 'Dashboardcontroller/personalDetailsSubmit';
$route['visaDetailsSubmit'] = 'Dashboardcontroller/visaDetailsSubmit';
$route['passportDetailsSubmit'] = 'Dashboardcontroller/passportDetailsSubmit';


$route['deleteChildren'] = 'Dashboardcontroller/deleteChildren';
$route['deleteFamily'] = 'Dashboardcontroller/deleteFamily';
$route['deleteEducation'] = 'Dashboardcontroller/deleteEducation';
$route['deleteEmployment'] = 'Dashboardcontroller/deleteEmployment';
$route['deletePassport'] = 'Dashboardcontroller/deletePassport';
$route['deleteUserVisa'] = 'Dashboardcontroller/deleteUserVisa';

$route['addTimesheet'] = 'Dashboardcontroller/addTimesheet';
$route['editTimesheet'] = 'Dashboardcontroller/editTimesheet';
$route['checkTimesheet'] = 'Dashboardcontroller/checkTimesheet';
$route['saveTimesheet'] = 'Dashboardcontroller/saveTimesheet';
$route['viewTimesheet'] = 'Dashboardcontroller/viewTimesheet';

$route['addWeeklyReview'] = 'Dashboardcontroller/addWeeklyReview';
$route['saveWeeklyReview'] = 'Dashboardcontroller/saveWeeklyReview';
$route['viewWeeklyReviews'] = 'Dashboardcontroller/viewWeeklyReviews';
$route['approvalTimesheet'] = 'Dashboardcontroller/approvalTimesheet';

$route['approveComment'] = "Dashboardcontroller/approveComment";
$route['RejectComment'] = "Dashboardcontroller/RejectComment";

$route['approveReview'] = "Dashboardcontroller/approveReview";
$route['RejectReview'] = "Dashboardcontroller/RejectReview";

$route['timesheets'] = "Dashboardcontroller/timesheets";

$route['tickets'] = "Dashboardcontroller/tickets";
$route['createTicket'] = "Dashboardcontroller/createTicket";
$route['saveTicket'] = "Dashboardcontroller/saveTicket";

$route['approvalTimesshetsInfo'] = "Dashboardcontroller/approvalTimesshetsInfo";