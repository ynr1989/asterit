<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");

class Dashboardcontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('userTypeCode'))){
    		redirect(base_url(), 'Location');
    	}
		$this->load->model('MainDB_model', 'mainModel');		
    }

    public function global_functions(){    	
    	$data['userTypeCode'] = $this->session->userdata('userTypeCode');
    	$data['userType'] = $this->session->userdata('userType');
    	$data['loginUniqueId'] = $this->session->userdata('loginUniqueId'); 
    	if($data['userTypeCode'] == 2){
    		$data['userInfo'] = $this->mainModel->getBusinessInfo($data['loginUniqueId']);
    		$data['userName'] = $data['userInfo'][0]['ownerFirstName']." ".$data['userInfo'][0]['ownerLastName'];
    		$data['firstName'] = $data['userInfo'][0]['ownerFirstName'];
    		$data['lastName'] = $data['userInfo'][0]['lastName'];
    		$data['businessId'] = $data['userInfo'][0]['businessId'];
    		$data['businessUniqueId'] = $data['userInfo'][0]['businessUniqueId']; 
    		$data['passwordFlag'] = $data['userInfo'][0]['passwordFlag']; 
    	}else{
    		$data['userInfo'] = $this->mainModel->getUserInfo($data['loginUniqueId']);
    		$data['userName'] = $data['userInfo']['firstName']." ".$data['userInfo']['lastName'];
    		$data['businessId'] = $data['userInfo'][0]['businessId'];
    		$data['businessUniqueId'] = $data['userInfo'][0]['businessUniqueId'];
    		$data['passwordFlag'] = $data['userInfo'][0]['passwordFlag']; 
    	}

    	if($data['userTypeCode'] == 1){
    		$data['businessName'] = "Aster IT";
    		if($data['userInfo'][0]['profilePic']){
    			$pic =  base_url()."assets/profilePics/".$data['userInfo'][0]['profilePic'];
    		}else{
    			$pic =  base_url()."assets/"."noimage.png";;
    		}
    		$data['businessLogo'] = $pic;
    	}else{
    		$data['bInfo'] = $this->mainModel->getBusinessInfo($data['loginUniqueId']);
    		$data['businessName'] = $data['bInfo'][0]['businessName'];

    		if($data['bInfo'][0]['businessLogo']){
    			$pic =  base_url()."assets/profilePics/".$data['bInfo'][0]['businessLogo'];
    		}else{
    			$pic =  base_url()."assets/"."noimage.png";;
    		}
    		
    		$data['businessLogo'] = $pic;
    	}

    	$segment = $this->uri->segment(1);    	
    	if(($segment!= 'change-password' && $segment!='submitChangePassword') && $data['userTypeCode']!=1){
    		if($data['passwordFlag'] == 0){
    			$this->session->set_flashdata('message1', 'Please change password for security reasons');
    			redirect(base_url('change-password'), 'Location');
    		}
    	}
    	
    	return $data;
    }
	
	public function dashboard() {
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);	
		$this->load->view('dashboard',$data);	
		$this->load->view('templates/footer',$data);	
	}   

	public function changePassword(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/change-password',$data);
		$this->load->view('templates/footer');
	}

	public function submitChangePassword(){
		$data = $this->global_functions();	
		$password = $this->input->post('password'); 
		$confirm_password = $this->input->post('confirm_password');
		if($password==$confirm_password) {
			$pw = md5($password);
			$form_data = array(
			'password' => $pw,
			'test_password' =>$password,
			'passwordFlag' =>'1'	
			);		

			if($data['userTypeCode'] == 2){
				$update = $this->mainModel->updateBusinessPassword($form_data,$data['loginUniqueId']);
			}else{
				$update = $this->mainModel->updateUserPassword($form_data,$data['loginUniqueId']);
			}
			
		    $this->session->set_flashdata('message', 'Password updated successfully');
			redirect(base_url('change-password'), 'Location');
		}else{
		    $this->session->set_flashdata('message', 'Confirm password wrong.');
			redirect(base_url('change-password'), 'Location');
		}
	}

 	public function userStatusUpdate(){
    	$status= $this->input->post('userStatus');
    	$userUniqueId= $this->input->post('userUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'userStatus' =>$status
		    );
		    $this->mainModel->userStatusUpdate($form_data,$userUniqueId);
        
        echo json_encode($response);
    }

     public function businessStatusUpdate(){
    	$status= $this->input->post('businessStatus');
    	$userUniqueId= $this->input->post('businessUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'businessStatus' =>$status
		    );
		    $this->mainModel->businessStatusUpdate($form_data,$userUniqueId);
        echo json_encode($response);
    }

	public function updateProfile(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/update-profile',$data);
		$this->load->view('templates/footer');
	}

	public function updateBusinessProfile(){
		$data = $this->global_functions();
        $data['businessInfo'] = $this->mainModel->getBusinessInfo($data['loginUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('business/updateBusinessprofile',$data);
		$this->load->view('templates/footer');
	}

	public function profileSubmit() {
		$data = $this->global_functions();
		if($_FILES["profilePic"]["name"]!='') {
			$target_dir = "assets/profilePics/";
			$target_file = $target_dir . basename($_FILES["profilePic"]["name"]);
			$file_name = basename($_FILES["profilePic"]["name"]);
			move_uploaded_file($_FILES["profilePic"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['profilePic'];
		}
		$form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' => $this->input->post('lastName'),
			'address' => $this->input->post('address'),
			'gender' => $this->input->post('gender'),
			'profilePic' => $file_name,
			'panNumber' => $this->input->post('panNumber'),
			'passportNumber' => $this->input->post('passportNumber'),
		);
		
		$update = $this->mainModel->updateProfileDetails($form_data,$data['loginUniqueId']);
		$this->session->set_flashdata('message', 'Profile updated successfully');		
		redirect(base_url('updateProfile'), 'Location');
	}

	public function createBusiness(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('business/createBusiness',$data);
		$this->load->view('templates/footer');
	}

	public function saveBusiness(){
    	$response = array();
        $usertype = "2";
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->mainModel->generateBusinessID();
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
            'userTypeCode' =>BUSINESS,
            'userType' =>'Business',
			'businessId' =>  $businessId,
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus')
		);
		 
		$check = $this->mainModel->checkBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertBusines = $this->mainModel->insertBusiness($form_data);
			
			if($insertBusines>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Business created successfully.';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}			
		}else{
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function businessList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['businessData'] = $this->mainModel->getBusinessList();	
		$this->load->view('templates/header',$data);
		$this->load->view('business/businessList',$data);
		$this->load->view('templates/footer');
	} 

	public function deleteBusiness(){
        $userid = $this->input->get('businessUniqueId');
        $delete = $this->mainModel->deleteBusiness($userid);
        if($delete){
            $this->session->set_flashdata('message', 'Business deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Business delete Failed');
        }
        redirect(base_url('businessList'), 'Location');
    }

    public function editBusiness(){
		$data = $this->global_functions();	
		$data['businessUniqueId'] = $this->input->get('businessUniqueId');
        $data['businessInfo'] = $this->mainModel->getBusinessInfo($data['businessUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('business/editBusiness',$data);
		$this->load->view('templates/footer');
	}

	public function updateBusiness(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->mainModel->generateBusinessID();
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			//'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus')			
		);
		$businessUniqueId = $this->input->post('businessUniqueId');
		$check = $this->mainModel->checkExistBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'),$businessUniqueId);
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$insertBusines = $this->mainModel->updateBusiness($form_data,$businessUniqueId);			
			if($insertBusines>0) {
				$response['message'] = 'Business updated successfully.';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function submitBusinessProfile(){
		$data = $this->global_functions();
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');

        if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['businessLogo'];
		}

        $form_data = array(
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			//'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'businessLogo' =>	$file_name		
		);
		$businessUniqueId = $data['loginUniqueId'];
		$check = $this->mainModel->checkExistBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'),$businessUniqueId);
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$insertBusines = $this->mainModel->updateBusiness($form_data,$businessUniqueId);			
			if($insertBusines>0) {
				$this->session->set_flashdata('message', 'Profile updated successfully');
			}else{
				$this->session->set_flashdata('message', 'Server Error');
			}			
		}else if($check=='1'){
			$this->session->set_flashdata('message', 'Mobile Already Exist');
		}else if($check=='2'){
			$this->session->set_flashdata('message', 'Email Already Exist');
		}else if($check=='3'){
			$this->session->set_flashdata('message', 'Mobile/Email Already Exist');
		}	
       redirect(base_url('updateBusinessprofile'), 'Location');
	}

	public function createEmployee(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveEmployee(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' => EMPLOYEE,
            'userType' =>'Employee',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Employee created successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function employeeList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editEmployee(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateEmployee(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Employee updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteEmployee(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Employee deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Employee delete Failed');
        }
        redirect(base_url('employeeList'), 'Location');
    }

    //HR
    public function createHr(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('hr/createHr',$data);
		$this->load->view('templates/footer');
	}

	public function saveHr(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>HR,
            'userType' =>'HR',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'HR created successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function hrList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getUsers(HR,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('hr/hrList',$data);
		$this->load->view('templates/footer');
	} 

	public function editHr(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('hr/editHr',$data);
		$this->load->view('templates/footer');
	}

	public function updateHr(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'HR updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteHr(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'HR deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'HR delete Failed');
        }
        redirect(base_url('hrList'), 'Location');
    }

    //Accountant

    public function createAccountant(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('accountant/createAccountant',$data);
		$this->load->view('templates/footer');
	}

	public function saveAccountant(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>ACCOUNTANT,
            'userType' =>'Accountant',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			//$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {				
			    $this->uploadDocuments($insertUser);
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Accountant created successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function uploadDocuments($userUniqueId){
		for($i = 0; $i <= count($this->input->post('documentType')); $i++) {				
			$documentType = $this->input->post('documentType');
			$attachment = $this->input->post('attachment');
			$values = array($documentType[$i], $attachment[$i]);			
			if(!empty($documentType[$i]) && $documentType[$i]>0 &&(count(array_unique($values))!=1))
			{	
				if($_FILES["attachment"]["name"]!='') {
					$target_dir = "assets/documents/";
					$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
					$file_name = basename($_FILES["attachment"]["name"][$i]);
					move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
				} 

				$documentsData = array(
					"documentTypeId" => $documentType[$i],
					"documentType" => $this->mainModel->getTypeName($documentType[$i],'DOCUMENT_TYPE'),
					"attachment" => $file_name,
				    "userUniqueId" => $userUniqueId,
				);
				$this->mainModel->userDocuments($documentsData);
			}
		}
	}

	public function accountantList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getUsers(ACCOUNTANT,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('accountant/accountantList',$data);
		$this->load->view('templates/footer');
	} 

	public function editAccountant(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('accountant/editAccountant',$data);
		$this->load->view('templates/footer');
	}

	public function updateAccountant(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Accountant updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteAccountant(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Accountant deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Accountant delete Failed');
        }
        redirect(base_url('accountantList'), 'Location');
    }

    //Visa
    public function createVisa(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('visa/createVisa',$data);
		$this->load->view('templates/footer');
	}

	public function saveVisa(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>VISA,
            'userType' =>'Visa',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Visa created successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function visaList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getUsers(VISA,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('visa/visaList',$data);
		$this->load->view('templates/footer');
	} 

	public function editVisa(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('visa/editVisa',$data);
		$this->load->view('templates/footer');
	}

	public function updateVisa(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Visa updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteVisa(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Visa deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Visa delete Failed');
        }
        redirect(base_url('visaList'), 'Location');
    }

    //Bench
    public function createBench(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('bench/createBench',$data);
		$this->load->view('templates/footer');
	}

	public function saveBench(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>BENCH,
            'userType' =>'Bench',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Bench created successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function benchList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getUsers(BENCH,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('bench/benchList',$data);
		$this->load->view('templates/footer');
	} 

	public function editBench(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('bench/editBench',$data);
		$this->load->view('templates/footer');
	}

	public function updateBench(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Bench updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteBench(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Bench deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Bench delete Failed');
        }
        redirect(base_url('visaList'), 'Location');
    }

    //Vendor
     public function createVendor(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/createVendor',$data);
		$this->load->view('templates/footer');
	}

	public function saveVendor(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $vendorId = $this->mainModel->generateVendorID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>VENDOR,
            'userType' =>'Vendor',
            'vendorId' => $vendorId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'gender' => $this->input->post('gender'),
			'vendorStatus' => $this->input->post('vendorStatus')
		);
		 
		$check = $this->mainModel->checkVendorAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertVendor($form_data);
			
			if($insertUser>0) {    		
				$response['message'] = 'Vendor created successfully.';
				$response['status'] = true;
                $response['vendorUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function vendorList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getVendors($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/vendorList',$data);
		$this->load->view('templates/footer');
	} 

	public function editVendor(){
		$data = $this->global_functions();	
		$data['vendorUniqueId'] = $this->input->get('vendorUniqueId');
        $data['vendorInfo'] = $this->mainModel->getVendorInfo($data['vendorUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/editVendor',$data);
		$this->load->view('templates/footer');
	}

	public function updateVendor(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'gender' => $this->input->post('gender'),
			'vendorStatus' => $this->input->post('vendorStatus')		
		);
		$vendorUniqueId = $this->input->post('vendorUniqueId');
		$check = $this->mainModel->checkExistVendorAvailable($this->input->post('mobile'),$this->input->post('email'),$vendorUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateVendor($form_data,$vendorUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Vendor updated successfully.';
				$response['status'] = true;
                $response['vendorUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteVendor(){
        $userUniqueId = $this->input->get('vendorUniqueId');
        $delete = $this->mainModel->deleteVendor($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Vendor deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Vendor delete Failed');
        }
        redirect(base_url('vendorList'), 'Location');
    }

    //Clients
     public function createClient(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('client/createClient',$data);
		$this->load->view('templates/footer');
	}

	public function saveClient(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $clientrId = $this->mainModel->generateClientID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>CLIENT,
            'userType' =>'Client',
            'clientId' => $clientrId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'gender' => $this->input->post('gender'),
			'clientStatus' => $this->input->post('clientStatus')
		);
		 
		$check = $this->mainModel->checkClientAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertClient($form_data);
			
			if($insertUser>0) {    		
				$response['message'] = 'Client created successfully.';
				$response['status'] = true;
                $response['clinetUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function clientList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('client/clientList',$data);
		$this->load->view('templates/footer');
	} 

	public function editClient(){
		$data = $this->global_functions();	
		$data['clientUniqueId'] = $this->input->get('clientUniqueId');
        $data['clientInfo'] = $this->mainModel->getClientInfo($data['clientUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('client/editClient',$data);
		$this->load->view('templates/footer');
	}

	public function updateClient(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'gender' => $this->input->post('gender'),
			'clientStatus' => $this->input->post('clientStatus')		
		);
		$clientUniqueId = $this->input->post('clientUniqueId');
		$check = $this->mainModel->checkExistClientAvailable($this->input->post('mobile'),$this->input->post('email'),$clientUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateClient($form_data,$clientUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Client updated successfully.';
				$response['status'] = true;
                $response['clientUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteClient(){
        $clientUniqueId = $this->input->get('clientUniqueId');
        $delete = $this->mainModel->deleteClient($clientUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Client deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Client delete Failed');
        }
        redirect(base_url('clientList'), 'Location');
    }

    public function getUserDocuments(){
		if(isset($_POST["userUniqueId"]))  
	 	{  
 		$data = $this->global_functions();	
		$userUniqueId = $this->input->post('userUniqueId');
        $docInfo = $this->mainModel->getUserDocuments($userUniqueId);
	      $output = '';
	      $output .= '  
	      <div class="table-responsive">  
	           <table class="table table-bordered"><tr><th>Document Type</th><th>Attachment</th></tr>';  
	      foreach($docInfo as $docInfos)  
	      {   $path = base_url()."assets/documents/".$docInfos["attachment"];
	           $output .= '  
	                <tr> <td >'.$docInfos["documentType"].'</td>   
	                     <td><a href="'.$path.'" download>'.$docInfos["attachment"].'</a></td>  
	                </tr> 
	                ';  
	      }  
	      $output .= "</table></div>";  
	      echo $output;  
 		}
    }

	public function logout() {	
		$this->session->sess_destroy();
		redirect(base_url('/'), 'Location');
	}

    
}