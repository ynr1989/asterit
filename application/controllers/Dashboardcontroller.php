<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");
 
class Dashboardcontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('userTypeCode'))){
    		redirect(base_url(), 'Location');
    	}
		$this->load->model('MainDB_model', 'mainModel');		
    } 

    public function global_functions(){    	
    	$data['userTypeCode'] = $this->session->userdata('userTypeCode');
    	$data['userType'] = $this->session->userdata('userType'); 
    	$data['loginUniqueId'] = $this->session->userdata('loginUniqueId'); 
    	if($data['userTypeCode'] == 2){
    		$data['userInfo'] = $this->mainModel->getBusinessInfo($data['loginUniqueId']);
    		$data['userName'] = $data['userInfo'][0]['ownerFirstName']." ".$data['userInfo'][0]['ownerLastName'];
    		$data['firstName'] = $data['userInfo'][0]['ownerFirstName'];
    		$data['lastName'] = $data['userInfo'][0]['lastName'];
    		$data['businessId'] = $data['userInfo'][0]['businessId'];
    		$data['businessUniqueId'] = $data['userInfo'][0]['businessUniqueId']; 
    		$data['passwordFlag'] = $data['userInfo'][0]['passwordFlag']; 
    		$data['employeeId'] = '';
    		//$data['profilePic'] = $data['userInfo'][0]['passwordFlag']; 
    		//$data['employeeId'] = ''; 

    		$data['employeeCount'] = $this->mainModel->getUsersByType(EMPLOYEE,$data['businessUniqueId']);
    		$data['clientCount'] = $this->mainModel->getUsersByType(CLIENT,$data['businessUniqueId']);
    		$data['benchCount'] = $this->mainModel->getUsersByType(BENCH,$data['businessUniqueId']);
    		//$data['employeeCount'] = $this->mainModel->getUsersByType(EMPLOYEE,$data['businessUniqueId']);
    	}else{ 
    		$data['userInfo'] = $this->mainModel->getUserInfo($data['loginUniqueId']);
    		$data['userName'] = $data['userInfo'][0]['firstName']." ".$data['userInfo'][0]['lastName'];
    		$data['businessId'] = $data['userInfo'][0]['businessId'];
    		$data['businessUniqueId'] = $data['userInfo'][0]['businessUniqueId'];
    		$data['passwordFlag'] = $data['userInfo'][0]['passwordFlag']; 
    		$data['employeeId'] = $data['userInfo'][0]['employeeId']; 
    	}
 
    	if($data['userTypeCode'] == 1){
    		$data['businessName'] = "Aster IT";
    		if($data['userInfo'][0]['profilePic']){
    			$pic =  base_url()."assets/profilePics/".$data['userInfo'][0]['profilePic'];
    		}else{
    			$pic =  base_url()."assets/"."noimage.png";;
    		}
    		$data['businessLogo'] = $pic;
    	}else{
    		$data['bInfo'] = $this->mainModel->getBusinessInfo($data['loginUniqueId']);
    		$data['businessName'] = $data['bInfo'][0]['businessName'];

    		if($data['bInfo'][0]['businessLogo']){
    			$pic =  base_url()."assets/businessLogos/".$data['bInfo'][0]['businessLogo'];
    		}else{
    			$pic =  base_url()."assets/"."noimage.png";;
    		}
    		
    		$data['businessLogo'] = $pic;
    	}

    	$segment = $this->uri->segment(1);    	
    	if(($segment!= 'change-password' && $segment!='submitChangePassword') && $data['userTypeCode']!=1){
    		if($data['passwordFlag'] == 0){
    			$this->session->set_flashdata('message1', 'Please change password for security reasons');
    			redirect(base_url('change-password'), 'Location');
    		}
    	}
    	
    	return $data;
    }
	
	public function dashboard() {
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);	
		$this->load->view('dashboard',$data);	
		$this->load->view('templates/footer',$data);	
	}   

	public function changePassword(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/change-password',$data); 
		$this->load->view('templates/footer');
	}

	public function tickets(){
		$data = $this->global_functions();		
		$data['tickets'] = $this->mainModel->getTickets($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('tickets/tickets',$data); 
		$this->load->view('templates/footer');
	}

	public function createTicket(){
		$data = $this->global_functions();		
		$data['allemployees'] = $this->mainModel->getUsers("4",$data['businessUniqueId']);
		$data['tickets'] = $this->mainModel->getTickets($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('tickets/create-tickets',$data); 
		$this->load->view('templates/footer');
	}

	public function saveTicket(){
		$tdata = $this->input->post('tdata'); 
		$tdata['ticketId'] = $this->mainModel->generateTicketID();
		$ticketUniqueId = $this->mainModel->insertBusiness($form_data);

		$time = time();
		$target_dir = "assets/ticketDocuments/";
		if($_FILES["attachment1"]["name"][$i]!='') {
			$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
			$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$i]);
			move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
		}else{
			$attachment1 = $attachment11[$i];
		}

		$this->session->set_flashdata('message', 'Ticket created successfully.');
		redirect(base_url('tickets'), 'Location');
	}

	public function submitChangePassword(){
		$data = $this->global_functions();	
		$password = $this->input->post('password'); 
		$confirm_password = $this->input->post('confirm_password');
		if($password==$confirm_password) {
			$pw = md5($password);
			$form_data = array(
			'password' => $pw,
			'test_password' =>$password,
			'passwordFlag' =>'1'	
			);		

			if($data['userTypeCode'] == 2){
				$update = $this->mainModel->updateBusinessPassword($form_data,$data['loginUniqueId']);
			}else{
				$update = $this->mainModel->updateUserPassword($form_data,$data['loginUniqueId']);
			}
			$this->session->set_flashdata('messageStatus', 'success');
		    $this->session->set_flashdata('message', 'Password updated successfully');
			redirect(base_url('change-password'), 'Location');
		}else{
		    $this->session->set_flashdata('message1', 'Confirm password wrong.');
			redirect(base_url('change-password'), 'Location');
		}
	}

 	public function userStatusUpdate(){
    	$status= $this->input->post('userStatus');
    	$userUniqueId= $this->input->post('userUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'userStatus' =>$status
		    );
		    $this->mainModel->userStatusUpdate($form_data,$userUniqueId);
        
        echo json_encode($response);
    }

    public function clientStatusUpdate(){
    	$clientStatus= $this->input->post('clientStatus');
    	$clientUniqueId= $this->input->post('clientUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'clientStatus' =>$clientStatus
		    );
		    $this->mainModel->clientStatusUpdate($form_data,$clientUniqueId);
        
        echo json_encode($response);
    }

    public function vendorStatusUpdate(){
    	$vendorStatus= $this->input->post('vendorStatus');
    	$vendorUniqueId= $this->input->post('vendorUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
        
	    $form_data = array(
			'vendorStatus' =>$vendorStatus
		    );
		    $this->mainModel->vendorStatusUpdate($form_data,$vendorUniqueId);
        
        echo json_encode($response);
    }

     public function businessStatusUpdate(){
    	$status= $this->input->post('businessStatus');
    	$userUniqueId= $this->input->post('businessUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'businessStatus' =>$status
		    );
		    $this->mainModel->businessStatusUpdate($form_data,$userUniqueId);
        echo json_encode($response);
    }
    

    public function personalDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}	
		$data['familyDetails'] = $this->mainModel->getFamilyDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/personalDetails',$data);
		$this->load->view('templates/footer');
	}

	public function educationDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}	
		$data['educationDetails'] = $this->mainModel->getEducationDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/educationDetails',$data);
		$this->load->view('templates/footer');
	}


	public function employmentDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['employmentDetails'] = $this->mainModel->getEmploymentDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employmentDetails',$data);
		$this->load->view('templates/footer');
	}

	public function visaDetails(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['visaDetails'] = $this->mainModel->getVisaDetails($data['loginUniqueId']);
		$data['passportDetails'] = $this->mainModel->getPassportDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/visaDetails',$data);
		$this->load->view('templates/footer');
	}

	public function myProjects(){
		$data = $this->global_functions();	
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['projectData'] = $this->mainModel->getMyProjects($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/myProjects',$data);
		$this->load->view('templates/footer');
	}

	public function viewTimesheet(){
		$data = $this->global_functions();
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/viewTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function viewWeeklyReviews(){
		$data = $this->global_functions();
		if(!empty($this->input->get('userUniqueId'))){
			$data['loginUniqueId'] = $this->input->get('userUniqueId');
		}
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/viewWeeklyReviews',$data);
		$this->load->view('templates/footer');
    }

	public function addPersonalDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}	
		$data['familyDetails'] = $this->mainModel->getFamilyDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addPersonalDetails',$data);
		$this->load->view('templates/footer');
	}

	public function addEducationDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}	
		$data['educationDetails'] = $this->mainModel->getEducationDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addEducationDetails',$data);
		$this->load->view('templates/footer');
	}


	public function addEmploymentDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}
		$data['employmentDetails'] = $this->mainModel->getEmploymentDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addEmploymentDetails',$data);
		$this->load->view('templates/footer');
	}

	public function addVisaDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}
		$data['familyDetails'] = $this->mainModel->getFamilyDetails($data['loginUniqueId']);
		$data['visaDetails'] = $this->mainModel->getVisaDetails($data['loginUniqueId']);
		$data['passportDetails'] = $this->mainModel->getPassportDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addVisaDetails',$data);
		$this->load->view('templates/footer');
	}

	public function addPassportDetails(){
		$data = $this->global_functions();	
		if($data['userTypeCode'] !=4){
			redirect(base_url('dashboard'), 'Location');
		}
		$data['familyDetails'] = $this->mainModel->getFamilyDetails($data['loginUniqueId']);		
		$data['passportDetails'] = $this->mainModel->getPassportDetails($data['loginUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('employee/addPassportDetails',$data);
		$this->load->view('templates/footer');
	}

	public function personalDetailsSubmit() {
		$data = $this->global_functions();
		$userUniqueId = $this->input->post('userUniqueId');
		$postData = $this->input->post('pdetails'); 
		$update = $this->mainModel->updatePersonalDetails($postData,$userUniqueId);
		$employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
		
		for($i = 0; $i <= count($this->input->post('firstName')); $i++) {				
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');
			$dob = $this->input->post('dob');
			$gender = $this->input->post('gender');
			$email = $this->input->post('email');
			$mobile = $this->input->post('mobile');
			$address = $this->input->post('address');
			$familyType = $this->input->post('familyType'); 
			$values = array($firstName[$i], $lastName[$i], $dob[$i], $gender[$i], $email[$i], $mobile[$i], $address[$i], $familyType[$i]);	
			$employeeFamilyId = $this->input->post('employeeFamilyId');	
			//if(!empty($firstName[$i]) && $firstName[$i]>0 &&(count(array_unique($values))!=1))	
			if((count(array_unique($values))!=1))
			{	
				$fData[] = [
					"employeeFamilyId" => ( isset( $employeeFamilyId[ $i ] ) ? $employeeFamilyId[ $i ] : '' ),
					"firstName" => $firstName[$i],
					"lastName" => $lastName[$i],
				    "dob"       => $dob[$i],
				    "gender"  => $gender[$i],
				    "email"       => $email[$i],
				    "mobile"       => $mobile[$i],
				    "userUniqueId"       => $userUniqueId,
				    "address"       => $address[$i],
				    "employeeId" => $employeeId,
				    "businessUniqueId" => $data['businessUniqueId'],
				    "businessId" => $data['businessId'],
				    "familyType" =>  $familyType[$i]
				];
			}
		}
		//echo "<hr><pre>"; print_r($fData); echo "</pre>"; die();
		$update = $this->mainModel->updateEmployeeFamilyDetails($userUniqueId,$fData);

		//Childrens Data
		/*for($i = 0; $i <= count($this->input->post('childrenFirstName')); $i++) {				
			$childrenFirstName = $this->input->post('childrenFirstName');
			$childrenlastName = $this->input->post('childrenlastName');
			$childrenDob = $this->input->post('childrenDob');
			$childrenGender = $this->input->post('childrenGender');
			$values = array($childrenFirstName[$i], $childrenlastName[$i], $childrenDob[$i], $childrenGender[$i]);		
			$childrenId = $this->input->post('childrenId');		
			if(!empty($childrenFirstName[$i]) && $childrenFirstName[$i]>0 &&(count(array_unique($values))!=1))
			{	
				$cData[] = [
					"childrenId" => ( isset( $childrenId[ $i ] ) ? $childrenId[ $i ] : '' ),
					"firstName" => $childrenFirstName[$i],
					"lastName" => $childrenlastName[$i],
				    "dob"       => $childrenDob[$i],
				    "gender"  => $childrenGender[$i],
				    "userUniqueId"       => $userUniqueId,
				];
			}
		}
		$update = $this->mainModel->updateChildrenDetails($userUniqueId,$cData);*/

		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('personalDetails'), 'Location');		 
	}

	public function passportDetailsSubmit(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];		
		$employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
		
		for($i = 0; $i <= count($this->input->post('firstName')); $i++) {				
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');
			$passportNo = $this->input->post('passportNo');
			$issueDate = $this->input->post('issueDate');
			$placeOfIssue = $this->input->post('placeOfIssue');
			$expireDate = $this->input->post('expireDate'); 
			$country = $this->input->post('country');
			$employeeFamilyId = $this->input->post('employeeFamilyId'); 

			$values = array($firstName[$i], $lastName[$i], $passportNo[$i], $issueDate[$i], $placeOfIssue[$i], $expireDate[$i], $country[$i], $employeeFamilyId[$i]);	
			$passportUniqueId = $this->input->post('passportUniqueId');	

			$attachment11 = $this->input->post('attachment11');	
			$attachment22 = $this->input->post('attachment22');	
			$attachment33 = $this->input->post('attachment33');	
			if(!empty($firstName[$i]) && (count(array_unique($values))!=1))	
			//if((count(array_unique($values))!=1))
			{	
				 $attachment1 = '';
				 $attachment2 = '';
				 $attachment3 = '';
				 $time = time();
				 $target_dir = "assets/passportDocuments/";
				if($_FILES["attachment1"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
					$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$i]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
				}else{
					$attachment1 = $attachment11[$i];
				}
				 
				if($_FILES["attachment2"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment2"]["name"][$i]);
					$attachment2 = $time."_".basename($_FILES["attachment2"]["name"][$i]);
					move_uploaded_file($_FILES["attachment2"]["tmp_name"][$i], $target_file);
				}else{
					$attachment2 = $attachment22[$i];
				}

				if($_FILES["attachment3"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment3"]["name"][$i]);
					$attachment3 = $time."_".basename($_FILES["attachment3"]["name"][$i]);
					move_uploaded_file($_FILES["attachment3"]["tmp_name"][$i], $target_file);
				}else{
					$attachment3 = $attachment33[$i];
				}

				$fData[] = [
					"passportUniqueId" => ( isset( $passportUniqueId[ $i ] ) ? $passportUniqueId[ $i ] : '' ),
					"firstName" => $firstName[$i],
					"lastName" => $lastName[$i],
				    "passportNo"       => $passportNo[$i],
				    "issueDate"  => $issueDate[$i],
				    "placeOfIssue"       => $placeOfIssue[$i],
				    "expireDate"       => $expireDate[$i],
				    "userUniqueId"       => $userUniqueId,
				    "country"       => $country[$i],
				    "employeeId" => $employeeId,
				    "businessUniqueId" => $data['businessUniqueId'],
				    "businessId" => $data['businessId'],
				    "employeeFamilyId" =>  $employeeFamilyId[$i]?$employeeFamilyId[$i]:'0',
				    "attachment1" => $attachment1,
				    "attachment2" => $attachment2,
				    "attachment3" => $attachment3,
				    "familyType" => $employeeFamilyId[$i]!=0?$this->mainModel->getFamilyTypeById($employeeFamilyId[$i]):'Self'
				];
				
			}
		}
		
		$update = $this->mainModel->updatePassportDetails($userUniqueId,$fData);

		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('visaDetails'), 'Location');	
	}

	public function visaDetailsSubmit(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];		
		$employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
		//echo "<pre>"; print_r($this->input->post()); echo "</pre>"; die();
		for($j = 0; $j <= count($this->input->post('visaType')); $j++) {				
			$visaType = $this->input->post('visaType');
			$visaNumber = $this->input->post('visaNumber');
			$issueDate = $this->input->post('vissueDate');
			$placeOfIssue = $this->input->post('vplaceOfIssue');
			$expireDate = $this->input->post('vexpireDate');
			$country = $this->input->post('vcountry');
			$employeeFamilyId = $this->input->post('vemployeeFamilyId'); 
			$values = array($visaType[$j], $visaNumber[$j], $issueDate[$j], $placeOfIssue[$j], $expireDate[$j], $country[$j], $employeeFamilyId[$j]);	
			$visaUniqueId = $this->input->post('visaUniqueId');	

			$attachment11 = $this->input->post('attachment11');	
			$attachment22 = $this->input->post('attachment22');	
			$attachment33 = $this->input->post('attachment33');	

			//if(!empty($firstName[$j]) && $firstName[$j]>0 &&(count(array_unique($values))!=1))	
			if((count(array_unique($values))!=1))
			{	 
				$attachment1 = '';
				 $attachment2 = '';
				 $attachment3 = '';
				 $time = time();
				 $target_dir = "assets/visaDocuments/";
				if($_FILES["attachment1"]["name"][$j]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$j]);
					$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$j]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$j], $target_file);
				}else{
					$attachment1 = $attachment11[$j];
				}
				 
				if($_FILES["attachment2"]["name"][$j]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment2"]["name"][$j]);
					$attachment2 = $time."_".basename($_FILES["attachment2"]["name"][$j]);
					move_uploaded_file($_FILES["attachment2"]["tmp_name"][$j], $target_file);
				}else{
					$attachment2 = $attachment22[$j];
				}

				if($_FILES["attachment3"]["name"][$j]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment3"]["name"][$j]);
					$attachment3 = $time."_".basename($_FILES["attachment3"]["name"][$j]);
					move_uploaded_file($_FILES["attachment3"]["tmp_name"][$j], $target_file);
				}else{
					$attachment3 = $attachment33[$j];
				}

				$fData1[] = [
					"visaUniqueId" => ( isset( $visaUniqueId[ $j ] ) ? $visaUniqueId[ $j ] : '' ),
					"visaType" => $visaType[$j],
					"visaNumber" => $visaNumber[$j],
					"employeeFamilyId" => $employeeFamilyId[$j]?$employeeFamilyId[$j]:'0',
				    "issueDate"  => $issueDate[$j],
				    "placeOfIssue"       => $placeOfIssue[$j],
				    "expireDate"       => $expireDate[$j],
				    "userUniqueId"       => $userUniqueId,
				    "country"       => $country[$j],
				    "employeeId" => $employeeId,
				    "businessUniqueId" => $data['businessUniqueId'],
				    "businessId" => $data['businessId'],
				    "attachment1" => $attachment1,
				    "attachment2" => $attachment2,
				    "attachment3" => $attachment3,
				    "familyType" => $employeeFamilyId[$j]!=0?$this->mainModel->getFamilyTypeById($employeeFamilyId[$j]):'Self'
				];
				
			}
		}
		echo "<pre>";
		//print_r($fData1); echo "</pre>";  die();	
		$update = $this->mainModel->updateVisaDetails($userUniqueId,$fData1);

		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('visaDetails'), 'Location');	
	}

	public function educationDetailsSubmit(){
		$data = $this->global_functions();echo "<pre>";
		print_r($this->input->post()); echo "</pre>"; 
		$userUniqueId = $data['loginUniqueId'];
		for($i = 0; $i <= count($this->input->post('qualificationTitle')); $i++) {				
			$qualificationTitle = $this->input->post('qualificationTitle');
			$institution = $this->input->post('institution');
			$startYear = $this->input->post('startYear'); 
			$endYear = $this->input->post('endYear');
			$specialization = $this->input->post('specialization');
			$values = array($qualificationTitle[$i], $institution[$i], $startYear[$i], $endYear[$i],$specialization[$i]);		
			$educationId = $this->input->post('educationId');		
			$attachment11 = $this->input->post('attachment11');	
			echo "d===".$qualificationTitle[$i]; 
			if(!empty($qualificationTitle[$i]) &&(count(array_unique($values))!=1))
			{	
                $file_name = '';
				if($_FILES["attachment1"]["name"][$i]!='') {
					$time = time();
					$target_dir = "assets/educationDocuments/";
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
					$file_name = $time."_".basename($_FILES["attachment1"]["name"][$i]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
				}else{
					$attachment1 = $attachment11[$i];
				} 

				$cData[] = [
					"educationId" => ( isset( $educationId[ $i ] ) ? $educationId[ $i ] : '' ),
					"qualificationTitle" => $qualificationTitle[$i],
					"institution" => $institution[$i],
				    "startYear"       => $startYear[$i],
				    "endYear"  => $endYear[$i],
				    "specialization"  => $specialization[$i],
				    "userUniqueId"       => $userUniqueId,
				    "attachment1" =>$file_name
				];
			}
		}
	

		$update = $this->mainModel->updateEducationDetails($userUniqueId,$cData);
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('educationDetails'), 'Location');
	}

	public function employmentDetailsSubmit(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];
		
		for($i = 0; $i <= count($this->input->post('companyName')); $i++) {				
			$companyName = $this->input->post('companyName');
			$address = $this->input->post('address');
			$fromDate = $this->input->post('fromDate');
			$toDate = $this->input->post('toDate');
			$role = $this->input->post('role');
			$technology = $this->input->post('technology');
			$values = array($companyName[$i], $address[$i], $fromDate[$i], $toDate[$i], $role[$i], $technology[$i]);	
			  
			$employmentId = $this->input->post('employmentId');		
				$attachment11 = $this->input->post('attachment11');	
			$attachment22 = $this->input->post('attachment22');	
			$attachment33 = $this->input->post('attachment33');
			if(!empty($companyName[$i])  &&(count(array_unique($values))!=1))
			{	 
				 $attachment1 = '';
				 $attachment2 = '';
				 $attachment3 = '';
				 $time = time();
				 $target_dir = "assets/employmentDocuments/";
				if($_FILES["attachment1"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment1"]["name"][$i]);
					$attachment1 = $time."_".basename($_FILES["attachment1"]["name"][$i]);
					move_uploaded_file($_FILES["attachment1"]["tmp_name"][$i], $target_file);
				}else{
					$attachment1 = $attachment11[$i];
				}
				 
				if($_FILES["attachment2"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment2"]["name"][$i]);
					$attachment2 = $time."_".basename($_FILES["attachment2"]["name"][$i]);
					move_uploaded_file($_FILES["attachment2"]["tmp_name"][$i], $target_file);
				}else{
					$attachment2 = $attachment22[$i];
				} 

				if($_FILES["attachment3"]["name"][$i]!='') {
					$target_file = $target_dir . $time."_".basename($_FILES["attachment3"]["name"][$i]);
					$attachment3 = $time."_".basename($_FILES["attachment3"]["name"][$i]);
					move_uploaded_file($_FILES["attachment3"]["tmp_name"][$i], $target_file);
				}else{
					$attachment3 = $attachment33[$i];
				} 

				$cData[] = [
					"employmentId" => ( isset( $employmentId[ $i ] ) ? $employmentId[ $i ] : '' ),
					"companyName" => $companyName[$i],
					"address" => $address[$i],
				    "fromDate"       => $fromDate[$i],
				    "toDate"  => $toDate[$i],
				    "role"  => $role[$i],
				    "technology"  => $technology[$i],
				    "userUniqueId"       => $userUniqueId,
				    "attachment1" => $attachment1,
				    "attachment2" => $attachment2,
				    "attachment3" => $attachment3
				];


			}
		}

		 
		$update = $this->mainModel->updateEmploymentDetails($userUniqueId,$cData);
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect(base_url('employmentDetails'), 'Location');
	}

	public function deleteChildren() {
		$data = $this->global_functions();
		$childrenId = $this->input->get('childrenId');
		$delete = $this->mainModel->deleteChildren($childrenId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteFamily() {
		$data = $this->global_functions();
		$employeeFamilyId = $this->input->get('employeeFamilyId');
		$delete = $this->mainModel->deleteFamily($employeeFamilyId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteEducation() {
		$data = $this->global_functions();
		$educationId = $this->input->get('educationId');
		$delete = $this->mainModel->deleteEducation($educationId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deletePassport() {
		$data = $this->global_functions();
		$passportUniqueId = $this->input->get('passportUniqueId');
		$delete = $this->mainModel->deletePassport($passportUniqueId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteUserVisa() {
		$data = $this->global_functions();
		$visaUniqueId = $this->input->get('visaUniqueId');
		$delete = $this->mainModel->deleteUserVisa($visaUniqueId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function deleteEmployment() {
		$data = $this->global_functions();
		$employmentId = $this->input->get('employmentId');
		$delete = $this->mainModel->deleteEmployment($employmentId,$data['loginUniqueId']);
		if($delete==1) {
			$this->session->set_flashdata('message', 'deleted successfully');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		} else {
			$this->session->set_flashdata('message', 'deletion Failed');
			redirect($_SERVER['HTTP_REFERER'], 'Location');
		}
	}

	public function updateProfile(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/update-profile',$data);
		$this->load->view('templates/footer');
	}

	public function updateBusinessProfile(){
		$data = $this->global_functions();
        $data['businessInfo'] = $this->mainModel->getBusinessInfo($data['loginUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('business/updateBusinessprofile',$data);
		$this->load->view('templates/footer');
	}

	public function profileSubmit() {
		$data = $this->global_functions();
		if($_FILES["profilePic"]["name"]!='') {
			$target_dir = "assets/profilePics/";
			$target_file = $target_dir . basename($_FILES["profilePic"]["name"]);
			$file_name = basename($_FILES["profilePic"]["name"]);
			move_uploaded_file($_FILES["profilePic"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['profilePic'];
		}
		$form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' => $this->input->post('lastName'),
			'address' => $this->input->post('address'),
			'gender' => $this->input->post('gender'),
			'profilePic' => $file_name,
			'ssnNumber' => $this->input->post('ssnNumber'),
			'passportNumber' => $this->input->post('passportNumber'),
			'dob' => $this->input->post('dob')
		);
		
		$update = $this->mainModel->updateProfileDetails($form_data,$data['loginUniqueId']);
		$this->session->set_flashdata('message', 'Profile updated successfully');		
		redirect(base_url('updateProfile'), 'Location');
	}

	public function createBusiness(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('business/createBusiness',$data);
		$this->load->view('templates/footer');
	}

	public function saveBusiness(){
    	$response = array();
    	if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = '';
		}

        $usertype = "2";
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->mainModel->generateBusinessID();
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
            'userTypeCode' =>BUSINESS,
            'userType' =>'Business',
			'businessId' =>  $businessId,
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'bankAccountNumber' =>$this->input->post('bankAccountNumber'),
			'bankName' =>$this->input->post('bankName'),
			'routingNumber' =>$this->input->post('routingNumber'),
			'dob' => $this->input->post('dob'),
			'businessLogo' => $file_name
		);
		 
		$check = $this->mainModel->checkBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertBusines = $this->mainModel->insertBusiness($form_data);
			
			if($insertBusines>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Business created successfully.';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}			
		}else{
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function businessList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['businessData'] = $this->mainModel->getBusinessList();	
		$this->load->view('templates/header',$data);
		$this->load->view('business/businessList',$data);
		$this->load->view('templates/footer');
	} 

	public function deleteBusiness(){
        $userid = $this->input->get('businessUniqueId');
        $delete = $this->mainModel->deleteBusiness($userid);
        if($delete){
            $this->session->set_flashdata('message', 'Business deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Business delete Failed');
        }
        redirect(base_url('businessList'), 'Location');
    }

    public function deleteDocument(){
    	$data = $this->global_functions();
        $userDocumentId = $this->input->get('userDocumentId');
        $documentKey = $this->input->get('documentKey');
        $delete = $this->mainModel->deleteDocument($userDocumentId,$documentKey);
        if($delete){
            $this->session->set_flashdata('message', 'Document deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Document delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    public function editBusiness(){
		$data = $this->global_functions();	
		$data['businessUniqueId'] = $this->input->get('businessUniqueId');
        $data['businessInfo'] = $this->mainModel->getBusinessInfo($data['businessUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('business/editBusiness',$data);
		$this->load->view('templates/footer');
	}

	public function updateBusiness(){
    	$response = array();

    	if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = $this->input->post('exbusinessLogo');
		}
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $this->mainModel->generateBusinessID();
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,			
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'bankAccountNumber' =>$this->input->post('bankAccountNumber'),
			'bankName' =>$this->input->post('bankName'),
			'routingNumber' =>$this->input->post('routingNumber'),
			'dob' => $this->input->post('dob'),
			'businessLogo' => $file_name			
		);
		$businessUniqueId = $this->input->post('businessUniqueId');
		$check = $this->mainModel->checkExistBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'),$businessUniqueId);
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$insertBusines = $this->mainModel->updateBusiness($form_data,$businessUniqueId);			
			if($insertBusines>0) {
				$response['message'] = 'Business updated successfully.';
				$response['status'] = true;
                $response['businessUniqueId'] = $insertBusines;
                $response['businessId'] = $businessId;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function submitBusinessProfile(){
		$data = $this->global_functions();
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');

        if($_FILES["businessLogo"]["name"]!='') {
			$target_dir = "assets/businessLogos/";
			$target_file = $target_dir . basename($_FILES["businessLogo"]["name"]);
			$file_name = basename($_FILES["businessLogo"]["name"]);
			move_uploaded_file($_FILES["businessLogo"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['businessLogo'];
		}

        $form_data = array(
			'businessName' => $this->input->post('businessName'),
			'businessStartDate' => $this->input->post('businessStartDate'),
			'businessEndDate' =>  $this->input->post('businessEndDate'),
			'ownerFirstName' =>  $this->input->post('ownerFirstName'),
			'ownerLastName' => $this->input->post('ownerLastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			//'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'bankAccountNumber' =>$this->input->post('bankAccountNumber'),
			'bankName' =>$this->input->post('bankName'),
			'routingNumber' =>$this->input->post('routingNumber'),
			'gender' => $this->input->post('gender'),
			'businessUrl' => $this->input->post('businessUrl'),
			'businessStatus' => $this->input->post('businessStatus'),
			'businessLogo' =>	$file_name, 
			'dob' => $this->input->post('dob')	
		);
		$businessUniqueId = $data['loginUniqueId'];
		$check = $this->mainModel->checkExistBusinessUserAvailable($this->input->post('mobile'),$this->input->post('email'),$businessUniqueId);
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$insertBusines = $this->mainModel->updateBusiness($form_data,$businessUniqueId);			
			if($insertBusines>0) {
				$this->session->set_flashdata('message', 'Profile updated successfully');
			}else{
				$this->session->set_flashdata('message', 'Server Error');
			}			
		}else if($check=='1'){
			$this->session->set_flashdata('message', 'Mobile Already Exist');
		}else if($check=='2'){
			$this->session->set_flashdata('message', 'Email Already Exist');
		}else if($check=='3'){
			$this->session->set_flashdata('message', 'Mobile/Email Already Exist');
		}	
       redirect(base_url('updateBusinessprofile'), 'Location');
	}

	public function createEmployee(){
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'employeeList';	
		$data['createUserTypeCode'] = EMPLOYEE;
		$data['createUserType'] = "Employee";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveEmployee(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $createUserType = $this->input->post('createUserType');
        $form_data = array(
            'userTypeCode' => $this->input->post('createUserTypeCode'),
            'userType' =>$this->input->post('createUserType'),
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber'),
			'dob' => $this->input->post('dob'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			     $this->uploadDocuments($insertUser);
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		    
				$response['message'] = 'Employee created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'employeeList';
                $response['userUniqueId'] = $insertUser;
                $this->session->set_flashdata('message', "$createUserType added successsfully.");
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function employeeList(){
		$data = $this->global_functions();	
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createEmployee';
		$data['editUrl'] = 'editEmployee';
		$data['deleteUrl'] = 'deleteEmployee';
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function timesheets(){
		$data = $this->global_functions();	
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createEmployee';
		$data['editUrl'] = 'editEmployee';
		$data['deleteUrl'] = 'deleteEmployee';
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/timesheets',$data);
		$this->load->view('templates/footer');
	} 

	public function approvalTimesshetsInfo(){
		$data = $this->global_functions();	
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createEmployee';
		$data['editUrl'] = 'editEmployee';
		$data['deleteUrl'] = 'deleteEmployee';
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/approvalTimesshetsInfo',$data);
		$this->load->view('templates/footer');
	}

	public function editEmployee(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Employee';
		$data['cancelUrl'] = 'employeeList';
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateEmployee(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $createUserType = $this->input->post('createUserType');
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber'),
			'dob' => $this->input->post('dob'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'userStatus' => $this->input->post('userStatus')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		if($check=='0') {

			$this->uploadDocuments($userUniqueId);
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = "$createUserType updated successfully.";
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteEmployee(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Employee deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Employee delete Failed');
        }
        redirect(base_url('employeeList'), 'Location');
    }

    //Project
    public function createProject(){
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'projectList';	
		$data['createUserTypeCode'] = EMPLOYEE;
		$data['createUserType'] = "Project";
		$data['vendorData'] = $this->mainModel->getVendors($data['businessUniqueId']);
		$data['clientData'] = $this->mainModel->getClients($data['businessUniqueId']);
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
		$this->load->view('templates/header',$data);
		$this->load->view('project/createProject',$data);
		$this->load->view('templates/footer');
	}

	public function ProjectList(){
		$data = $this->global_functions();	
		$data['projectData'] = $this->mainModel->getProjects($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('project/projectList',$data);
		$this->load->view('templates/footer');
	} 

	public function saveProject(){
    	$response = array();
    	$data = $this->global_functions();	
             
        $projectId = $this->mainModel->generateProjectID($data['businessUniqueId']);
        $form_data = array(
            'projectId' => $projectId,
            'projectName' =>$this->input->post('projectName'),
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'vendorUniqueId' =>  $this->input->post('vendorUniqueId'),
			'vendorId' =>  $this->mainModel->getVendorId($this->input->post('vendorUniqueId')),
			'clientUniqueId' => $this->input->post('clientUniqueId'),
			'clientId' =>  $this->mainModel->getClientId($this->input->post('clientUniqueId')),
			'employeeId' => $this->mainModel->getEmployeeId($this->input->post('employeeUniqueId')),
			'userUniqueId' => $this->input->post('employeeUniqueId'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'projectStatus' => $this->input->post('projectStatus'),
			'billRate' => $this->input->post('billRate'),
			'netDays' => $this->input->post('netDays'),
			'description' => $this->input->post('description')
		);
		 
		$check = $this->mainModel->checkProjectAvailable($this->input->post('projectName'));
		if($check=='0') {			
			$insertUser = $this->mainModel->insertProject($form_data);
			
			if($insertUser>0) {
			     $this->uploadProjectDocuments($insertUser);

			   /* $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();*/
    		    
				$response['message'] = 'Project created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'projectList';
                $response['projectUniqueId'] = $insertUser;
                $this->session->set_flashdata('message', "Project added successsfully.");
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Project Name Already Exist';
		}
       echo json_encode($response);
	}

	public function updateProject(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $createUserType = $this->input->post('createUserType');
        $form_data = array(			
            'projectName' =>$this->input->post('projectName'),
			'vendorUniqueId' =>  $this->input->post('vendorUniqueId'),
			'vendorId' =>  $this->mainModel->getVendorId($this->input->post('vendorUniqueId')),
			'clientUniqueId' => $this->input->post('clientUniqueId'),
			'clientId' =>  $this->mainModel->getClientId($this->input->post('clientUniqueId')),
			'employeeId' => $this->mainModel->getEmployeeId($this->input->post('employeeUniqueId')),
			'userUniqueId' => $this->input->post('employeeUniqueId'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'billRate' => $this->input->post('billRate'),
			'netDays' => $this->input->post('netDays'),
			'description' => $this->input->post('description')
		);
		$projectUniqueId = $this->input->post('projectUniqueId');
		$check = $this->mainModel->checkExistProjectAvailable($this->input->post('projectName'),$projectUniqueId);
		if($check=='0') {

			$this->uploadProjectDocuments($projectUniqueId);
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateProject($form_data,$projectUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = "Project updated successfully.";
				$response['status'] = true;
                $response['projectUniqueId'] = $updateUser;
			}else{
				$response['message'] = "Project updated successfully.";
				$response['status'] = true;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Project Name Already Exist';
		}
       echo json_encode($response);
	}

	public function projectStatusUpdate(){
    	$status= $this->input->post('projectStatus');
    	$projectUniqueId= $this->input->post('projectUniqueId');
        $response = array();       
        $response['status'] = true;
        $response['message'] = "Status updated.";
	    $form_data = array(
			'projectStatus' =>$status
		    );
	    $this->mainModel->projectStatusUpdate($form_data,$projectUniqueId);
        echo json_encode($response);
    }

    public function editProject(){
		$data = $this->global_functions();	
		$data['vendorData'] = $this->mainModel->getVendors($data['businessUniqueId']);
		$data['clientData'] = $this->mainModel->getClients($data['businessUniqueId']);
		$data['employeeData'] = $this->mainModel->getUsers(EMPLOYEE,$data['businessUniqueId']);
		$data['projectUniqueId'] = $this->input->get('projectUniqueId');
        $data['projectInfo'] = $this->mainModel->getProjectInfo($data['projectUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('project/editProject',$data);
		$this->load->view('templates/footer');
	}

	 public function deleteProjectDocument(){
    	$data = $this->global_functions();
        $projectDocumentId = $this->input->get('projectDocumentId');
        $documentKey = $this->input->get('documentKey');
        $delete = $this->mainModel->deleteProjectDocument($projectDocumentId,$documentKey);
        if($delete){
            $this->session->set_flashdata('message', 'Document deleted successfully');
        } else {
           $this->session->set_flashdata('message1', 'Document delete Failed');
        }
        redirect($_SERVER['HTTP_REFERER'], 'Location');
    }

    public function deleteProject(){
        $projectUniqueId = $this->input->get('projectUniqueId');
        $delete = $this->mainModel->deleteProject($projectUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Project deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Project delete Failed');
        }
        redirect(base_url('projectList'), 'Location');
    }

    //HR
    public function createHr(){ 
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'hrList';	
		$data['createUserTypeCode'] = HR;
		$data['createUserType'] = "HR";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveHr(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>HR,
            'userType' =>'HR',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'ssnNumber' => $this->input->post('ssnNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'HR created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'hrList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function hrList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'HR';
		$data['createurl'] = 'createHr';
		$data['editUrl'] = 'editHr';
		$data['deleteUrl'] = 'deleteHr';
		$data['employeeData'] = $this->mainModel->getUsers(HR,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editHr(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'HR';
		$data['cancelUrl'] = 'hrList';
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateHr(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'HR updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteHr(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'HR deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'HR delete Failed');
        }
        redirect(base_url('hrList'), 'Location');
    }

    //Accountant

    public function createAccountant(){
		$data = $this->global_functions();
		$data['cancelUrl'] = 'accountantList';	
		$data['createUserTypeCode'] = ACCOUNTANT;
		$data['createUserType'] = "Accountant";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveAccountant(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>ACCOUNTANT,
            'userType' =>'Accountant',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			//$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {				
			    $this->uploadDocuments($insertUser);
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Accountant created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'accountantList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function uploadDocuments($userUniqueId){
		for($i = 0; $i <= count($this->input->post('documentType')); $i++) {				
			$documentType = $this->input->post('documentType');
			$attachment = $this->input->post('attachment');
			$values = array($documentType[$i], $attachment[$i]);			
			if(!empty($documentType[$i]) && $documentType[$i]>0 &&(count(array_unique($values))!=1))
			{	
				if($_FILES["attachment"]["name"]!='') {
					$target_dir = "assets/documents/";
					$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
					$file_name = basename($_FILES["attachment"]["name"][$i]);
					move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
				} 

				$documentsData = array(
					"documentKey" => $this->mainModel->generateEmailKey(),
					"documentTypeId" => $documentType[$i],
					"documentType" => $this->mainModel->getTypeName($documentType[$i],'DOCUMENT_TYPE'),
					"attachment" => $file_name,
				    "userUniqueId" => $userUniqueId,
				);
				$this->mainModel->userDocuments($documentsData);
			}
		}
	}

	public function uploadProjectDocuments($projectUniqueId){
		for($i = 0; $i <= count($this->input->post('docName')); $i++) {				
			$documentType = $this->input->post('docName');
			$attachment = $this->input->post('attachment');
			$values = array($documentType[$i], $attachment[$i]);			
			if(!empty($documentType[$i]) && $documentType[$i]>0 &&(count(array_unique($values))!=1))
			{	
				if($_FILES["attachment"]["name"]!='') {
					$target_dir = "assets/projectDocuments/";
					$target_file = $target_dir . basename($_FILES["attachment"]["name"][$i]);
					$file_name = basename($_FILES["attachment"]["name"][$i]);
					move_uploaded_file($_FILES["attachment"]["tmp_name"][$i], $target_file);
				} 

				$documentsData = array(
					"documentKey" => $this->mainModel->generateEmailKey(),
					"documentName" => $documentType[$i],
					"attachment" => $file_name,
				    "projectUniqueId" => $projectUniqueId,
				);
				$this->mainModel->insertProjectDocuments($documentsData);
			}
		}
	}

	public function accountantList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'Accountant';
		$data['createurl'] = 'createAccountant';
		$data['editUrl'] = 'editAccountant';
		$data['deleteUrl'] = 'deleteAccountant';
		$data['createUrl'] = 'createAccountant';
		$data['employeeData'] = $this->mainModel->getUsers(ACCOUNTANT,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editAccountant(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Accountant';
		$data['cancelUrl'] = 'accountantList';
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateAccountant(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Accountant updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteAccountant(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Accountant deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Accountant delete Failed');
        }
        redirect(base_url('accountantList'), 'Location');
    }

    //Visa
    public function createVisa(){
		$data = $this->global_functions();		
		$data['cancelUrl'] = 'visaList';	
		$data['createUserTypeCode'] = VISA;
		$data['createUserType'] = "VISA";
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveVisa(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>VISA,
            'userType' =>'Visa',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Visa created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'visaList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function visaList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'Visa';
		$data['createurl'] = 'createVisa';
		$data['editUrl'] = 'editVisa';
		$data['deleteUrl'] = 'deleteVisa';
		$data['employeeData'] = $this->mainModel->getUsers(VISA,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editVisa(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Visa';
		$data['cancelUrl'] = 'visaList';
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateVisa(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Visa updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteVisa(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Visa deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Visa delete Failed');
        }
        redirect(base_url('visaList'), 'Location');
    }

    //Bench
    public function createBench(){
		$data = $this->global_functions();	
		$data['cancelUrl'] = 'benchList';	
		$data['createUserTypeCode'] = BENCH;
		$data['createUserType'] = "Bench";	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/createEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function saveBench(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $generatePw = $this->mainModel->generatePassword();
        $employeeId = $this->mainModel->generateEmployeeID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>BENCH,
            'userType' =>'Bench',
            'employeeId' => $employeeId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')
		);
		 
		$check = $this->mainModel->checkEmployeeAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertUser($form_data);
			
			if($insertUser>0) {
			    
			    $config = Array(
    				'protocol' => 'sendmail',
    				'mailpath' => '/usr/sbin/sendmail',
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE
    			);
    		
        		$query = $this->db->get_where('tbl_communications', array('commid' => '1'), 1);
        		$row = $query->result_array();
        		$email_message = $row[0]['message'];
        		$subject =  $row[0]['subject'];
        		$finalmessage = str_replace('EMAIL',$email,$email_message);
        		$finalmessage = str_replace('MOBILE',$mobile,$finalmessage);
        		$finalmessage = str_replace('PASSWORD',$generatePw,$finalmessage);
        		
        		//$list = array($email, 'info@efileservices.in');
        		$this->load->library('email',$config);
        		$this->email->set_newline("\r\n");
        		$this->email->from(FROM_EMAIL,'Aster IT');
        		$this->email->to($email);
        		$this->email->subject($subject);
        		$this->email->message($finalmessage);
        		$this->email->send();
    		
				$response['message'] = 'Bench created successfully.';
				$response['status'] = true;
				$response['rurl'] = 'benchList';
                $response['userUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function benchList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['labelName'] = 'Bench';
		$data['createurl'] = 'createBench';
		$data['editUrl'] = 'editBench';
		$data['deleteUrl'] = 'deleteBench';
		$data['employeeData'] = $this->mainModel->getUsers(BENCH,$data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('employee/employeeList',$data);
		$this->load->view('templates/footer');
	} 

	public function editBench(){
		$data = $this->global_functions();	
		$data['userUniqueId'] = $this->input->get('userUniqueId');
		$data['labelName'] = 'Bench';
		$data['cancelUrl'] = 'benchList';
        $data['userInfo'] = $this->mainModel->getUserInfo($data['userUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('employee/editEmployee',$data);
		$this->load->view('templates/footer');
	}

	public function updateBench(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' =>  $this->input->post('lastName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($generatePw),
			'address' => $this->input->post('address'),
			'panNumber' => $this->input->post('panNumber'),
			'test_password' =>$generatePw,
			'gender' => $this->input->post('gender'),
			'passportNumber' => $this->input->post('passportNumber')		
		);
		$userUniqueId = $this->input->post('userUniqueId');
		$check = $this->mainModel->checkExistUserAvailable($this->input->post('mobile'),$this->input->post('email'),$userUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateUser($form_data,$userUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Bench updated successfully.';
				$response['status'] = true;
                $response['userUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteBench(){
        $userUniqueId = $this->input->get('userUniqueId');
        $delete = $this->mainModel->deleteUser($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Bench deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Bench delete Failed');
        }
        redirect(base_url('visaList'), 'Location');
    }

    //Vendor
     public function createVendor(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/createVendor',$data);
		$this->load->view('templates/footer');
	}

	public function saveVendor(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $vendorId = $this->mainModel->generateVendorID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>VENDOR,
            'userType' =>'Vendor',
            'vendorId' => $vendorId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'vendorFirm' => $this->input->post('vendorFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'vendorStatus' => $this->input->post('vendorStatus')
		);
		 
		$check = $this->mainModel->checkVendorAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertVendor($form_data);
			
			if($insertUser>0) {    		
				$this->session->set_flashdata('message', 'Vendor created successfully.');
				$response['message'] = 'Vendor created successfully.';
				$response['status'] = true;
                $response['vendorUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function vendorList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getVendors($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/vendorList',$data);
		$this->load->view('templates/footer');
	} 

	public function editVendor(){
		$data = $this->global_functions();	
		$data['vendorUniqueId'] = $this->input->get('vendorUniqueId');
        $data['vendorInfo'] = $this->mainModel->getVendorInfo($data['vendorUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('vendor/editVendor',$data);
		$this->load->view('templates/footer');
	}

	public function updateVendor(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'vendorFirm' => $this->input->post('vendorFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'vendorStatus' => $this->input->post('vendorStatus')		
		);
		$vendorUniqueId = $this->input->post('vendorUniqueId');
		$check = $this->mainModel->checkExistVendorAvailable($this->input->post('mobile'),$this->input->post('email'),$vendorUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateVendor($form_data,$vendorUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Vendor updated successfully.';
				$response['status'] = true;
                $response['vendorUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteVendor(){
        $userUniqueId = $this->input->get('vendorUniqueId');
        $delete = $this->mainModel->deleteVendor($userUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Vendor deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Vendor delete Failed');
        }
        redirect(base_url('vendorList'), 'Location');
    }

    //Clients
     public function createClient(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('client/createClient',$data);
		$this->load->view('templates/footer');
	}

	public function saveClient(){
    	$response = array();
    	$data = $this->global_functions();	
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $businessId = $data['businessId'];
        $clientrId = $this->mainModel->generateClientID($data['businessUniqueId']);
        $form_data = array(
            'userTypeCode' =>CLIENT,
            'userType' =>'Client',
            'clientId' => $clientrId,
			'businessId' =>  $data['businessId'],
			'businessUniqueId' => $data['businessUniqueId'],
			'clientFirm' => $this->input->post('clientFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'clientStatus' => $this->input->post('clientStatus')
		);
		 
		$check = $this->mainModel->checkClientAvailable($this->input->post('mobile'),$this->input->post('email'));
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
		//	$result = $this->servModel->sendsms($this->input->post('mobile'),$otp);
			$insertUser = $this->mainModel->insertClient($form_data);
			
			if($insertUser>0) {    		
				$this->session->set_flashdata('message', 'Client created successfully.');
				$response['message'] = 'Client created successfully.';
				$response['status'] = true;
                $response['clinetUniqueId'] = $insertUser;
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}
       echo json_encode($response);
	}

	public function clientList(){
		$data = $this->global_functions();	
		$data['token'] = $this->security->get_csrf_hash();
		$data['employeeData'] = $this->mainModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('client/clientList',$data);
		$this->load->view('templates/footer');
	} 

	public function editClient(){
		$data = $this->global_functions();	
		$data['clientUniqueId'] = $this->input->get('clientUniqueId');
        $data['clientInfo'] = $this->mainModel->getClientInfo($data['clientUniqueId']);		 
		$this->load->view('templates/header',$data);
		$this->load->view('client/editClient',$data);
		$this->load->view('templates/footer');
	}

	public function updateClient(){
    	$response = array();
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $generatePw = $this->mainModel->generatePassword();
        $form_data = array(
			'clientFirm' => $this->input->post('clientFirm'),
			'contactName' =>  $this->input->post('contactName'),
			'email' =>  $email,
			'mobile' =>  $mobile,
			'address' => $this->input->post('address'),
			'description' => $this->input->post('description'),
			'gender' => $this->input->post('gender'),
			'clientStatus' => $this->input->post('clientStatus')	 	
		);
		$clientUniqueId = $this->input->post('clientUniqueId');
		$check = $this->mainModel->checkExistClientAvailable($this->input->post('mobile'),$this->input->post('email'),$clientUniqueId);
		
		if($check=='0') {
			$otp = $this->mainModel->generateSMSKey();
			$updateUser = $this->mainModel->updateClient($form_data,$clientUniqueId);			
			if($updateUser > 0 ) {
				$response['message'] = 'Client updated successfully.';
				$response['status'] = true;
                $response['clientUniqueId'] = $updateUser;
			}else{
				$response['status'] = false;
				$response['message'] = 'Server error';
			}			
		}else if($check=='1'){
			$response['status'] = false;
			$response['message'] = 'Mobile Already Exist';
		}else if($check=='2'){
			$response['status'] = false;
			$response['message'] = 'Email Already Exist';
		}else if($check=='3'){
			$response['status'] = false;
			$response['message'] = 'Mobile/Email Already Exist';
		}	
       echo json_encode($response);
	}

	public function deleteClient(){
        $clientUniqueId = $this->input->get('clientUniqueId');
        $delete = $this->mainModel->deleteClient($clientUniqueId);
        if($delete){
            $this->session->set_flashdata('message', 'Client deleted successfully');
        } else {
           $this->session->set_flashdata('message', 'Client delete Failed');
        }
        redirect(base_url('clientList'), 'Location');
    }

    public function getUserDocuments(){
		if(isset($_POST["userUniqueId"]))   
	 	{  
 		$data = $this->global_functions();	
		$userUniqueId = $this->input->post('userUniqueId');
        $docInfo = $this->mainModel->getUserDocuments($userUniqueId);
	      $output = '';
	      $output .= '  
	      <div class="table-responsive">  
	           <table class="table table-bordered"><tr><th>Document Type</th><th>Attachment</th></tr>';  
	      foreach($docInfo as $docInfos)  
	      {   $path = base_url()."assets/documents/".$docInfos["attachment"];
	           $output .= '  
	                <tr> <td >'.$docInfos["documentType"].'</td>   
	                     <td><a href="'.$path.'" download><i class="fa fa-download" aria-hidden="true"></i></a></td>  
	                </tr> 
	                ';  
	      }  
	      $output .= "</table></div>";  
	      echo $output;  
 		}
    }

    public function addTimesheet(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->mainModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/addTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function editTimesheet(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->mainModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/editTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function checkTimesheet(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->mainModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/checkTimesheet',$data);
		$this->load->view('templates/footer');
    }

    public function addWeeklyReview(){
		$data = $this->global_functions();
		$data['employeeData'] = $this->mainModel->getClients($data['businessUniqueId']);	
		$this->load->view('templates/header',$data);
		$this->load->view('timesheet/addWeeklyReview',$data);
		$this->load->view('templates/footer');
    } 

    public function saveWeeklyReview(){
    	$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];
		$description = $this->input->post('description');
		$weekDate = $this->input->post('weekDate');
		$projectUniqueId = $this->input->post('projectUniqueId');
		$check = $this->mainModel->checkWeeklyReviewByDate($userUniqueId,$weekDate);
		if($check>0){
			$form_data = array(
				'description' => $description,
				'projectUniqueId' =>$projectUniqueId,
				'supervisor' =>  $this->input->post('supervisor')
			);
			$update = $this->mainModel->updateWeeklyReview($userUniqueId,$weekDate,$form_data);
		}else{
			$form_data = array(
				'userUniqueId' => $data['loginUniqueId'],
				'description' => $description,
				'weekDate' => $weekDate,
				'projectUniqueId' =>$projectUniqueId,
				'supervisor' =>  $this->input->post('supervisor')
			);
			$this->db->insert('weekly_review', $form_data);
		}
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect($_SERVER['HTTP_REFERER'], 'Location');
    }
        
    public function saveTimesheet(){
		$data = $this->global_functions();
		$userUniqueId = $data['loginUniqueId'];
		for($i = 0; $i <= count($this->input->post('workDate')); $i++) {				
			$workDate = $this->input->post('workDate');
			$standardHours = $this->input->post('standardHours');
			$extraHours = $this->input->post('extraHours');
			$timesheetId = $this->input->post('timesheetId');
			$projectUniqueId = $this->input->post('projectUniqueId');
			$values = array($workDate[$i], $standardHours[$i]);	

			if(!empty($workDate[$i]) && $workDate[$i]>0 &&(count(array_unique($values))!=1))
			{
				$cData[] = [
					"timesheetId" => ( isset( $timesheetId[ $i ] ) ? $timesheetId[ $i ] : '' ),
					"workDate" => $workDate[$i],
					"standardHours" => $standardHours[$i],
				    "extraHours"       => $extraHours[$i],
				    "userUniqueId" => $userUniqueId,
				    "projectUniqueId" =>$projectUniqueId
				];
			}
		}

		$update = $this->mainModel->saveTimesheet($userUniqueId,$cData);

		if($_FILES["attachment1"]["name"]!='') {
			$target_dir = "assets/timesheetAttachments/";
			$target_file = $target_dir . basename($_FILES["attachment1"]["name"]);
			$attachment1 = basename($_FILES["attachment1"]["name"]);
			move_uploaded_file($_FILES["attachment1"]["tmp_name"], $target_file);
		} else{
			$attachment1 = $this->input->post('attachment1Old');
		}

		if($_FILES["attachment2"]["name"]!='') {
			$target_dir = "assets/timesheetAttachments/";
			$target_file = $target_dir . basename($_FILES["attachment2"]["name"]);
			$attachment2 = basename($_FILES["attachment2"]["name"]);
			move_uploaded_file($_FILES["attachment2"]["tmp_name"], $target_file);
		} else{
			$attachment2 = $this->input->post('attachment2Old');
		}
		$weekDate = $this->input->post('weekDate');

		$checkAttachment1 = $this->mainModel->checkWeekAttachment($weekDate,$userUniqueId);
		if($checkAttachment1>0){
			$form_data = array(
				'attachment1' => $attachment1,
				'attachment2' => $attachment2
			);
			$this->mainModel->updateWeekAttachment($weekDate,$form_data,$userUniqueId);
		}else{
			$form_data = array(
				'userUniqueId' => $data['loginUniqueId'],
				'employeeId' => $data['employeeId'],
				'attachment1' => $attachment1,
				'attachment1' => $attachment1,
				'attachment2' => $attachment2,
				'weekDate' =>$weekDate
			);
			$this->mainModel->insertTimesheetAttachments($form_data);
		}
		 
		
		$this->session->set_flashdata('message', 'Data updated successfully');		
		redirect($_SERVER['HTTP_REFERER'], 'Location'); 
	}

	public function approveComment(){
		$data = $this->global_functions(); 
		$timeSheetAutoId = $this->input->get('timeSheetAutoId');
		$form_data = array(
                'weekStatus' => 1
            );
        $insert=$this->mainModel->approveComment($form_data,$timeSheetAutoId);
        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment approved successfully');
		}else{
		    $this->session->set_flashdata('message', 'Failed due to error.');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function RejectComment(){
		$data = $this->global_functions(); 
		$timeSheetAutoId = $this->input->get('timeSheetAutoId');
		$form_data = array(
                'weekStatus' => 0
            );
        $insert=$this->mainModel->approveComment($form_data,$timeSheetAutoId);

        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment approved successfully');
		}else{
		    $this->session->set_flashdata('message', 'Failed due to error.');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function approveReview(){
		$data = $this->global_functions(); 
		$weeklyReviewId = $this->input->get('weeklyReviewId');
		$form_data = array(
                'reviewStatus' => 1
            );
        $insert=$this->mainModel->approveReview($form_data,$weeklyReviewId);
        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}else{
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function RejectReview(){
		$data = $this->global_functions(); 
		$weeklyReviewId = $this->input->get('weeklyReviewId');
		$form_data = array(
                'reviewStatus' => 0
            );
        $insert=$this->mainModel->approveReview($form_data,$weeklyReviewId);

        if($insert==1) {
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}else{
		    $this->session->set_flashdata('message', 'Comment updated successfully');
		}
		redirect($_SERVER['HTTP_REFERER'], 'Location');
	}

	public function logout() {	
		$this->session->sess_destroy();
		redirect(base_url('/'), 'Location');
	}

    
}