<?php defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");

class Homecontroller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        if(!empty($this->session->userdata('userTypeCode'))){
    		redirect(base_url('dashboard'), 'Location');
    	}
		$this->load->model('MainDB_model', 'mainModel');
    }

    public function global_functions(){    	
    	
    }

    public function index() {
		$this->load->view('landingpage');	
	}
	
	public function login() {
		$this->load->view('index');	
	}  

	public function forgotPassword() {
		$this->session->unset_userdata('userOtp');
		$this->session->unset_userdata('utype');
		$this->session->unset_userdata('userid');
		$this->load->view('forgotPassword');	
	} 

	public function submitForgotPassword(){
		$response = array();
	    $emailMobile = $this->input->post('emailMobile');   
	    $loginOtp = $this->input->post('loginOtp');
	    $password = $this->input->post('password');

	    if($password != ""){
	    	$userTypeCode = $this->session->userdata('utype');
	    	$userUniqueId = $this->session->userdata('userid');

	    	$pw = md5($password);
			$form_data = array(
			'password' => $pw,
			'test_password' =>$password		
			);		
			if($userTypeCode == 2){
				$update = $this->mainModel->updateBusinessPassword($form_data,$userUniqueId);
			}else{
				$update = $this->mainModel->updateUserPassword($form_data,$userUniqueId);
			}
			$response["status"] = true; 
			$response["otpStatus"] = false; 
			$response["pStatus"] = true;      
			$response["message"] = "Password changed successfully";
			echo json_encode($response);
			$this->session->unset_userdata('userOtp');
			$this->session->unset_userdata('utype');
			$this->session->unset_userdata('userid');
	    	return false;
	    }

	    if($loginOtp != ""){ 
	    	if($loginOtp == $this->session->userdata('userOtp')){
	    		$response["status"] = true;    
	    		$response["otpStatus"] = true;     
				$response["message"] = "OTP validated. Please enter new password.";
	    	}else{
	    		$response["status"] = false;
	    		$response["otpStatus"] = false;      
				$response["message"] = "Invalid OTP";
	    	}
	    	echo json_encode($response);
	    	return false;
	    }

	    
	    $userUniqueId = $this->mainModel->checkUserMobileEmail($emailMobile); 
	    $businessUniqueId = $this->mainModel->checkBusinessMobileEmail($emailMobile);  
	    $response["otpStatus"] = false; 
	    if($userUniqueId != '0'){
			$userInfo = $this->mainModel->getUserInfo($userUniqueId);
			if($userInfo[0]['userStatus'] == 2){
				$response["status"] = false;      
				$response["message"] = "Your account is disabled. Please contact to admin.";
			}else if($userInfo[0]['userStatus'] == 0){
				$response["status"] = false;      
				$response["message"] = "Your account is Inactive. Please contact to admin.";
			}else if($userInfo[0]['userStatus'] == 1){
				$otp = $this->mainModel->generateSMSKey();
				$this->sendEmail($userInfo[0]['email'],$userInfo[0]['mobile'],$otp);
				$result = $this->mainModel->sendsms($userInfo[0]['mobile'],$otp);
				$this->mainModel->updateSms($userInfo[0]['mobile'],$otp);
				$response["status"] = true;      
				$response["message"] = "OTP sent to your email. Please enter here.";
				$this->session->set_userdata('userOtp', $otp);
				$this->session->set_userdata('utype', $userInfo[0]['userTypeCode']);
				$this->session->set_userdata('userid', $userInfo[0]['userUniqueId']);
			}else{
				$response["status"] = false;      
				$response["message"] = "System Error. Try again later";
			}
	    }else if($businessUniqueId  != '0'){
			//$result = $this->servModel->sendPasswordActivation($user_key, $email);
			$userInfo = $this->mainModel->getBusinessInfo($businessUniqueId);
			if($userInfo[0]['businessStatus'] == 2){
				$response["status"] = false;      
				$response["message"] = "Your account is disabled. Please contact to admin.";
			}else if($userInfo[0]['businessStatus'] == 0){
				$response["status"] = false;      
				$response["message"] = "Your account is Inactive. Please contact to admin.";
			}else if($userInfo[0]['businessStatus'] == 1){			
				$otp = $this->mainModel->generateSMSKey();	
				$this->sendEmail($userInfo[0]['email'],$userInfo[0]['mobile'],$otp);
				$result = $this->mainModel->sendsms($userInfo[0]['mobile'],$otp);
				$this->mainModel->updateSms($userInfo[0]['mobile'],$otp);
				$response["status"] = true;      
				$response["message"] = "OTP sent to your email. Please enter here.";
				$this->session->set_userdata('userOtp', $otp);
				$this->session->set_userdata('utype', $userInfo[0]['userTypeCode']);
				$this->session->set_userdata('userid', $userInfo[0]['userUniqueId']);
			}else{
				$response["status"] = false;      
				$response["message"] = "System Error. Try again later";
			}
	    }else{
	    	$response["status"] = false;      
			$response["message"] = "This Email/Mobile does not exist.";
	    }
    	echo json_encode($response); 
	}   

	public function sendWelcomeOtpEmail($email,$mobile,$otp){
		$config = Array(
			'protocol' => 'sendmail',
			'mailpath' => '/usr/sbin/sendmail',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);
	
		$query = $this->db->get_where('tbl_communications', array('commid' => '13'), 1);
		$row = $query->result_array();
		$email_message = $row[0]['message'];
		$subject =  $row[0]['subject'];
		$finalmessage = str_replace('MOBILE',$mobile,$email_message);
		$finalmessage = str_replace('ENTEROTP',$otp,$finalmessage);
		
		//$list = array($email, 'info@efileservices.in');
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL,'Aster IT');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($finalmessage);
		$this->email->send();
	}


	public function sendEmail($email,$mobile,$otp){
		$config = Array(
			'protocol' => 'sendmail',
			'mailpath' => '/usr/sbin/sendmail',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);
	
		$query = $this->db->get_where('tbl_communications', array('commid' => '2'), 1);
		$row = $query->result_array();
		$email_message = $row[0]['message'];
		$subject =  $row[0]['subject'];
		$finalmessage = str_replace('MOBILE',$mobile,$email_message);
		$finalmessage = str_replace('OTP',$otp,$finalmessage);
		
		//$list = array($email, 'info@efileservices.in');
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from(FROM_EMAIL,'Aster IT');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($finalmessage);
		$this->email->send();
	}

	public function loginAction() {
		$response = array();
		$mobile = $this->input->post('emailMobile');
		$password = md5($this->input->post('password'));
		$checkUser = $this->mainModel->checkUser($mobile,$password);
		$checkBusiness = $this->mainModel->checkBusiness($mobile,$password);
		$checkBusinessAvail = $this->mainModel->checkBusinessAvail($mobile,$password);
		
		$userinfo = $this->mainModel->getUserInfo($checkUser);
		$businessInfo = '';
		if($checkBusiness !='no_business'){
		    $businessInfo = $this->mainModel->getBusinessInfo($checkBusiness);
		}		
		if($checkBusinessAvail=='business_avail'){			
			if($businessInfo[0]['businessStatus'] == 1){
				$this->session->set_userdata('userTypeCode', $businessInfo[0]['userTypeCode']);
				$this->session->set_userdata('loginUniqueId', $businessInfo[0]['businessUniqueId']);
				$this->session->set_userdata('userType', $businessInfo[0]['userType']);
				$message = "success"; 
			}else if($businessInfo[0]['businessStatus'] == 0){
				$message= "Your account is inactive";
			}else if($businessInfo[0]['businessStatus'] == 2){
				$message= "Your account is disabled";
			}
			
		}else if($checkUser=='no_user'){
			$message= "Please Check Credentials";
		}else{			
   			if($userinfo[0]['userStatus'] == 1){
   				$this->session->set_userdata('userTypeCode', $userinfo[0]['userTypeCode']);
				$this->session->set_userdata('loginUniqueId', $userinfo[0]['userUniqueId']);
				$this->session->set_userdata('userType', $userinfo[0]['userType']);
	   			$message = "success";
			}else if($userinfo[0]['userStatus'] == 0){
				$message= "Your account is inactive";
			}else if($userinfo[0]['userStatus'] == 2){
				$message= "Your account is disabled";
			}

		}
		echo $message;
	}

	public function saveNewBusiness(){ //echo "ggg"; die();
		$response = array();
        $restkey = md5($this->mainModel->generateRESTKey());
        $otpverify = 0;
        $otpValue = $this->input->post('otpValue');
        $loginOtp = $this->input->post('loginOtp');
        $businessName = $this->input->post('businessName');        
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $password = $this->input->post('password');
        $address = $this->input->post('address');
        $businessUrl = $this->input->post('businessUrl');
        $ssnNumber = $this->input->post('ssnNumber');
        $businessId = $this->mainModel->generateBusinessID();
        $form_data = array(
			'userTypeCode' =>BUSINESS,
            'userType' =>'Business',
			'businessId' =>  $businessId,
			'businessName' =>  $businessName,
			'email' =>  $email,
			'mobile' =>  $mobile,
			'password' => md5($password),
			'address' => $address,
			'businessUrl' => $businessUrl,
			'ssnNumber' => $ssnNumber,
			'businessStatus' =>1
		);

        if(!empty($businessName) && !empty($email) && !empty($mobile) && !empty($password)){ 
	    	$check = $this->mainModel->checkBusinessUserAvailable($mobile,$email);
	    	if($check !=0){ 
    			$response["status"] = false; 
				$response["otpStatus"] = false; 
				$response["message"] = "Email/Mobile already exist.";
				echo json_encode($response);
	    		return false;
	    	}	    	
	    } 
    	if($this->session->userdata('userOtp') == ""){  
    		$otp = $this->mainModel->generateSMSKey();
			$this->sendWelcomeOtpEmail($email,$mobile,$otp); 
			$result = $this->mainModel->sendSmsEmail($mobile,$otp);
			//$this->mainModel->updateSms($mobile,$otp);
			$response["status"] = true;      
			$response["message"] = "OTP sent to your email. Please enter here.";
			$response["otpStatus"] = false; 
			$this->session->set_userdata('userOtp', $otp);			
			echo json_encode($response);
    		return false;
    	}	    	
	    

        if($loginOtp != ""){ 
	    	if($loginOtp == $this->session->userdata('userOtp')){
	    		$insert = $this->mainModel->insertBusiness($form_data);	
				if($insert>0) {
					$this->session->set_userdata('userid', $restkey);
					$response["status"] = true; 
					$response["otpStatus"] = true;   
					$response["message"] = "OTP Validated Successsfully";
					$this->session->unset_userdata('userOtp');
				}	
	    	}else{
	    		$response["status"] = true;
	    		$response["otpStatus"] = false;      
				$response["message"] = "Invalid OTP";
	    	}
	    	echo json_encode($response);
	    	return false;
	    }
	}

	public function businessSignup(){
		$this->session->unset_userdata('userOtp');
		$this->load->view('business-signup');
	}
    
}