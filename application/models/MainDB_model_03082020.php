<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	
class MainDB_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function validateLogin() {
		$uname = $this->input->post('mobile'); 
		$pwd = md5($this->input->post('password'));
		$sql = "select * from tbl_users_master where mobile='$uname' and password='$pwd' and status=1";
		$query = $this->db->query($sql);		
        $cnt = $query->num_rows();	
        if ($cnt == 1) {
			$row = $query->result_array();
			$uid = $row[0]['rest_verification_key'];
            return $uid;
        } else {
			return 'No_User';
		}
	}

	 public function generateTicketID() {
		$sql = "select m_ticket_id from tbl_tickets where m_ticket_id LIKE '%MMC_TKT%' order by ticket_id desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('MMC_TKT',$row[0]['m_ticket_id']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'MMC_TKT'.$random_string;
		} else {
			$uniqueid = 'MMC_TKT001';
		}
		return $uniqueid;
	}
	
	public function generateBusinessID() {
		$sql = "select businessId from business where businessId LIKE '%BIN%' order by businessUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('BIN',$row[0]['businessId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'BIN'.$random_string;
		} else {
			$uniqueid = 'BIN001';
		}
		return $uniqueid;
	}
	
	public function generateEmployeeID($businessUniqueId) {
		$sql = "select employeeId from users where employeeId LIKE '%MEM%' AND businessUniqueId=$businessUniqueId order by userUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('MEM',$row[0]['employeeId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'MEM'.$random_string;
		} else {
			$uniqueid = 'MEM001';
		}
		return $uniqueid;
	}

	public function generateVendorID($businessUniqueId) {
		$sql = "select vendorId from vendors where vendorId LIKE '%VEN%' AND businessUniqueId=$businessUniqueId order by vendorUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('VEN',$row[0]['vendorId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'VEN'.$random_string;
		} else {
			$uniqueid = 'VEN001';
		}
		return $uniqueid;
	}

	public function generateClientID($businessUniqueId) {
		$sql = "select clientId from clients where clientId LIKE '%CLI%' AND businessUniqueId=$businessUniqueId order by clientUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('CLI',$row[0]['clientId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'CLI'.$random_string;
		} else {
			$uniqueid = 'CLI001';
		}
		return $uniqueid;
	}
	
	public function generateEmailKey() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ123456789',6)),0,6);
	}

	public function generateSMSKey() {
		return substr(str_shuffle(str_repeat('123456789',5)),0,5);
	}

	public function generateRESTKey() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ6789',6)),0,6);
	}
	
	public function generatePassword() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ6789',10)),0,10);
	}
	
	public function checkUserMobileEmail($emailMobile) {
		$sql = "select * from users where mobile='$emailMobile' or email='$emailMobile'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		$row = $query->result_array();
		if($cnt>0) {
			return $row[0]['userUniqueId'];
		} else {
			return 0;
		}		
	}

	public function checkBusinessMobileEmail($emailMobile) {
		$sql = "select * from business where mobile='$emailMobile' or email='$emailMobile'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		$row = $query->result_array();
		if($cnt>0) {
			return $row[0]['businessUniqueId'];
		} else {
			return 0;
		}		
	}
	
	public function checkUserAvail($ph,$email,$panNumber = NULL) {
		$sql = "select * from users where mobile='$ph' or email='$email' or (panNumber = '$panNumber' and panNumber!='')";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}

	public function checkBusinessUserAvailable($mobile,$email) {
		$sql = "select * from business where mobile='$mobile' or email='$email'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();

		$sql1 = "select * from users where mobile='$mobile' or email='$email'";
		$query1 = $this->db->query($sql1);
		$cnt1 = $query1->num_rows();
				
		if($cnt>0 || $cnt1>0) {
			if($cnt){
				$row = $query->result_array();
			}else{
				$row = $query1->result_array();
			}	
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkEmployeeAvailable($mobile,$email) {
		$sql = "select * from users where mobile='$mobile' or email='$email'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();

		$sql1 = "select * from business where mobile='$mobile' or email='$email'";
		$query1 = $this->db->query($sql1);
		$cnt1 = $query1->num_rows();
				
		if($cnt>0 || $cnt1>0) {
			if($cnt){
				$row = $query->result_array();
			}else{
				$row = $query1->result_array();
			}			

			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkVendorAvailable($mobile,$email) {
		$sql = "select * from vendors where mobile='$mobile' or email='$email'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			$row = $query->result_array();
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkClientAvailable($mobile,$email) {
		$sql = "select * from clients where mobile='$mobile' or email='$email'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			$row = $query->result_array();
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkExistBusinessUserAvailable($mobile,$email,$businessUniqueId) {
		$sql = "select * from business where (mobile='$mobile' or email='$email') AND businessUniqueId!=$businessUniqueId";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			$row = $query->result_array();
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkExistUserAvailable($mobile,$email,$userUniqueId) {
		$sql = "select * from users where (mobile='$mobile' or email='$email') AND userUniqueId!=$userUniqueId";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			$row = $query->result_array();
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkExistVendorAvailable($mobile,$email,$vendorUniqueId) {
		$sql = "select * from vendors where (mobile='$mobile' or email='$email') AND vendorUniqueId!=$vendorUniqueId";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			$row = $query->result_array();
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}

	public function checkExistClientAvailable($mobile,$email,$clientUniqueId) {
		$sql = "select * from clients where (mobile='$mobile' or email='$email') AND clientUniqueId!=$clientUniqueId";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			$row = $query->result_array();
			if($mobile == $row[0]['mobile'] && $email != $row[0]['email']){
				return 1;
			}else if($email == $row[0]['email'] && $mobile != $row[0]['mobile']){
				return 2;
			}else if($email == $row[0]['email'] && $email == $row[0]['email']){
				return 3;
			}else{
				return 4;
			}			
		} else {
			return 0;
		}		
	}
	
	public function checkUserMobile($ph) {
		$sql = "select * from users where mobile='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
	
	public function checkUserEmail($ph) {
		$sql = "select * from users where email='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
	
	public function insertUser($data){
        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }

    public function userDocuments($data){
        $this->db->insert('user_documents',$data);
        return $this->db->insert_id();
    }

    public function businessDocuments($data){
        $this->db->insert('business_documents',$data);
        return $this->db->insert_id();
    }

    public function insertVendor($data){
        $this->db->insert('vendors',$data);
        return $this->db->insert_id();
    }

    public function insertClient($data){
        $this->db->insert('clients',$data);
        return $this->db->insert_id();
    }   
    
    public function insertBusiness($data){
        $this->db->insert('business',$data);
        return $this->db->insert_id();
    }
    
    public function otpVerify($ph,$sms){
        $sql = "select * from sms_history where mobile='$ph' AND sms='$sms'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}
    }
	
	public function checkUser($ph,$pw) {
		$sql = "select * from users WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND userStatus!=2"; 	
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_user';
		} else {
			$row = $query->result_array();
			return $row[0]['userUniqueId'];
		}		
	}
	
	public function checkBusiness($ph,$pw) {
		$sql = "select * from business WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND businessStatus!=2";
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_business';
		} else {
			$row = $query->result_array();
			return $row[0]['businessUniqueId'];
		}		
	}
	
	public function checkBusinessAvail($ph,$pw) {
		$sql = "select * from business WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND businessStatus!=2";		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_business';
		} else {
			return 'business_avail';
		}		
	}
	
	
	
	public function checkBusinessUser($ph,$pw) {
		$sql = "select * from business WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND businessStatus!=2";
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_user';
		} else {
			$row = $query->result_array();
			return $row[0]['businessUniqueId'];
		}		
	}
	
	public function getUserInfo($id) {
		$sql = "select * from users where userUniqueId=?";
		$query = $this->db->query($sql,$id);
            return $query->result_array();
	}

	public function getVendorInfo($id) {
		$sql = "select * from vendors where vendorUniqueId=?";
		$query = $this->db->query($sql,$id);
            return $query->result_array();
	}

	public function getClientInfo($id) {
		$sql = "select * from clients where clientUniqueId=?";
		$query = $this->db->query($sql,$id);
            return $query->result_array();
	}

	public function getUserDocuments($id) {
		$sql = "select * from user_documents where userUniqueId=?";
		$query = $this->db->query($sql,$id);
            return $query->result_array();
	}
	
	public function getTypeName($code,$type) {       
        $sql = "select * from tbl_codes where code=$code AND type='$type'";       
        $query = $this->db->query($sql);
        $cnt = $query->num_rows();
        if ($cnt > 0) {
            $row = $query->result_array();
            return $row[0]['value'];
        }
    }
    
    public function checkOtpMobile($ph) {
		$sql = "select * from sms_history where mobile='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
    
    public function sendSmsEmail($mobile,$email){
        $sms = $this->generateSMSKey();
        $this->updateSms($mobile,$sms);
        $message = "Please use OTP: $sms for signup.";
        $this->sendSms($mobile,$message);
        return $message;
    }
    
    public function updateSms($mobile,$sms){
        $checkUser = $this->checkOtpMobile($mobile);
        if($checkUser == 0){
            $form_data = array(
		    'mobile' =>$mobile,
			'sms' =>  $sms
    		);
    		$this->db->insert('sms_history',$form_data);
        }else{
             $form_data = array(
			'sms' =>  $sms
    		);
            $this->db->where('mobile',$mobile);
            $this->db->update('sms_history',$form_data);
        }
    }
    
    public function sendSms($mobile,$message){
        $sendtext = urlencode("$message");
        $fields = array(
        	'projectid' => urlencode(PROJECT_ID),
        	'authtoken' => urlencode(AUTH_TOKEN),
        	'to' => urlencode("+91".$mobile),
        	'body' => $sendtext
        );
        $fields_string = '';
        
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
        $url = SMS_URL;
        $curl = curl_init();
         curl_setopt($curl, CURLOPT_POST, 1);
         // OPTIONS:
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl,CURLOPT_POST, count($fields));
         curl_setopt($curl,CURLOPT_POSTFIELDS, $fields_string);
        
         curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        	//"cache-control: no-cache",
        	'Content-type: application/x-www-form-urlencoded'
         ));
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        
         // EXECUTE:
         $result = curl_exec($curl);
         if(!$result){die("Connection Failure");}else{echo "";}
         curl_close($curl);
    }
    
    public function checkOldPassword($oldPass,$userkey) {
		$sql ="select * from users WHERE password = '".md5($oldPass)."' AND restKey='$userkey'"; 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt > 0){
			return 1;
		}else{
			return 0;
		}
	}
	
	public function updateUserPassword($data,$userUniqueId) {
		$this->db->where('userUniqueId',$userUniqueId);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function updateVendor($data,$vendorUniqueId) {
		$this->db->where('vendorUniqueId',$vendorUniqueId);
		$this->db->update('vendors',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function updateClient($data,$clientUniqueId) {
		$this->db->where('clientUniqueId',$clientUniqueId);
		$this->db->update('clients',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function updateBusinessPassword($data,$businessUniqueId) {
		$this->db->where('businessUniqueId',$businessUniqueId);
		$this->db->update('business',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function updateBusiness($data,$businessUniqueId) {
		$this->db->where('businessUniqueId',$businessUniqueId);
		$this->db->update('business',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}

	public function updateUser($data,$userUniqueId) {
		$this->db->where('userUniqueId',$userUniqueId);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 1;	
		}
	}
	
	public function getProfile($userid){
	    $sql = "select * from users WHERE userUniqueId='$userid'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function getBusinessInfo($businessUniqueId){ 
	    $sql ="select * from business WHERE businessUniqueId=$businessUniqueId";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function updateProfileDetails($data,$userid) { 
        $this->db->where('userUniqueId',$userid);
        $this->db->update('users',$data);
        if($this->db->affected_rows() == 1) {
            return 1;
        } else {
            return 0;   
        }
    }
    
	public function updateCompanyDetails($data,$userid) { 
        $this->db->where('businessUniqueId',$userid);
        $this->db->update('business',$data);
        if($this->db->affected_rows() == 1) {
            return 1;
        } else {
            return 0;   
        }
    }
	
	public function getUsers($userTypeCode,$businessUniqueId){
	    if($businessUniqueId == ""){
	        $sql = "SELECT * FROM `users` WHERE userTypeCode=$userTypeCode AND userStatus!=2 ORDER BY userUniqueId DESC";
	    }else{
	        $sql = "SELECT * FROM `users` WHERE userTypeCode=$userTypeCode AND businessUniqueId=$businessUniqueId AND userStatus!=2 ORDER BY userUniqueId DESC";
	    }
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}

	public function getVendors($businessUniqueId){
	    if($businessUniqueId == ""){
	         $sql = "SELECT * FROM `vendors` WHERE vendorStatus!=2 ORDER BY vendorUniqueId DESC";
	    }else{
	        $sql = "SELECT * FROM `vendors` WHERE businessUniqueId=$businessUniqueId AND vendorStatus!=2 ORDER BY vendorUniqueId DESC";
	    }
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}

	public function getClients($businessUniqueId){
	    if($businessUniqueId == ""){
	         $sql = "SELECT * FROM `clients` WHERE clientStatus!=2 ORDER BY clientUniqueId DESC";
	    }else{
	        $sql="SELECT * FROM `clients` WHERE businessUniqueId=$businessUniqueId AND clientStatus!=2 ORDER BY clientUniqueId DESC";
	    }
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function getBusinessList(){
	     $sql = "SELECT * FROM `business` WHERE businessStatus!=2 ORDER BY businessUniqueId DESC";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function userStatusUpdate($data,$userd) {
		$this->db->where('userUniqueId',$userd);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function businessStatusUpdate($data,$userd) {
		$this->db->where('businessUniqueId',$userd);
		$this->db->update('business',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	
	public function getTypes($type){
	    $sql = "select code,value from tbl_codes WHERE type='$type' AND status=1"; 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function deleteUser($userid){
	     $sql = "UPDATE users set userStatus=2 WHERE userUniqueId='$userid'";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function deleteVendor($vendorUniqueId){
	     $sql = "UPDATE vendors set vendorStatus=2 WHERE vendorUniqueId='$vendorUniqueId'";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function deleteClient($clientUniqueId){
	     $sql = "UPDATE clients set clientStatus=2 WHERE clientUniqueId='$clientUniqueId'";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function deleteBusiness($userid){
	     $sql = "UPDATE business set businessStatus=2 WHERE businessUniqueId=$userid";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function updateProfilePic($data,$user_id) {
		$this->db->where('userUniqueId',$user_id);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function updateBusinesslogo($data,$user_id) {
		$this->db->where('businessUniqueId',$user_id);
		$this->db->update('business',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

}