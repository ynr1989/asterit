<?php
class ServicesDB_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	
    public function generateTicketID() {
		$sql = "select m_ticket_id from tbl_tickets where m_ticket_id LIKE '%MMC_TKT%' order by ticket_id desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('MMC_TKT',$row[0]['m_ticket_id']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'MMC_TKT'.$random_string;
		} else {
			$uniqueid = 'MMC_TKT001';
		}
		return $uniqueid;
	}
	
	public function generateBusinessID() {
		$sql = "select businessId from business where businessId LIKE '%AST%' order by businessUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('AST',$row[0]['businessId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'AST'.$random_string;
		} else {
			$uniqueid = 'AST001';
		}
		return $uniqueid;
	}
	
	public function generateUserID() {
		$sql = "select userId from users where userId LIKE '%MEM%' order by userUniqueId desc limit 1";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt>=1) {
			$row = $query->result_array();
			$existid = explode('MEM',$row[0]['userId']);
			$random_string = sprintf("%03d", $existid[1] + 1);
			$uniqueid = 'MEM'.$random_string;
		} else {
			$uniqueid = 'MEM001';
		}
		return $uniqueid;
	}
	
	public function generateEmailKey() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ123456789',6)),0,6);
	}

	public function generateSMSKey() {
		return substr(str_shuffle(str_repeat('123456789',5)),0,5);
	}

	public function generateRESTKey() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ6789',6)),0,6);
	}
	
	public function generatePassword() {
		return substr(str_shuffle(str_repeat('ABCDEFGHJKMNPQRSTUVWXYZ6789',10)),0,10);
	}
	
	public function checkMobile($ph) {
		$sql = "select * from users where mobile='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		$row = $query->result_array();
		if($cnt>0) {
			return $row[0]['restKey'];
		} else {
			return 0;
		}		
	}
	
	public function checkUserAvail($ph,$email,$panNumber = NULL) {
		$sql = "select * from users where mobile='$ph' or email='$email' or (panNumber = '$panNumber' and panNumber!='')";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
	
	public function checkUserMobile($ph) {
		$sql = "select * from users where mobile='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
	
	public function checkUserEmail($ph) {
		$sql = "select * from users where email='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
	
	public function insertUser($data){
        $this->db->insert('users',$data);
        return 1;
    }
    
    public function insertBusiness($data){
        $this->db->insert('business',$data);
        return $this->db->insert_id();
    }
    
    public function otpVerify($ph,$sms){
        $sql = "select * from sms_history where mobile='$ph' AND sms='$sms'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}
    }
	
	public function checkUser($ph,$pw) {
		$sql = "select * from users WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND userStatus!=2";
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_user';
		} else {
			$row = $query->result_array();
			return $row[0]['userUniqueId'];
		}		
	}
	
	public function checkBusiness($ph,$pw) {
		$sql = "select * from business WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND businessStatus!=2";
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_business';
		} else {
			$row = $query->result_array();
			return $row[0]['businessUniqueId'];
		}		
	}
	
	public function checkBusinessAvail($ph,$pw) {
		$sql = "select * from business WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND businessStatus!=2";
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_business';
		} else {
			return 'business_avail';
		}		
	}
	
	
	
		public function checkBusinessUser($ph,$pw) {
		$sql = "select * from business WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND businessStatus!=2";
		
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 'no_user';
		} else {
			$row = $query->result_array();
			return $row[0]['businessUniqueId'];
		}		
	}
	
	public function getUserDetailsByKey($id) {
		$sql = "select * from users where userUniqueId=?";
		$query = $this->db->query($sql,$id);
            return $query->result_array();
	}
	
	public function getTypeName($code,$type) {       
        $sql = "select * from tbl_codes where code=$code AND type='$type'";        
        $query = $this->db->query($sql);
        $cnt = $query->num_rows();
        if ($cnt > 0) {
            $row = $query->result_array();
            return $row[0]['value'];
        }
    }
    
    public function checkOtpMobile($ph) {
		$sql = "select * from sms_history where mobile='$ph'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt>0) {
			return 1;
		} else {
			return 0;
		}		
	}
    
    public function sendSmsEmail($mobile,$email){
        $sms = $this->generateSMSKey();
        $this->updateSms($mobile,$sms);
        $message = "Please use OTP: $sms for signup.";
        $this->sendSms($mobile,$message);
        return $message;
    }
    
    public function updateSms($mobile,$sms){
        $checkUser = $this->servModel->checkOtpMobile($mobile);
        if($checkUser == 0){
            $form_data = array(
		    'mobile' =>$mobile,
			'sms' =>  $sms
    		);
    		$this->db->insert('sms_history',$form_data);
        }else{
             $form_data = array(
			'sms' =>  $sms
    		);
            $this->db->where('mobile',$mobile);
            $this->db->update('sms_history',$form_data);
        }
    }
    
    public function sendSms($mobile,$message){
        $sendtext = urlencode("$message");
        $fields = array(
        	'projectid' => urlencode(PROJECT_ID),
        	'authtoken' => urlencode(AUTH_TOKEN),
        	'to' => urlencode("+91".$mobile),
        	'body' => $sendtext
        );
        $fields_string = '';
        
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
        $url = SMS_URL;
        $curl = curl_init();
         curl_setopt($curl, CURLOPT_POST, 1);
         // OPTIONS:
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl,CURLOPT_POST, count($fields));
         curl_setopt($curl,CURLOPT_POSTFIELDS, $fields_string);
        
         curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        	//"cache-control: no-cache",
        	'Content-type: application/x-www-form-urlencoded'
         ));
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        
         // EXECUTE:
         $result = curl_exec($curl);
         if(!$result){die("Connection Failure");}else{echo "";}
         curl_close($curl);
    }
    
    public function checkOldPassword($oldPass,$userkey) {
		$sql ="select * from users WHERE password = '".md5($oldPass)."' AND restKey='$userkey'"; 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();		
		if($cnt > 0){
			return 1;
		}else{
			return 0;
		}
	}
	
	public function updateUserPassword($conPass,$user_id) {
		$data = array(
			'password' => md5($conPass)
		);		
		$this->db->where('restKey',$user_id);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function getProfile($userid){
	    $sql = "select * from users WHERE userUniqueId='$userid'";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function getBusinessInfo($businessUniqueId){ 
	    $sql ="select * from business WHERE businessUniqueId=$businessUniqueId";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function updateProfileDetails($data,$userid) { 
        $this->db->where('userUniqueId',$userid);
        $this->db->update('users',$data);
        if($this->db->affected_rows() == 1) {
            return 1;
        } else {
            return 0;   
        }
    }
    
	public function updateCompanyDetails($data,$userid) { 
        $this->db->where('businessUniqueId',$userid);
        $this->db->update('business',$data);
        if($this->db->affected_rows() == 1) {
            return 1;
        } else {
            return 0;   
        }
    }
	
	public function getUsers(){
	    $type = $this->input->post('userTypeCode');
	    $businessId = $this->input->post('businessUniqueId');
	    if($businessId == ""){
	        $sql = "SELECT * FROM `users` WHERE userTypeCode=$type AND userStatus!=2 ORDER BY userUniqueId DESC";
	    }else{
	        $sql = "SELECT * FROM `users` WHERE userTypeCode=$type AND businessUniqueId=$businessId AND userStatus!=2 ORDER BY userUniqueId DESC";
	    }
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function getBusinessList(){
	     $sql = "SELECT * FROM `business` WHERE businessStatus!=2 ORDER BY businessUniqueId DESC";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function userStatusUpdate($data,$userd) {
		$this->db->where('userUniqueId',$userd);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function businessStatusUpdate($data,$userd) {
		$this->db->where('businessUniqueId',$userd);
		$this->db->update('business',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	
	public function getTypes($type){
	    $sql = "select code,value from tbl_codes WHERE type='$type' AND status=1"; 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0) {
			return 0;
		} else {
			$row = $query->result_array();
			return $row;
		}
	}
	
	public function deleteUser($userid){
	     $sql = "UPDATE users set userStatus=2 WHERE userUniqueId='$userid'";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function deleteBusiness($userid){
	     $sql = "UPDATE business set businessStatus=2 WHERE businessUniqueId=$userid";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function updateProfilePic($data,$user_id) {
		$this->db->where('userUniqueId',$user_id);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
	
	public function updateBusinesslogo($data,$user_id) {
		$this->db->where('businessUniqueId',$user_id);
		$this->db->update('business',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}
}